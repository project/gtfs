<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of GTFS FareAttribute entities.
 *
 * @ingroup gtfs
 */
class FareAttributeListBuilder extends GTFSObjectListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['fare_id'] = $this->t('ID');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\gtfs\Entity\FareAttribute $entity */
    $row['fare_id'] = Link::createFromRoute(
      $entity->label(),
      'entity.gtfs_fare_attribute.canonical',
      ['gtfs_fare_attribute' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
