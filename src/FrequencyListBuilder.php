<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of GTFS Frequency entities.
 *
 * @ingroup gtfs
 */
class FrequencyListBuilder extends GTFSObjectListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['frequency_id'] = $this->t('ID');
    $header['frequency_name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\gtfs\Entity\Frequency $entity */
    $row['frequency_id'] = $entity->get('frequency_id')->value;
    $row['frequency_name'] = Link::createFromRoute(
      $entity->label(),
      'entity.gtfs_frequency.canonical',
      ['gtfs_frequency' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
