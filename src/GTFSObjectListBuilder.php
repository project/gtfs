<?php


namespace Drupal\gtfs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\gtfs\Entity\Feed;


/**
 * Class GTFSObjectListBuilder
 *
 * The base list builder that attaches the feed column for all GTFS entities.
 *
 * @package Drupal\gtfs
 */
abstract class GTFSObjectListBuilder extends EntityListBuilder {
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['feed_id'] = $this->t('Feed ID');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $feed = Feed::load($entity->get('feed_reference')->target_id);
    if ($feed) {
      $row['feed_id'] = Link::createFromRoute(
        $feed->id(),
        'entity.gtfs_feed.canonical',
        ['gtfs_feed' => $feed->id()]
      );
    } else {
      $row['feed_id'] = '';
    }
    return $row + parent::buildRow($entity);
  }


  /**
   * {@inheritDoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->sort($this->entityType->getKey('id'));

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    if ($feed_id = \Drupal::request()->query->get('feed_id')) {
      $query->condition('feed_reference__target_id', $feed_id);
    }
    return $query->execute();
  }
}
