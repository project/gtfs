<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of GTFS Stop entities.
 *
 * @ingroup gtfs
 */
class StopListBuilder extends GTFSObjectListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['stop_id'] = $this->t('ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\gtfs\Entity\Stop $entity */
    $row['stop_id'] = $entity->get('stop_id')->value;
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.gtfs_stop.canonical',
      ['gtfs_stop' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
