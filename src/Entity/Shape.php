<?php

namespace Drupal\gtfs\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the GTFS Shape entity.
 *
 * @ingroup gtfs
 *
 * @ContentEntityType(
 *   id = "gtfs_shape",
 *   label = @Translation("GTFS Shape"),
 *   handlers = {
 *     "storage" = "Drupal\gtfs\ShapeStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gtfs\ShapeListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "controller" = "Drupal\gtfs\Controller\ShapeController",
 *     "form" = {
 *       "default" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "add" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "edit" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "revision_revert" = "Drupal\gtfs\Form\ShapeRevisionRevertForm",
 *       "revision_delete" = "\Drupal\gtfs\Form\ShapeRevisionDeleteForm",
 *       "translation_revert" = "\Drupal\gtfs\Form\ShapeRevisionRevertTranslationForm",
 *       "settings" = "Drupal\gtfs\Form\ShapeSettingsForm",
 *     },
 *     "access" = "Drupal\gtfs\GTFSObjectAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gtfs\GTFSEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gtfs_shape",
 *   data_table = "gtfs_shape_field_data",
 *   revision_table = "gtfs_shape_revision",
 *   revision_data_table = "gtfs_shape_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer gtfs shape entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "shape_id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *    revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message"
 *   },
 *   links = {
 *     "canonical" = "/gtfs/shape/{gtfs_shape}",
 *     "add-form" = "/admin/gtfs/shape/add",
 *     "edit-form" = "/admin/gtfs/shape/{gtfs_shape}/edit",
 *     "delete-form" = "/admin/gtfs/shape/{gtfs_shape}/delete",
 *     "version-history" = "/admin/gtfs/shape/{gtfs_shape}/revisions",
 *     "revision" = "/admin/gtfs/shape/{gtfs_shape}/revisions/{gtfs_shape_revision}/view",
 *     "revision_revert" = "/admin/gtfs/shape/{gtfs_shape}/revisions/{gtfs_shape_revision}/revert",
 *     "revision_delete" = "/admin/gtfs/shape/{gtfs_shape}/revisions/{gtfs_shape_revision}/delete",
 *     "translation_revert" = "/admin/gtfs/shape/{gtfs_shape}/revisions/{gtfs_shape_revision}/revert/{langcode}",
 *     "collection" = "/admin/gtfs/shape",
 *   },
 *   field_ui_base_route = "gtfs_shape.settings"
 * )
 */
class Shape extends GTFSEntityBase implements GeodataInterface {

  public static $resourceURL = '/gtfs/api/v1/shapes';

  /**
   * {@inheritdoc}
   */
  public static function schema(): array {
    return [
      'fields' => [
        'shape_id' => ['type' => 'varchar', 'length' => 128, 'not null' => TRUE],
        'shape_pt_lat' => ['type' => 'numeric', 'precision' => 9, 'scale' => 6],
        'shape_pt_lon' => ['type' => 'numeric', 'precision' => 9, 'scale' => 6],
        'shape_pt_sequence' => ['type' => 'int', 'length' => 11, 'not null' => TRUE],
        'shape_dist_traveled' => ['type' => 'int', 'length' => 11],
      ],
      'primary key' => ['shape_id', 'shape_pt_sequence'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function resourceUrl(string $append = ''): string {
    $link = static::$resourceURL;
    $url = \Drupal::request()->getSchemeAndHttpHost() . $link . '/' . $this->get('shape_id')->value . '/' . $this->get('shape_pt_sequence')->value;
    $url .= $append;
    \Drupal::moduleHandler()->alter('gtfs_entity_resource_url', $url, $this);
    return $url;
  }

  public function geojson(): array {
    $other_points_in_shape = \Drupal::database()->query(
      'SELECT `shape_pt_lon`, `shape_pt_lat`
         FROM {gtfs_shape_field_data}
         WHERE `shape_id` = :shape_id
         ORDER BY `shape_pt_sequence` ASC',
      [':shape_id' => $this->get('shape_id')->value]
    )->fetchAll(\PDO::FETCH_NUM);

    foreach ($other_points_in_shape as &$pair) {
      $pair[0] = floatval($pair[0]);
      $pair[1] = floatval($pair[1]);
    }

    return [
      'type' => 'LineString',
      'coordinates' => $other_points_in_shape
    ];
  }

  public function links(): array {
    return array_merge(parent::links(), [
      'geojson' => $this->resourceUrl() . '/geojson',
    ]);
  }

  public function trips() {
    return Trip::loadMultiple(\Drupal::database()->query(
      'SELECT `id`
      FROM {gtfs_trip_field_data}
      WHERE `shape_id` = :shape_id',
      [':shape_id' => $this->id()]
    )->fetchAll(\PDO::FETCH_COLUMN));
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('shape_id')->value . ' point ' . $this->get('shape_pt_sequence')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $parts = explode(' ', $name);
    $this->set('shape_id', $parts[0]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['shape_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Shape ID'))
      ->setDescription(t('The shape_id field contains an ID that uniquely identifies a shape.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['shape_pt_lat'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Shape Lat'))
      ->setDescription(t('The shape_pt_lat field associates a shape point\'s latitude with a shape ID. The field value must be a valid WGS 84 latitude. Each row in shapes.txt represents a shape point in your shape definition.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'precision' => 10,
        'scale' => 6
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'number_decimal',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['shape_pt_lon'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Shape Lon'))
      ->setDescription(t('The shape_pt_lon field associates a shape point\'s longitude with a shape ID. The field value must be a valid WGS 84 longitude value from -180 to 180. Each row in shapes.txt represents a shape point in your shape definition.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'precision' => 10,
        'scale' => 6
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'number_decimal',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['shape_pt_sequence'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Shape Point Sequence'))
      ->setDescription(t('The shape_pt_sequence field associates the latitude and longitude of a shape point with its sequence order along the shape. The values for shape_pt_sequence must be non-negative integers, and they must increase along the trip.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['shape_dist_traveled'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Shape Distance Traveled'))
      ->setDescription(t('When used in the shapes.txt file, the shape_dist_traveled field positions a shape point as a distance traveled along a shape from the first shape point. The shape_dist_traveled field represents a real distance traveled along the route in units such as feet or kilometers. This information allows the trip planner to determine how much of the shape to draw when showing part of a trip on the map. The values used for shape_dist_traveled must increase along with shape_pt_sequence: they cannot be used to show reverse travel along a route.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'number_decimal',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    return $fields;
  }

}
