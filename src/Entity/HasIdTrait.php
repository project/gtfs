<?php

namespace Drupal\gtfs\Entity;

use Exception;


/**
 * Trait HasIdTrait
 *
 * Some GTFS entities have a unique ID, like agency_id. This allows retrieval by them.
 *
 * @package Drupal\gtfs\Entity
 */
trait HasIdTrait {

  /**
   * Gets the entity by its unique ID.
   *
   * @param $id
   *
   * @return mixed
   * @throws \Exception
   */
  public static function getById($id, $feed_id = null) {
    $class = get_called_class();
    $schema = self::schema();
    if (count($schema['primary key']) !== 1) {
      throw new Exception("Trying to get record of object {$class} with multiple or missing primary keys");
    }
    $id_field = $schema['primary key'][0];
    $entity_type = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class);
    $query = \Drupal::database()
      ->select("{$entity_type}_field_data", 'fd')
      ->condition("fd.{$id_field}", $id, '=')
      ->fields('fd', ['id'])
      ->addTag("{$entity_type}_access");
    if ($feed_id) {
      $query->condition("fd.feed_reference__target_id", $feed_id);
    }
    $drupal_id = $query
      ->execute()
      ->fetch(\PDO::FETCH_COLUMN);
    if (!$drupal_id) {
      $message = "{$class} with id {$id} does not exist";
      if ($feed_id) {
        $message .= " in feed {$feed_id}";
      }
      throw new Exception($message);
    }
    return self::load($drupal_id);
  }

  public static function getFromSource($id) {
    $class = get_called_class();
    $schema = self::schema();
    if (count($schema['primary key']) !== 1) {
      throw new Exception("Trying to get record of object {$class} with multiple or missing primary keys");
    }
    $id_field = $schema['primary key'][0];
    $entity_type = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class);
    $result = \Drupal::database()->query('
      SELECT *
      FROM {' . $entity_type . '_source}
      WHERE `' . $id_field . '` = :id
      AND `feed_reference__target_revision_id` IN (
          SELECT MAX(feed_reference__target_revision_id)
          FROM {' . $entity_type . '_source}
          GROUP BY `' . $id_field . '`
      )
    ', [':id' => $id])->fetch(\PDO::FETCH_ASSOC);
    unset($result['feed_reference__target_id']);
    unset($result['feed_reference__target_revision_id']);
    return $result;
  }
}
