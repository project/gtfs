<?php

namespace Drupal\gtfs\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the GTFS Frequency entity.
 *
 * @ingroup gtfs
 *
 * @ContentEntityType(
 *   id = "gtfs_frequency",
 *   label = @Translation("GTFS Frequency"),
 *   handlers = {
 *     "storage" = "Drupal\gtfs\FrequencyStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gtfs\FrequencyListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "controller" = "Drupal\gtfs\Controller\FrequencyController",
 *     "form" = {
 *       "default" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "add" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "edit" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "revision_revert" = "Drupal\gtfs\Form\FrequencyRevisionRevertForm",
 *       "revision_delete" = "\Drupal\gtfs\Form\FrequencyRevisionDeleteForm",
 *       "translation_revert" = "\Drupal\gtfs\Form\FrequencyRevisionRevertTranslationForm",
 *       "settings" = "Drupal\gtfs\Form\FrequencySettingsForm",
 *     },
 *     "access" = "Drupal\gtfs\GTFSObjectAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gtfs\GTFSEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gtfs_frequency",
 *   data_table = "gtfs_frequency_field_data",
 *   revision_table = "gtfs_frequency_revision",
 *   revision_data_table = "gtfs_frequency_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer gtfs frequency entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *    revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message"
 *   },
 *   links = {
 *     "canonical" = "/gtfs/frequency/{gtfs_frequency}",
 *     "add-form" = "/admin/gtfs/frequency/add",
 *     "edit-form" = "/admin/gtfs/frequency/{gtfs_frequency}/edit",
 *     "delete-form" = "/admin/gtfs/frequency/{gtfs_frequency}/delete",
 *     "version-history" = "/admin/gtfs/frequency/{gtfs_frequency}/revisions",
 *     "revision" = "/admin/gtfs/frequency/{gtfs_frequency}/revisions/{gtfs_frequency_revision}/view",
 *     "revision_revert" = "/admin/gtfs/frequency/{gtfs_frequency}/revisions/{gtfs_frequency_revision}/revert",
 *     "revision_delete" = "/admin/gtfs/frequency/{gtfs_frequency}/revisions/{gtfs_frequency_revision}/delete",
 *     "translation_revert" = "/admin/gtfs/frequency/{gtfs_frequency}/revisions/{gtfs_frequency_revision}/revert/{langcode}",
 *     "collection" = "/admin/gtfs/frequency",
 *   },
 *   field_ui_base_route = "gtfs_frequency.settings"
 * )
 */
class Frequency extends GTFSEntityBase {

    /**
   * {@inheritdoc}
   */
  public static function schema(): array {
    return [
      'fields' => [
        'trip_id' => [
          'type' => 'varchar',
          'length' => 128,
          'default' => '',
        ],
        'start_time' => ['type' => 'varchar', 'length' => 8, 'default' => ''],
        'end_time' => ['type' => 'varchar', 'length' => 8, 'default' => ''],
        'headway_secs' => ['type' => 'numeric', 'precision' => 9, 'scale' => 6],
        'exact_times' => ['type' => 'int', 'length' => 2],
      ],
      'primary key' => ['trip_id', 'headway_secs'],
    ];
  }

  /**
   * Returns the trip associated with this stoptime.
   *
   * @return \Drupal\gtfs\Entity\Trip
   *   The associated trip
   */
  public function trip() {
    $trip_id = $this->get('trip_id')->target_id;
    return Trip::load($trip_id);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    $this->setName($this->trip()->getName() . ' ' . $this->get('headway_secs')->value . ' seconds');
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('This will be automatically populated from the other fields'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE)
      ->setReadOnly(TRUE);

    $fields['trip_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Trip ID'))
      ->setDescription(t('Identifies a trip to which the specified headway of service applies.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'target_type' => 'gtfs_trip',
        'default_value' => 0,
        'handler' => 'default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['start_time'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Start Time'))
      ->setDescription(t('Time at which the first vehicle departs from the first stop of the trip with the specified headway.'))
    // ->setSetting('datetime_type', 'date')
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'weight' => -3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['end_time'] = BaseFieldDefinition::create('string')
      ->setLabel(t('End Time'))
      ->setDescription(t('Time at which service changes to a different headway (or ceases) at the first stop in the trip.'))
    // ->setSetting('datetime_type', 'date')
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'weight' => -3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['headway_secs'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Headway Seconds'))
      ->setDescription(t('Time, in seconds, between departures from the same stop (headway) for the trip, during the time interval specified by start_time and end_time. Multiple headways for the same trip are allowed, but may not overlap. New headways may start at the exact time the previous headway ends.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string'
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -3
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['exact_times'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Exact Times'))
      ->setDescription(t('Indicates the type of service for a trip.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue('0')
      ->setSettings([
        'allowed_values' => [
          '0' => 'Frequency-Based Trips',
          '1' => 'Schedule-based trips with the exact same headway throughout the day. ',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    return $fields;
  }

}
