<?php

namespace Drupal\gtfs\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the GTFS Stop entity.
 *
 * @ingroup gtfs
 *
 * @ContentEntityType(
 *   id = "gtfs_stop",
 *   label = @Translation("GTFS Stop"),
 *   handlers = {
 *     "storage" = "Drupal\gtfs\StopStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gtfs\StopListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "controller" = "Drupal\gtfs\Controller\StopController",
 *     "form" = {
 *       "default" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "add" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "edit" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "revision_revert" = "Drupal\gtfs\Form\StopRevisionRevertForm",
 *       "revision_delete" = "\Drupal\gtfs\Form\StopRevisionDeleteForm",
 *       "translation_revert" = "\Drupal\gtfs\Form\StopRevisionRevertTranslationForm",
 *       "settings" = "Drupal\gtfs\Form\StopSettingsForm",
 *     },
 *     "access" = "Drupal\gtfs\GTFSObjectAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gtfs\GTFSEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gtfs_stop",
 *   data_table = "gtfs_stop_field_data",
 *   revision_table = "gtfs_stop_revision",
 *   revision_data_table = "gtfs_stop_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer gtfs stop entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "stop_name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *    revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message"
 *   },
 *   links = {
 *     "canonical" = "/gtfs/stop/{gtfs_stop}",
 *     "add-form" = "/admin/gtfs/stop/add",
 *     "edit-form" = "/admin/gtfs/stop/{gtfs_stop}/edit",
 *     "delete-form" = "/admin/gtfs/stop/{gtfs_stop}/delete",
 *     "version-history" = "/admin/gtfs/stop/{gtfs_stop}/revisions",
 *     "revision" = "/admin/gtfs/stop/{gtfs_stop}/revisions/{gtfs_stop_revision}/view",
 *     "revision_revert" = "/admin/gtfs/stop/{gtfs_stop}/revisions/{gtfs_stop_revision}/revert",
 *     "revision_delete" = "/admin/gtfs/stop/{gtfs_stop}/revisions/{gtfs_stop_revision}/delete",
 *     "translation_revert" = "/admin/gtfs/stop/{gtfs_stop}/revisions/{gtfs_stop_revision}/revert/{langcode}",
 *     "collection" = "/admin/gtfs/stop",
 *   },
 *   field_ui_base_route = "gtfs_stop.settings"
 * )
 */
class Stop extends GTFSEntityBase implements GeodataInterface {
  use HasIdTrait;

  public static $resourceURL = '/gtfs/api/v1/stops';

  /**
   * {@inheritdoc}
   */
  public static function schema(): array {
    return [
      'fields' => [
        'stop_id' => ['type' => 'varchar', 'length' => 128, 'not null' => TRUE],
        'stop_code' => ['type' => 'varchar', 'length' => 255],
        'stop_name' => ['type' => 'varchar', 'length' => 255],
        'stop_desc' => ['type' => 'text', 'length' => 4096],
        'stop_lat' => ['type' => 'numeric', 'precision' => 9, 'scale' => 6],
        'stop_lon' => ['type' => 'numeric', 'precision' => 9, 'scale' => 6],
        'zone_id' => ['type' => 'varchar', 'length' => 255],
        'stop_url' => ['type' => 'varchar', 'length' => 255],
        'location_type' => ['type' => 'int', 'length' => 2],
        'parent_station' => ['type' => 'varchar', 'length' => 128, 'default' => ''],
        'stop_timezone' => ['type' => 'varchar', 'length' => 255],
        'wheelchair_boarding' => ['type' => 'int', 'length' => 2],
      ],
      'primary key' => ['stop_id'],
      'foreign keys' => [
        'stop' => [
          'table' => 'gtfs_stop',
          'columns' => ['parent_station' => 'id'],
        ],
      ],
    ];
  }

  public function routes() {
    return Route::loadMultiple(\Drupal::database()->query(
      "SELECT `route_id`
       FROM {gtfs_trip_field_data}
       WHERE `id` IN (
         SELECT `trip_id`
         FROM {gtfs_stop_time_field_data}
         WHERE `stop_id` = :stop_id
       )",
      [':stop_id' => $this->id()]
    )->fetchAll(\PDO::FETCH_COLUMN));
  }

  public function geojson(): array {
    return [
      'type' => 'Point',
      'coordinates' => [$this->get('stop_lon')->value, $this->get('stop_lat')->value]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function resourceUrl(string $append = ''): string {
    $link = self::$resourceURL . '/' . $this->get('stop_id')->value;
    $url = \Drupal::request()->getSchemeAndHttpHost() . $link;
    $url .= $append;
    \Drupal::moduleHandler()->alter('gtfs_entity_resource_url', $url, $this);
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function links(): array {
    $links = [
      'routes' => $this->resourceUrl('/routes'),
      'trips' => $this->resourceUrl('/trips'),
      'stopTimes' => $this->resourceUrl('/stopTimes'),
      'directions' => $this->resourceUrl('/directions'),
    ];
    $links['children'] = array_values(array_map(function ($stop) {
      return $stop->resourceUrl();
    }, $this->children()));
    if ($this->hasParent()) {
      $links['parent'] = $this->parent()->resourceUrl();
    }
    return array_merge(parent::links(), $links);
  }

  public function toGTFSObject(): array {
    $obj = parent::toGTFSObject();
    if ($this->hasParent()) {
      $obj['parent_station'] = $this->parent()->get('stop_id')->value;
    }
    return $obj;
  }

  public function hasParent() {
    return isset($this->get('parent_station')->target_id);
  }

  public function parent() {
    if ($this->hasParent()) {
      return Stop::load($this->get('parent_station')->target_id);
    }
    return false;
  }

  public function children() {
    $child_ids = \Drupal::database()->query(
      'SELECT `id`
      FROM {gtfs_stop_field_data}
      WHERE `parent_station` = :this_id',
      [':this_id' => $this->id()]
    )->fetchAll(\PDO::FETCH_COLUMN);
    return Stop::loadMultiple($child_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('stop_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('stop_name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['stop_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The stop_name field contains the name of a stop, station, or station entrance. Please use a name that people will understand in the local and tourist vernacular.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['stop_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stop ID'))
      ->setDescription(t('The stop_id field contains an ID that uniquely identifies a stop, station, or station entrance. Multiple routes may use the same stop. The stop_id is used by systems as an internal identifier of this record (e.g., primary key in database), and therefore the stop_id must be dataset unique.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['stop_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stop code'))
      ->setDescription(t('The stop_code field contains short text or a number that uniquely identifies the stop for passengers. Stop codes are often used in phone-based transit information systems or printed on stop signage to make it easier for riders to get a stop schedule or real-time arrival information for a particular stop. The stop_code field contains short text or a number that uniquely identifies the stop for passengers. The stop_code can be the same as stop_id if it is passenger-facing. This field should be left blank for stops without a code presented to passengers.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['stop_desc'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Stop description'))
      ->setDescription(t('The stop_desc field contains a description of a stop. Please provide useful, quality information. Do not simply duplicate the name of the stop.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['stop_lat'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Stop Lat'))
      ->setDescription(t('The stop_lat field contains the latitude of a stop, station, or station entrance. The field value must be a valid WGS 84 latitude.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'precision' => 10,
        'scale' => 6
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'number_decimal',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['stop_lon'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Stop Lon'))
      ->setDescription(t('The stop_lon field contains the longitude of a stop, station, or station entrance. The field value must be a valid WGS 84 longitude value from -180 to 180.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'precision' => 10,
        'scale' => 6
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'number_decimal',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['zone_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Zone ID'))
      ->setDescription(t('The zone_id field defines the fare zone for a stop ID. Zone IDs are required if you want to provide fare information using fare_rules.txt. If this stop ID represents a station, the zone ID is ignored.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['stop_url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Stop URL'))
      ->setDescription(t('The stop_url field contains the URL of a web page about a particular stop. This should be different from the agency_url and the route_url fields. The value must be a fully qualified URL that includes https:// or https://, and any special characters in the URL must be correctly escaped. See http://www.w3.org/Addressing/URL/4_URI_Recommentations.html for a description of how to create fully qualified URL values.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['location_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Location Type'))
      ->setDescription(t('The location_type field identifies whether this stop ID represents a stop, station, or station entrance. If no location type is specified, or the location_type is blank, stop IDs are treated as stops. Stations may have different properties from stops when they are represented on a map or used in trip planning.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue('0')
      ->setSettings([
        'allowed_values' => [
          '0' => 'Stop',
          '1' => 'Station',
          '2' => 'Station Entrance/Exit',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    // TODO this field may be funky
    $fields['parent_station'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent Station'))
      ->setDescription(t('For stops that are physically located inside stations, the parent_station field identifies the station associated with the stop. To use this field, stops.txt must also contain a row where this stop ID is assigned location type=1 (Station).'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'target_type' => 'gtfs_stop',
        'default_value' => 0,
        'handler' => 'default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -3,
        'settings' => [
          // TODO How to limit this to only stops which are Stations?
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['stop_timezone'] = BaseFieldDefinition::create('tzfield')
      ->setLabel(t('Stop Timezone'))
      ->setDescription(t('The stop_timezone field contains the timezone in which this stop, station, or station entrance is located. Please refer to Wikipedia List of Timezones for a list of valid values. If omitted, the stop should be assumed to be located in the timezone specified by agency_timezone in agency.txt. When a stop has a parent station, the stop is considered to be in the timezone specified by the parent station\'s stop_timezone value. If the parent has no stop_timezone value, the stops that belong to that station are assumed to be in the timezone specified by agency_timezone, even if the stops have their own stop_timezone values. In other words, if a given stop has a parent_station value, any stop_timezone value specified for that stop must be ignored. Even if stop_timezone values are provided in stops.txt, the times in stop_times.txt should continue to be specified as time since midnight in the timezone specified by agency_timezone in agency.txt. This ensures that the time values in a trip always increase over the course of a trip, regardless of which timezones the trip crosses.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['wheelchair_boarding'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Wheelchair Boarding'))
      ->setDescription(t('The wheelchair_boarding field identifies whether wheelchair boardings are possible from the specified stop, station, or station entrance. '))
      ->setRevisionable(TRUE)
      ->setDefaultValue('0')
      ->setSettings([
        'allowed_values' => [
          '0' => 'No information available',
          '1' => 'Yes',
          '2' => 'No',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    return $fields;
  }

}
