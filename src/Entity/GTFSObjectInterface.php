<?php

namespace Drupal\gtfs\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining GTFS entities.
 *
 * @ingroup gtfs
 */
interface GTFSObjectInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {


  /**
   * Returns the nominal schema for the GTFS object.
   *
   * When importing from a GTFS feed, we need to know the structure of the
   * temporary table, i.e. the file from which we are importing. This method
   * must be implemented on each class that extends this interface so that
   * we can import it properly.
   *
   * @return array
   */
  public static function schema(): array;


  /**
   * Returns object as a GTFS key-value arry.
   *
   * This function should return a key-value array
   * representing the instance as a GTFS object.
   *
   * @return array
   */
  public function toGTFSObject(): array;

  /**
   * This function should return a CSV row
   * representing the instance.
   *
   * @return string
   */
  public function toGTFSRow(): string;

  /**
   * Generates links to related objects a la HATEOAS.
   *
   * @return array
   */
  public function links(): array;


  /**
   * Returns a link to itself.
   *
   * @param string $append
   *  A string to append to the link, e.g. for descendant paths.
   *
   * @return string
   */
  public function resourceUrl(string $append): string;


  /**
   * BELOW THIS POINT methods pertain to the ContentEntityInterface
   * or other extended interfaces
   */

   /**
    * Gets the GTFS Entity name.
    *
    * @return string
    *   Name of the GTFS Entity.
    */
   public function getName();

   /**
    * Sets the GTFS Entity name.
    *
    * @param string $name
    *   The GTFS Entity name.
    *
    * @return \Drupal\gtfs\Entity\GTFSObjectInterface
    *   The called GTFS Entity entity.
    */
   public function setName($name);

   /**
    * Gets the GTFS Entity creation timestamp.
    *
    * @return int
    *   Creation timestamp of the GTFS Entity.
    */
   public function getCreatedTime();

   /**
    * Sets the GTFS Entity creation timestamp.
    *
    * @param int $timestamp
    *   The GTFS Entity creation timestamp.
    *
    * @return \Drupal\gtfs\Entity\GTFSObjectInterface
    *   The called GTFS Entity entity.
    */
   public function setCreatedTime($timestamp);

   /**
    * Returns the GTFS Entity published status indicator.
    *
    * Unpublished GTFS Entity are only visible to restricted users.
    *
    * @return bool
    *   TRUE if the GTFS Entity is published.
    */
   public function isPublished();

   /**
    * Sets the published status of a GTFS Entity.
    *
    * @param bool $published
    *   TRUE to set this GTFS Entity to published, FALSE to set it to unpublished.
    *
    * @return \Drupal\gtfs\Entity\GTFSObjectInterface
    *   The called GTFS Entity entity.
    */
   public function setPublished($published);

   /**
    * Gets the GTFS Entity revision creation timestamp.
    *
    * @return int
    *   The UNIX timestamp of when this revision was created.
    */
   public function getRevisionCreationTime();

   /**
    * Sets the GTFS Entity revision creation timestamp.
    *
    * @param int $timestamp
    *   The UNIX timestamp of when this revision was created.
    *
    * @return \Drupal\gtfs\Entity\GTFSObjectInterface
    *   The called GTFS Entity entity.
    */
   public function setRevisionCreationTime($timestamp);

   /**
    * Gets the GTFS Entity revision author.
    *
    * @return \Drupal\user\UserInterface
    *   The user entity for the revision author.
    */
   public function getRevisionUser();

   /**
    * Sets the GTFS Entity revision author.
    *
    * @param int $uid
    *   The user ID of the revision author.
    *
    * @return \Drupal\gtfs\Entity\GTFSObjectInterface
    *   The called GTFS Entity entity.
    */
   public function setRevisionUserId($uid);
}
