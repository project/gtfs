<?php

namespace Drupal\gtfs\Entity\Feed;

use Drupal\Core\File\FileSystemInterface;
use Drupal\gtfs\Entity\FeedInterface;
use Drupal\Core\Archiver\Zip;

/**
 * Provides the ability to download from a GTFS feed.
 */
class Downloader {

  const MSG_FILE_DOWNLOADING = 'Downloading @filename';
  const MSG_FILE_EXISTS = '@filename has already been downloaded. Skipping.';
  const MSG_FILE_DOWNLOADED = '@filename downloaded to private files.';
  const MSG_ERR_FILE_DOWNLOAD = 'File could not be downloaded. Check your configuration and log messages';
  const MSG_FILE_EXTRACTED = 'Extracted @directory and removed @filename';

  /**
   * The feed which is being downloaded.
   *
   * @var \Drupal\gtfs\Entity\FeedInterface
   */
  private $feed;

  /**
   * The file that was downloaded.
   *
   * @var string
   */
  private $downloadedFile = '';

  /**
   * The location of the files after download and extraction.
   *
   * @var string
   */
  private $extractedDirectory = '';

  /**
   * Constructs a new importer instance.
   *
   * @param \Drupal\gtfs\Entity\FeedInterface $feed
   *   A Feed object.
   */
  public function __construct(FeedInterface $feed) {
    $this->feed = $feed;
    if (file_exists($this->getFileDownloadLocation())) {
      $this->downloadedFile = $this->getFileDownloadLocation();
    }
    if (!is_dir($this->getExtractedFileDirectory())) {
      mkdir($this->getExtractedFileDirectory());
    }
    $this->extractedDirectory = $this->getExtractedFileDirectory();
  }

  /**
   *
   */
  public function batch(): array {
    return [
      'title' => t(static::MSG_FILE_DOWNLOADING, [
        '@filename' => $this->getFeedFilename(),
      ]),
      'operations' => [
        [[$this, 'download'], []],
        [[$this, 'extract'], []],
      ],
      /** @see \Drupal\gtfs\Entity\Feed\Downloader::batchFinished */
      'finished' => [$this, 'batchFinished'],
    ];
  }

  /**
   * Empty implementation of batch finish function.
   */
  public function batchFinished() {}

  /**
   * Downloads the GTFS feed file.
   *
   * @param array $context
   *   The batch context array.
   *
   * @return boolean
   *  Whether or not the operation succeeded
   */
  public function download(&$context) {
    if ($this->downloadedFile) {
      $context['message'] = t(static::MSG_FILE_EXISTS, ['@filename' => $this->downloadedFile]);
      return TRUE;
    }

    $url = $this->getFeedUrl();
    $destination = $this->getFileDownloadLocation();
    $replace = FileSystemInterface::EXISTS_REPLACE;

    try {
      $parsed_url = parse_url($url);
      /** @var \Drupal\Core\File\FileSystemInterface $file_system */
      $file_system = \Drupal::service('file_system');
      if (is_dir($file_system->realpath($destination))) {
        // Prevent URIs with triple slashes when glueing parts together.
        $path = str_replace('///', '//', "$destination/") . \Drupal::service('file_system')->basename($parsed_url['path']);
      }
      else {
        $path = $destination;
      }
      $data = (string) \Drupal::httpClient()
        ->get($url, [
          'timeout' => 30,
        ])
        ->getBody();
      $this->downloadedFile =  $file_system->saveData($data, $path, $replace);
//      $this->downloadedFile = system_retrieve_file($url, $destination, $managed, $replace);
    } catch (\Exception $e) {
      \Drupal::logger('gtfs')->error($e->getMessage());
    }

    if (!$this->downloadedFile) {
      // An error will be thrown by system_retrieve_file.
      return FALSE;
    }

    $context['message'] = t(static::MSG_FILE_DOWNLOADED, ['@filename' => $destination]);
    return TRUE;
  }


  /**
   * Extracts the current feed zip file.
   *
   * @param array $context
   *  Batch contact
   *
   * @return null
   *
   * @throws \Exception
   * @throws \Drupal\Core\Archiver\ArchiverException
   */
  public function extract(&$context) {
    if (!$this->downloadedFile) {
      $downloaded = $this->download($context);
      if (!$downloaded) {
        throw new \Exception(static::MSG_ERR_FILE_DOWNLOAD);
      }
    }

    $zip = new Zip($this->downloadedFile);
    $zip->extract($this->extractedDirectory);
    $inner_zip_folder = str_replace('.zip', '', $this->getFeedFilename());
    if (is_dir("{$this->extractedDirectory}/{$inner_zip_folder}")) {
      /** @var FileSystemInterface $fs */
      $fs = \Drupal::service('file_system');
      $files_in_inner_zip_folder = $fs->scanDirectory("{$this->extractedDirectory}/{$inner_zip_folder}", '/.*/');
      foreach ($files_in_inner_zip_folder as $file) {
        $new_uri = str_replace(
          "{$this->extractedDirectory}/{$inner_zip_folder}",
          "{$this->extractedDirectory}",
          $file->uri
        );
        $fs->move($file->uri, $new_uri);
      }
    }
    unlink($this->downloadedFile);
    $context['message'] = t(static::MSG_FILE_EXTRACTED, [
      '@directory' => $this->extractedDirectory,
      '@filename' => $this->downloadedFile,
    ]);
    $this->downloadedFile = '';
    return null;
  }


  /**
   * Removes the specified file.
   *
   * @param $filename
   */
  public function removeFile($filename) {
    $filepath = "{$this->extractedDirectory}/{$filename}.txt";
    if (is_file($filepath)) {
      unlink($filepath);
    }
  }


  /**
   * Removes the current directory.
   */
  public function removeDirectory() {
    $files = \Drupal::service('file_system')->scanDirectory($this->extractedDirectory, '/.*/');
    foreach ($files as $file) {
      unlink($file->uri);
    }
    rmdir($this->extractedDirectory);
  }

  /**
   *
   */
  private function getFeedUrl(): string {
    return $this->feed->get('feed_url')->value;
  }

  /**
   *
   */
  private function getFeedFilename(): string {
    return basename($this->getFeedUrl());
  }

  /**
   *
   */
  private function getFileDownloadLocation(): string {
    $base = \Drupal::service('file_system')->realpath('private://');
    $filename = $this->getFeedFilename();
    return "{$base}/{$filename}";
  }

  /**
   *
   */
  public function getExtractedFileDirectory(): string {
    /** @var FileSystemInterface $filesystem */
    $filesystem = \Drupal::service('file_system');
    $base = $filesystem->realpath('private://');
    $dirname = str_replace('.zip', '', $this->getFeedFilename());
    return "{$base}/{$dirname}";
  }

}
