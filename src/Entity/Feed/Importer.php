<?php

namespace Drupal\gtfs\Entity\Feed;

use Drupal\Component\Uuid\Php as UUID;
use Drupal\gtfs\Entity\Feed;
use Drupal\gtfs\Entity\FeedInterface;
use Drupal\core\Url;
use Drupal\Core\Database\Database;

/**
 *
 */
class Importer {

  public $id;

  public bool $full;

  private $downloader;

  public $filesImporting = [];

  const DEFAULT_LANGCODE = 'en';

  const DEFAULT_REVISION = '1';

  const DEFAULT_USER = '1';

  const DEFAULT_LOG_MESSAGE = '';


  const BATCH_SIZE = 5000;
  const SPL_FLAGS = \SplFileObject::DROP_NEW_LINE | \SplFileObject::READ_CSV | \SplFileObject::READ_AHEAD | \SplFileObject::SKIP_EMPTY;

  /**
   * Constructs a new instance of the importer based on the database.
   *
   * @param string|int $importId
   *   The id of the gtfs_feed_imports row.
   */
  public function __construct($importId, $full = TRUE) {
    $this->id = $importId;
    $this->full = $full;
  }

  /**
   * Overloads the __get function to get values from database.
   *
   * @param $name
   *  The name of the value to get
   */
  public function __get($name) {
    if ($name === 'feed') {
      return $this->getFeed();
    }
    $db_entry = \Drupal::database()->query(
      'SELECT *
       FROM {gtfs_feed_imports}
       WHERE `id` = :id',
       [':id' => $this->id]
    )->fetch(\PDO::FETCH_OBJ);
    $db_entry->files = unserialize($db_entry->files);
    $db_entry->completed = unserialize($db_entry->completed);
    if (isset($db_entry->{$name})) {
      return $db_entry->{$name};
    }
    $trace = debug_backtrace();
    trigger_error(
      'Undefined property via __get(): ' . $name .
      ' in ' . $trace[0]['file'] .
      ' on line ' . $trace[0]['line'],
      E_USER_NOTICE);
  }

  /**
   * Overloads the set function to set the completed value in database.
   *
   * @param $name
   * @param $value
   */
  public function __set($name, $value) {
    if ($name === 'completed') {
      \Drupal::database()
        ->update('gtfs_feed_imports')
        ->fields([
          'completed' => serialize($value),
        ])
        ->condition('id', $this->id)
        ->execute();
    }
  }

  public function updateTransformCount(string $entity_type, string $transform_type, int $count) {
    $completed = $this->completed;
    if (empty($completed[$entity_type])) $completed[$entity_type] = [];
    if (empty($completed[$entity_type][$transform_type])) $completed[$entity_type][$transform_type] = 0;
    $completed[$entity_type][$transform_type] += $count;
    $this->completed = $completed;
  }

  /**
   * Given the location of a file,
   * counts the number of records (lines - 1)
   *
   * @param $filepath
   *  The path of the downloaded ZIP file in which to count records
   *
   * @return string
   */
  public static function countRecordsInFile($filepath): string {
    $splFile = new \SplFileObject($filepath);
    $splFile->setFlags(static::SPL_FLAGS);
    $splFile->seek($splFile->getSize());
    $records_in_file = number_format($splFile->key() - 1);
    return $records_in_file;
  }

  /**
   * Returns the feed from which entities are being imported.
   *
   * @return \Drupal\gtfs\Entity\FeedInterface
   *   The feed associated with the import.
   */
  public function getFeed(): FeedInterface {
    $feedId = \Drupal::database()->query(
      'SELECT `feed_id`
       FROM {gtfs_feed_imports}
       WHERE `id` = :id',
      [':id' => $this->id]
    )->fetch(\PDO::FETCH_COLUMN);
    return Feed::load($feedId);
  }

  /**
   * Gets the order of files to import.
   *
   * This function returns the order of files
   * to import, as specified by this and any
   * other extending modules.
   *
   * e.g. HOOK_gtfs_import_order_alter
   *
   * @return array
   *   An array of filenames, in order.
   */
  public static function getImportOrder(): array {
    $import_order = [];
    \Drupal::moduleHandler()->alter('gtfs_import_order', $import_order);
    return $import_order;
  }

  /**
   * Gets the map of which entities handle which files.
   *
   * This function returns class handlers for each file
   * in the GTFS file. Classes must implement the
   * \Drupal\gtfs\Entity\GTFSObjectInterface. This list
   * may be modified by other modules.
   *
   * e.g. HOOK_gtfs_import_entities_alter
   *
   * @return array
   *   An array keyed by filename, valued by class
   */
  public static function getFileEntityMap(): array {
    $file_entity_map = [];
    \Drupal::moduleHandler()->alter('gtfs_import_entities', $file_entity_map);
    return $file_entity_map;
  }

  /**
   * Gets a feed reference object to attach to imported entities.
   *
   * @return array
   *   The feed reference.
   */
  public function getFeedReference(): array {
    return [
      'target_id' => $this->feed->id(),
      'target_revision_id' => $this->feed->getRevisionId(),
    ];
  }

  /**
   * Builds the batch that creates temporary tables.
   *
   * Before inserting into the database, the importer
   * creates a temporary table for each file and in-
   * serts the data into them.
   *
   * @return array
   *   A \Drupal batch description
   *
   * @see https://www.drupal.org/docs/7/api/batch-api
   */
  public function batchPrepare($include_next_url = TRUE): array {
    $operations = [];

    $file_entity_map = static::getFileEntityMap();
    $downloader = new Downloader($this->feed);

    $files = $this->files;

    foreach ($files as $file) {
      // $class = $file_entity_map[$file];.
      $filepath = "{$downloader->getExtractedFileDirectory()}/{$file}.txt";
      // In case the file doesn't end in a hard return.
      $command = 'sed -i.bak -e "$a" ' . $filepath;
      passthru($command);
      $this->log("Ran `" . $command . '`', 'shell');
      $command = 'sed -i.bak "1 s/^/feed_reference__target_id,feed_reference__target_revision_id,/" ' . $filepath;
      passthru($command);
      $this->log("Ran `" . $command . '`', 'shell');
      // Add feed id and revision id to the end of each line
      $append = "{$this->getFeed()->id()},{$this->getFeed()->getRevisionId()},";
      $command = 'sed -i.bak "2,$ s/^/' . $append . '/" ' . $filepath;
      passthru($command);
      $this->log("Ran `" . $command . '`', 'shell');
      $operations[] = [
        [$this, 'sourceTableClean'],
        [$file, $this->getFeed()->id(), $this->getFeed()->getRevisionId()]
      ];
      $operations[] = [
        [$this, 'sourceTableLoad'],
        [$file],
      ];
      if ($this->full) {
        $operations[] = [
          [$this, 'tempTableCreate'],
          [$file],
        ];
        $operations[] = [
          [$this, 'tempTableLoad'],
          [$file],
        ];
      }
    }

    foreach ($files as $file) {
      $operations[] = [
        [$this, 'sourceTableAlter'],
        [$file, $this->getFeed()->id(), $this->getFeed()->getRevisionId()]
      ];
      if ($this->full) {
        $operations[] = [
          [$this, 'tempTableAlter'],
          [$file, $this->getFeed()->id(), $this->getFeed()->getRevisionId()]
        ];
        $operations[] = [
          [$this, 'tempTableHash'],
          [$file],
        ];
        $operations[] = [
          [$this, 'tempTableIndexHash'],
          [$file],
        ];
      }
    }

    \Drupal::moduleHandler()->alter('gtfs_prepare_post_source_table', $operations, $this->feed);

    $batch = [
      'title' => t('Preparing entities'),
      'operations' => $operations,
      'finished' => [$this, 'batchPrepareFinished'],
    ];

    if ($include_next_url) {
      if ($this->full) {
        $batch['next_url'] = Url::fromRoute('entity.gtfs_feed.import_form', [
          'gtfs_feed' => $this->feed->id(),
          'step' => 'transform',
          'class' => base64_encode($file_entity_map[reset($files)]),
        ]);
      } else {
        $batch['next_url'] = Url::fromRoute('entity.gtfs_feed.canonical', [
          'gtfs_feed' => $this->feed->id(),
        ]);
      }
    }

    return $batch;
  }

  private function feedReferenceColumns() {
    return [
      'feed_reference__target_id' => [
        'type' => 'int',
        'length' => 11,
      ],
      'feed_reference__target_revision_id' => [
        'type' => 'int',
        'length' => 11,
      ],
    ];
  }

  private function ensureSourceTable($class) {
    $db = \Drupal::database()->schema();
    $schema = $class::schema();
    $source_table_name = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class) . '_source';
    $schema['fields'] = array_merge($schema['fields'], $this->feedReferenceColumns());
    foreach ($schema['fields'] as &$field) {
      $field['not null'] = TRUE;
    }
    // Key this table to have unique indexes by feed and revision ID
    $schema['primary key'][] = 'feed_reference__target_id';
    $schema['primary key'][] = 'feed_reference__target_revision_id';
    if (!$db->tableExists($source_table_name)) {
      $db->createTable($source_table_name, $schema);
      $this->log($source_table_name . ' did not exist; created it.', 'db');
    }
    foreach ($schema['fields'] as $name => $definition) {
      if (!$db->fieldExists($source_table_name, $name)) {
        $db->addField($source_table_name, $name, $definition);
        $this->log($name . ' field did not exists on ' . $source_table_name . ', created it', 'db');
      }
    }
  }

  public function sourceTableClean($file, $target_id, $target_revision_id, &$context = []) {
    $file_entity_map = static::getFileEntityMap();
    $class = $file_entity_map[$file];
    $source_table_name = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class) . '_source';
    \Drupal::database()->delete($source_table_name)
      ->condition('feed_reference__target_id', $target_id)
//      ->condition('feed_reference__target_revision_id', $target_revision_id) // TODO remove this when we can query by revision ID
      ->execute();
    $context['message'] = $this->log("Cleaned $source_table_name to prevent collision", 'db');
  }

  public function sourceTableLoad(string $file, &$context = []) {
    $file_entity_map = static::getFileEntityMap();
    $class = $file_entity_map[$file];
    $source_table_name = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class) . '_source';
    $allowed_columns = array_merge(array_keys($this->feedReferenceColumns()), array_keys($class::schema()['fields']));
    $downloader = new Downloader($this->feed);
    $filepath = "{$downloader->getExtractedFileDirectory()}/{$file}.txt";
    $columns = explode(',', fgets(fopen($filepath, 'r')));
    $columns = array_map('trim', $columns);
    $columns = array_map(function ($column) use ($allowed_columns) {
      return in_array($column, $allowed_columns) ? $column : '@dummy';
    }, $columns);

    $this->ensureSourceTable($class);

    $columns = implode(',', $columns);

    $connection = \Drupal::database()->getConnectionOptions();
    $connection['pdo'][\PDO::MYSQL_ATTR_LOCAL_INFILE] = 1;
    Database::addConnectionInfo('extra', 'default', $connection);
    Database::setActiveConnection('extra');

    $terminator = EOLDetector::detectFileEOL($filepath);

    Database::getConnection()->query(
      "LOAD DATA LOCAL INFILE '{$filepath}'
       INTO TABLE {$source_table_name}
       FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'
       LINES TERMINATED BY '{$terminator}'
       IGNORE 1 LINES
       ({$columns});"
    )->execute();

    // Set connection back to default;.
    Database::setActiveConnection();

    $realpath = str_replace(\Drupal::service('file_system')->realpath('private://') . '/', '', $filepath);

    $context['message'] = $this->log("Imported {$realpath} into {$source_table_name}", 'db');
  }

  public function sourceTableAlter(string $file, $target_id, $target_revision_id, &$context = []) {
    $file_entity_map = static::getFileEntityMap();
    $class = $file_entity_map[$file];
    $source_table_name = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class) . '_source';
    \Drupal::moduleHandler()->invokeAll("{$source_table_name}_alter", [$source_table_name, $target_id, $target_revision_id]);
  }

  public function tempTableAlter(string $file, $target_id, $target_revision_id, &$context = []) {
    $file_entity_map = static::getFileEntityMap();
    $class = $file_entity_map[$file];
    $temp_table_name = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class) . '_temp';
    \Drupal::moduleHandler()->invokeAll("{$temp_table_name}_alter", [$temp_table_name, $target_id, $target_revision_id]);
  }


  /**
   * Creates a temporary table given a GTFS file.
   *
   * @param string $file
   *   A filename in the import.
   * @param array &$context
   *   The batch context.
   *
   * @see https://www.drupal.org/docs/7/api/batch-api
   */
  public function tempTableCreate(string $file, &$context) {
    $file_entity_map = static::getFileEntityMap();
    $class = $file_entity_map[$file];
    $temp_table_name = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class) . '_temp';
    $db = \Drupal::database();
    $db_schema = $db->schema();
    if ($db_schema->tableExists($temp_table_name)) {
      $db_schema->dropTable($temp_table_name);
      $this->log("Dropped {$temp_table_name}", 'db');
    }

    $schema = $class::schema();
    $schema['fields'] = array_merge($schema['fields'], $this->feedReferenceColumns(), [
      'hash' => ['type' => 'varchar', 'length' => 32],
      'index_hash' => ['type' => 'varchar', 'length' => 32],
      'langcode' => ['type' => 'varchar', 'length' => 12, 'default' => 'en'],
      'status' => ['type' => 'int', 'length' => 4, 'default' => 1],
      'revision_translation_affected' => ['type' => 'int', 'length' => 4, 'default' => 1],
      'default_langcode' => ['type' => 'int', 'length' => 4, 'default' => 1],
      'user_id' => ['type' => 'varchar', 'length' => 11, 'default' => \Drupal::currentUser()->id()],
    ]);

    $feedReference = $this->getFeedReference();
    $schema['fields']['feed_reference__target_id']['default'] = $feedReference['target_id'];
    $schema['fields']['feed_reference__target_revision_id']['default'] = $feedReference['target_revision_id'];

    // Null values break hashing.
    foreach ($schema['fields'] as &$field) {
      $field['not null'] = TRUE;
    }

    $db_schema->createTable($temp_table_name, $schema);
    $context['message'] = $this->log("Created {$temp_table_name} table", 'db');
  }

  /**
   * Loads data into a temporary table from a file.
   *
   * @param string $file
   *   A filename in the import.
   * @param array &$context
   *   The batch context.
   *
   * @see https://www.drupal.org/docs/7/api/batch-api
   */
  public function tempTableLoad(string $file, &$context) {
    $file_entity_map = static::getFileEntityMap();
    $class = $file_entity_map[$file];
    $allowed_columns = array_merge(array_keys($class::schema()['fields']), array_keys($this->feedReferenceColumns()));
    $downloader = new Downloader($this->feed);
    $filepath = "{$downloader->getExtractedFileDirectory()}/{$file}.txt";
    $columns = explode(',', fgets(fopen($filepath, 'r')));
    $columns = array_map('trim', $columns);
    $columns = array_map(function ($column) use ($allowed_columns) {
      return in_array($column, $allowed_columns) ? $column : '@dummy';
    }, $columns);
    $columns = implode(',', $columns);

    $temp_table_name = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class) . '_temp';

    $connection = \Drupal::database()->getConnectionOptions();
    $connection['pdo'][\PDO::MYSQL_ATTR_LOCAL_INFILE] = 1;
    Database::addConnectionInfo('extra', 'default', $connection);
    Database::setActiveConnection('extra');

    $terminator = EOLDetector::detectFileEOL($filepath);

    Database::getConnection()->query(
      "LOAD DATA LOCAL INFILE '{$filepath}'
       INTO TABLE {$temp_table_name}
       FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'
       LINES TERMINATED BY '{$terminator}'
       IGNORE 1 LINES
       ({$columns});"
    )->execute();

    // Set connection back to default;.
    Database::setActiveConnection();

    $realpath = str_replace(\Drupal::service('file_system')->realpath('private://') . '/', '', $filepath);

    $context['message'] = $this->log("Imported {$realpath} into {$temp_table_name}", 'db');
  }

  /**
   * Hashes a temporary table of entities.
   *
   * Hashing an object's data allows us to see if it has changed
   * since last import.
   *
   * @param string $file
   *   A filename in the import.
   * @param array &$context
   *   The batch context.
   *
   * @see https://www.drupal.org/docs/7/api/batch-api
   */
  public function tempTablehash(string $file, &$context) {
    $file_entity_map = static::getFileEntityMap();
    $class = $file_entity_map[$file];

    $temp_table_name = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class) . '_temp';

    $schema = $class::schema();

    $cast_fields = [];
    foreach (array_keys($schema['fields']) as $key) {
      $cast_fields[] = "CAST({$key} as CHAR)";
    }

    $cast_fields = implode(',', $cast_fields);

    \Drupal::database()->query(
      "UPDATE `{$temp_table_name}`
       SET `hash` = CRC32(CONCAT({$cast_fields}));"
    );

    $context['message'] = $this->log("Hashed {$temp_table_name}", 'hash');
  }

  /**
   * Index hashes data a temporary table of entities.
   *
   * Index hashing an object allows us to identify which objects
   * persist even if data has changed.
   *
   * @param string $file
   *   A filename in the import.
   * @param array &$context
   *   The batch context.
   *
   * @see https://www.drupal.org/docs/7/api/batch-api
   */
  public function tempTableIndexHash(string $file, &$context) {
    $file_entity_map = static::getFileEntityMap();
    $class = $file_entity_map[$file];

    $temp_table_name = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class) . '_temp';

    $schema = $class::schema();

    $index_hash_fields = implode(',', array_merge($schema['primary key'], ['feed_reference__target_id']));

    \Drupal::database()->query(
      "UPDATE {{$temp_table_name}}
       SET `index_hash` = CRC32(CONCAT({$index_hash_fields}));"
    );

    $context['message'] = $this->log("Hashed index of {$temp_table_name}", 'hash');
  }

  /**
   * @param $success
   * @param $results
   * @param $operations
   *
   * @see https://www.drupal.org/docs/7/api/batch-api
   */
  public function batchPrepareFinished($success, $results, $operations) {}

  /**
   * Builds a batch given a single entity type.
   *
   * @param string $class
   *   The class for which to build the batch.
   *
   * @return array
   *   A \Drupal batch description.
   *
   * @see https://www.drupal.org/docs/7/api/batch-api
   */
  public function batchTransform(string $class, $include_next_url = TRUE): array {
    $entity_type = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class);
    $db = \Drupal::database();
    $operations = [];
    $feedReference = $this->getFeedReference();
    $file_entity_map = static::getFileEntityMap();

    $new_count = $db->query(
      "SELECT count(t.index_hash)
       FROM {{$entity_type}_temp} `t`
        WHERE `t`.`index_hash` NOT IN (
          SELECT `fd`.`index_hash`
          FROM {{$entity_type}_field_data} `fd`
          WHERE `fd`.`feed_reference__target_id` = :frti
        )",
      [':frti' => $feedReference['target_id']]
    )->fetch(\PDO::FETCH_COLUMN);

    $offset = 0;

    while ($offset < $new_count) {
      $operations[] = [
        [$this, 'transformInsertNew'],
        [$class],
      ];
      $offset += self::BATCH_SIZE;
    }


    $updated_count = $db->query(
      "SELECT count(fd.index_hash)
       FROM {{$entity_type}_field_data} `fd`
       WHERE `fd`.`feed_reference__target_id` = :frti
       AND `fd`.`index_hash` IN (
         SELECT `t`.`index_hash`
         FROM {{$entity_type}_temp} `t`
       ) AND `fd`.`hash` NOT IN (
         SELECT `t`.`hash`
         FROM {{$entity_type}_temp} `t`
       )",
     [':frti' => $feedReference['target_id']]
    )->fetch(\PDO::FETCH_COLUMN);


    while ($offset < $updated_count) {
      $operations[] = [
        [$this, 'transformUpdateExisting'],
        [$class],
      ];
      $offset += self::BATCH_SIZE;
    }


    $deleted_count = $db->query(
      "SELECT count(fd.index_hash)
       FROM {{$entity_type}_field_data} `fd`
        WHERE `fd`.`index_hash` NOT IN (
          SELECT `t`.`index_hash`
          FROM {{$entity_type}_temp} `t`
        ) AND `fd`.`feed_reference__target_id` = :frti",
      [':frti' => $feedReference['target_id']]
    )->fetch(\PDO::FETCH_COLUMN);

    $offset = 0;

    while ($offset < $deleted_count) {
      $operations[] = [
        [$this, 'transformDeleteOld'],
        [$class],
      ];
      $offset += self::BATCH_SIZE;
    }

    $this->log("Enqueued transformations for {$class}: " . PHP_EOL . "New: {$new_count}" . PHP_EOL . "Updated: {$updated_count}" . PHP_EOL . "Deleted: {$deleted_count}", 'default');

    $next_class = NULL;

    foreach ($this->files as $index => $file) {
      if (
        $file_entity_map[$file] == $class
        && isset($this->files[$index + 1])
      ) {
        $next_class = $file_entity_map[$this->files[$index + 1]];
      }
    }

    if ($next_class) {
      $urlParameters = [
        'gtfs_feed' => $this->feed->id(),
        'step' => 'transform',
        'class' => base64_encode($next_class),
      ];
    }
    else {
      $urlParameters = [
        'gtfs_feed' => $this->feed->id(),
        'step' => 'cleanup',
      ];
    }

    $batch = [
      'title' => t('Transforming and Inserting Data'),
      'operations' => $operations,
      'finished' => [$this, 'batchTransformFinished'],
    ];

    if ($include_next_url) {
      $batch['next_url'] = Url::fromRoute('entity.gtfs_feed.import_form', $urlParameters);
    }

    return $batch;
  }

  /**
   * Transforms and inserts a batch of entities.
   *
   * @param string $class
   *   The class from which to insert entities.
   * @param array $context
   *   The batch context.
   *
   * @throws \Exception
   * @see https://www.drupal.org/docs/7/api/batch-api
   */
  public function transformInsertNew(string $class, &$context) {
    $schema = $class::schema();
    $entity_type = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class);
    $limit = self::BATCH_SIZE;
    $feedReference = $this->getFeedReference();
    $count = 0;
    $key_cache = [];
    $db = \Drupal::database();

    if (isset($schema['foreign keys'])) {
      foreach ($schema['foreign keys'] as $specification) {
        foreach ($specification['columns'] as $source_field => $destination_field) {
          // Key = gtfs id, value = drupal id
          $key_cache[$specification['table']][$source_field] = $db->query(
            "SELECT `{$source_field}`,`{$destination_field}`
               FROM {{$specification['table']}_field_data}
               WHERE `feed_reference__target_id` = :frti",
            [':frti' => $feedReference['target_id']]
          )->fetchAll(\PDO::FETCH_KEY_PAIR);
        }
      }
    }

    $records = $db->query(
      "SELECT `t`.*
       FROM {{$entity_type}_temp} `t`
       WHERE `t`.`index_hash` NOT IN (
         SELECT `fd`.`index_hash`
         FROM {{$entity_type}_field_data} `fd`
         WHERE `feed_reference__target_id` = '{$feedReference['target_id']}'
       )
       LIMIT {$limit}"
    )->fetchAll(\PDO::FETCH_ASSOC);

    if (empty($records)) {
      return;
    }

    \Drupal::lock()->acquire('gtfs_lock');

    $base_columns = ['id', 'vid', 'uuid', 'langcode'];
    $revision_columns = [
      'id',
      'vid',
      'langcode',
      'revision_default',
      'revision_user',
      'revision_created',
      'revision_log_message',
    ];
    $data_revision_columns = array_merge(array_keys($records[0]), ['id', 'vid']);
    $data_columns = array_merge($data_revision_columns, ['changed', 'created']);

    $base_insert_query = $db->insert($entity_type)->fields($base_columns);
    $base_revision_insert_query = $db->insert("{$entity_type}_revision")->fields($revision_columns);
    $data_revision_insert_query = $db->insert("{$entity_type}_field_revision")->fields($data_revision_columns);
    $data_insert_query = $db->insert("{$entity_type}_field_data")->fields($data_columns);

    $id = ((int) $db->query("SELECT MAX(id) FROM {{$entity_type}}")->fetch(\PDO::FETCH_COLUMN)) + 1;
    $vid = ((int) $db->query("SELECT MAX(vid) FROM {{$entity_type}}")->fetch(\PDO::FETCH_COLUMN)) + 1;

    $time = time();

    foreach ($records as $object) {

      $valid = TRUE;
      $reason = '';

      foreach ($schema['primary key'] as $key) {
        // Discard invalid objects.
        if ($object[$key] != 0 && empty($object[$key])) {
          $valid = FALSE;
          $reason .= "{$key} is invalid";
        }
      }

      if (isset($schema['foreign keys'])) {
        foreach ($schema['foreign keys'] as $specification) {
          $table = $specification['table'];
          foreach ($specification['columns'] as $source_field => $destination_field) {
            $this_map_invalidates = FALSE;
            if (isset($key_cache[$table][$source_field][$object[$source_field]])) {
              $target_id = $key_cache[$table][$source_field][$object[$source_field]];
            } else {
              $target_id = FALSE;
            }

            // Sometimes, like with stops, the source field will be 0
            // which is semantically equal to blank but is valid.
            if (!$target_id && $object[$source_field] != 0) {
              $this_map_invalidates = TRUE;
            }
            else {
              $object[$source_field] = (
                $object[$source_field] === 0
                || $object[$source_field] === '0'
                ) ? 0 : $target_id;
              if($object[$source_field] === FALSE) {
                $object[$source_field] = NULL;
              }
            }
            // We keep track of whether or not each foreign key map
            // throws a validation error, because if the foreign key maps
            // to its own table, this is impossible to calculate validity
            // due to import order. These must be sorted out in post-import
            // tasks. See core Stop entity's "parent_station" field.
            if ($this_map_invalidates && $table !== $entity_type) {
              $reason .= " {$table} !== {$entity_type}";
              $valid = FALSE;
            } else if ($this_map_invalidates) {
              // We need to null this field, because the foreign key value
              // is not going to be legal. i.e. it does not contain a \Drupal ID
              $object[$source_field] = NULL;
            }
          }
        }
      }

      if (!$valid) {
        $this->log("Invalid Import: {$reason}" . PHP_EOL . '<pre>' . print_r($object, true) . '</pre>', 'error');
        continue;
      }

      $object['id'] = $id;
      $object['vid'] = $vid;

      $uuid = (new UUID())->generate();
      $base_values = [$object['id'], $object['vid'], $uuid, 'en'];
      $base_insert_query->values(array_combine($base_columns, $base_values));

      $revision_values = [
        $object['id'],
        $object['vid'],
        static::DEFAULT_LANGCODE,
        static::DEFAULT_REVISION,
        static::DEFAULT_USER,
        $time,
        static::DEFAULT_LOG_MESSAGE,
      ];

      $base_revision_insert_query->values(array_combine($revision_columns, $revision_values));

      $data_revision_insert_query->values($object);

      $object['created'] = $object['changed'] = $time;

      $data_insert_query->values($object);

      $id++;
      $vid++;
      $count++;
    }

    $base_insert_query->execute();
    $base_revision_insert_query->execute();
    $data_insert_query->execute();
    $data_revision_insert_query->execute();

    \Drupal::lock()->release('gtfs_lock');

    $context['message'] = t('Inserted @count new @tablename records', ['@count' => $count, '@tablename' => $entity_type]);
    $this->updateTransformCount($entity_type, 'inserted', $count);
  }

  /**
   * Transforms and updates a batch of entities.
   *
   * @param string $class
   *   The class from which to update entities.
   * @param array $context
   *   The batch context.
   *
   * @see https://www.drupal.org/docs/7/api/batch-api
   */
  public function transformUpdateExisting(string $class, &$context) {
    $db = \Drupal::database();
    $entity_type = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class);
    $limit = self::BATCH_SIZE;
    $count = 0;
    $existing_records = $db->query(
      "SELECT `fd`.`index_hash`
       FROM {{$entity_type}_field_data} `fd`
       WHERE `fd`.`index_hash` IN (
         SELECT `t`.`index_hash`
         FROM {{$entity_type}_temp} `t`
       ) AND `fd`.`hash` NOT IN (
         SELECT `t`.`hash`
         FROM {{$entity_type}_temp} `t`
       )
       LIMIT {$limit}"
    )->fetchAll(\PDO::FETCH_COLUMN, 0);

    foreach ($existing_records as $index_hash) {
      $temp = $db->query(
        "SELECT *
         FROM {{$entity_type}_temp}
         WHERE `index_hash` = :index",
        [':index' => $index_hash]
      )->fetch(\PDO::FETCH_ASSOC);

      $existing = $class::load($db->query(
        "SELECT `id`
         FROM {{$entity_type}_field_data}
         WHERE `index_hash` = :index",
        [':index' => $index_hash]
      )->fetch(\PDO::FETCH_COLUMN));

      foreach ($temp as $key => $value) {
        if ($existing->{$key} != $value) {
          $existing->setNewRevision();
        }
        $existing->set($key, $value);
      }

      $existing->save();
      $count++;
    }

    $db->delete("{$entity_type}_temp")
      ->condition('index_hash', $existing_records, 'IN')
      ->execute();

    $context['message'] = t('Updated @count existing @tablename records', ['@count' => $count, '@tablename' => $entity_type]);
    $this->updateTransformCount($entity_type, 'updated', $count);

  }

  /**
   * Deletes entities not present in import.
   *
   * @param string $class
   *   The class from which to delete entities.
   * @param array $context
   *   The batch context.
   *
   * @see https://www.drupal.org/docs/7/api/batch-api
   */
  public function transformDeleteOld(string $class, &$context) {
    $entity_type = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class);
    $limit = self::BATCH_SIZE;
    $feedReference = $this->getFeedReference();
    $count = 0;
    $old_records = \Drupal::database()->query(
      "SELECT `fd`.`id`
       FROM {{$entity_type}_field_data} `fd`
       WHERE `fd`.`index_hash` NOT IN (
         SELECT `t`.`index_hash`
         FROM {{$entity_type}_temp} `t`
       )
       AND `fd`.`feed_reference__target_id` = :frti
       LIMIT {$limit}",
      [':frti' => $feedReference['target_id']]
    )->fetchAll(\PDO::FETCH_COLUMN, 0);

    foreach ($old_records as $id) {
      $existing = $class::load($id);
      $existing->delete();
      $count++;
    }

    $context['message'] = t('Deleted @count old records', ['@count' => $count]);
    $this->updateTransformCount($entity_type, 'deleted', $count);
  }

  /**
   * Cleans up temporary tables after inerting a class.
   *
   * @param bool $success
   *   Whether or not the operation succeeded.
   * @param array $results
   *   Data stored during operation in $context['results'].
   * @param array $operations
   *   Failed operations.
   *
   * @see https://www.drupal.org/docs/7/api/batch-api
   */
  public function batchTransformFinished($success, $results, $operations) {
    if (!$success) {
      $this->log("Import failed: " . PHP_EOL . "<pre>" . print_r($results, TRUE) . print_r($operations, TRUE) . "</pre>", 'error');
    }
  }

  /**
   * Creates a batch to cleanup the import's mess.
   *
   * @return array
   *   \Drupal batch description.
   *
   * @see https://www.drupal.org/docs/7/api/batch-api
   */
  public function batchCleanup(): array {
    $batch = [
      'title' => t('Cleaning up'),
      'operations' => [],
      'finished' => [$this, 'batchCleanupFinished'],
    ];
    $file_entity_map = static::getFileEntityMap();

    // First let the imported entities do any cleanup
    // that they need
    foreach ($file_entity_map as $class) {
      $batch['operations'] = $batch['operations'] + $class::postImportBatchOperations($this->feed->id());
    }

    $downloader = new Downloader($this->feed);
    // Remove the files used to create this feed
    $batch['operations'][] = [
      [$downloader, 'removeDirectory'],
      []
    ];
    return $batch;
  }

  /**
   * @param $success
   * @param $results
   * @param $operations
   *
   * @see https://www.drupal.org/docs/7/api/batch-api
   */
  public function batchCleanupFinished($success, $results, $operations) {
    // Once it's finally imported and post-import tasks processed,
    // then we can delete import tables
    $file_entity_map = static::getFileEntityMap();
    $db_schema = \Drupal::database()->schema();
    foreach ($file_entity_map as $class) {
      $temp_table_name = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class) . '_temp';
      if ($db_schema->tableExists($temp_table_name)) {
        $db_schema->dropTable($temp_table_name);
        $this->log("Dropped {$temp_table_name} table", 'cleanup');
      }
    }
    \Drupal::moduleHandler()->invokeAll('gtfs_feed_imported', [$this->id]);
    \Drupal::database()
      ->update('gtfs_feed_imports')
      ->fields([
        'finished' => time(),
      ])
      ->condition('id', $this->id)
      ->execute();
    Importer::mailLogs($this->id, \Drupal::config('system.site')->get('mail'));
  }

  public function log(string $message, $category = 'default') {
    $db = \Drupal::database();
    $last_message = $db->query('
      SELECT id,category,message,timestamp
      FROM {gtfs_feed_import_logs}
      ORDER BY id DESC
      LIMIT 1
    ')->fetchObject();
    if ($last_message->category === $category) {
      $timestamp = $last_message->timestamp . '+' . (time() - (int) $last_message->timestamp);
      $db
        ->update('gtfs_feed_import_logs')
        ->condition('id', $last_message->id)
        ->fields([
          'message' => $last_message->message . PHP_EOL . $timestamp . ': ' . $message
        ])
        ->execute();
    } else {
      $db
        ->insert('gtfs_feed_import_logs')
        ->fields([
          'import_id' => $this->id,
          'timestamp' => time(),
          'message' => $message,
          'category' => substr($category, 0, 255)
        ])
        ->execute();
    }
    return $message;
  }

  public static function logForFeed($feed_id, $message, $category = 'default') {
    // We have to make a guess as to which import, so we'll just use the latest
    // for a given feed id.
    $import_id = \Drupal::database()->query('
      SELECT id
      FROM gtfs_feed_imports
      WHERE feed_id = :feed_id
      ORDER BY id DESC
    ', [':feed_id' => $feed_id])->fetchField();
    if (!$import_id) {
      throw new \Exception("No imports for feed {$feed_id}");
    }
    $importer = new Importer($import_id);
    return $importer->log($message, $category);
  }

  public static function mailLogs($import_id, $email_address) {
    $mailManager = \Drupal::service('plugin.manager.mail');
    $importer = new Importer($import_id);
    $logs = \Drupal::database()->query('
      SELECT *
      FROM {gtfs_feed_import_logs}
      WHERE import_id = :import_id
    ', [
      ':import_id' => $import_id
    ])->fetchAll(\PDO::FETCH_OBJ);
    $results = '';
    foreach ($importer->completed as $type => $counts) {
      $title = ucwords(str_replace(['gtfs_', '_'], ['', ' '], $type));
      foreach ($counts as $count_type => $count) {
        $results .= implode(' ', [
          ucwords($count_type),
          $count,
          $title,
          'records',
          PHP_EOL
        ]);
      }
    }
    $important = '';
    foreach ($logs as $index => $log) {
      if ('important' === $log->category) {
        unset($logs[$index]);
        $important .= date('H:i:s', $log->timestamp) . PHP_EOL;
        $log->message = str_replace($log->timestamp, '', $log->message);
        $important .= $log->message . PHP_EOL;
      }
    }
    try {
      $feed = $importer->getFeed();
    } catch (\Throwable $e) {
      $feed = null;
    }
    $params['subject'] = 'GTFS Import Log ' . $import_id;
    $params['message'] = 'The GTFS Feed "' . ($feed ? $feed->label() : 'unknown') . '" was just imported' . PHP_EOL;
    if ($feed) {
      $params['message'] .= $feed->toUrl('canonical', ['absolute' => true])->toString() . PHP_EOL . PHP_EOL;
    }
    $params['message'] .= 'Started at ' . date('F j, Y H:i:s', $importer->began);
    if ($importer->finished) {
      $params['message'] .= ' and completed at ' . date('F j, Y H:i:s', $importer->finished);
      $difference = $importer->finished - $importer->began;
      $params['message'] .= ', taking ' . static::secondsToDuration($difference) . PHP_EOL;
      $params['subject'] .= '✅';
    } else {
      $params['message'] .= ' but encountered an error before finishing. Please review the log.' . PHP_EOL;
      $params['subject'] .= '❗️';
    }
    $params['message'] .= PHP_EOL . $results . PHP_EOL;
    if ($important) {
      $params['message'] .= "Important Messages:" . PHP_EOL;
      $params['message'] .= PHP_EOL . $important . PHP_EOL;
    }
    $params['message'] .= "Log Messages:" . PHP_EOL;
    foreach ($logs as $log) {
      $params['message'] .= PHP_EOL . '## ' . strtoupper($log->category) . ' ';
      $params['message'] .= date('H:i:s', $log->timestamp) . PHP_EOL;
      $log->message = str_replace($log->timestamp, '', $log->message);
      $params['message'] .= $log->message . PHP_EOL;
    }
    \Drupal::moduleHandler()->alter('gtfs_mail_log', $params, $email_address, $import_id);
    $mailManager->mail('gtfs', 'import_log', $email_address, 'en', $params, NULL, true);
  }

  public static function secondsToDuration($seconds) {
    // Calculate days, hours, minutes, and seconds
    $days = floor($seconds / (60 * 60 * 24));
    $seconds %= (60 * 60 * 24);

    $hours = floor($seconds / (60 * 60));
    $seconds %= (60 * 60);

    $minutes = floor($seconds / 60);
    $seconds %= 60;

    // Build the duration string
    $duration = '';
    if ($days > 0) {
      $duration .= $days . ' day' . ($days != 1 ? 's' : '') . ' ';
    }
    if ($hours > 0) {
      $duration .= $hours . ' hour' . ($hours != 1 ? 's' : '') . ' ';
    }
    if ($minutes > 0) {
      $duration .= $minutes . ' minute' . ($minutes != 1 ? 's' : '') . ' ';
    }
    if ($seconds > 0) {
      $duration .= $seconds . ' second' . ($seconds != 1 ? 's' : '') . ' ';
    }

    // Trim any extra spaces at the end and return
    return trim($duration);
  }

}


/**
 * Class EOLDetector
 *
 * @see https://stackoverflow.com/a/24927253
 *
 * @package \Drupal\gtfs\Entity\Feed
 */
class EOLDetector {
  const EOL_UNIX    = 'lf';        // Code: \n
  const EOL_TRS80   = 'cr';        // Code: \r
  const EOL_ACORN   = 'lfcr';      // Code: \n \r
  const EOL_WINDOWS = 'crlf';      // Code: \r \n

  /**
   * Detects the end-of-line character of a string.
   * @param string $str The string to check.
   * @param string $key [io] Name of the detected eol key.
   * @return string The detected EOL, or default one.
   *
   * @return mixed|string
   */
  public static function detectEOL($str, &$key) {
     static $eols = array(
       self::EOL_ACORN   => "\n\r",  // 0x0A - 0x0D - acorn BBC
       self::EOL_WINDOWS => "\r\n",  // 0x0D - 0x0A - Windows, DOS OS/2
       self::EOL_UNIX    => "\n",    // 0x0A -      - Unix, OSX
       self::EOL_TRS80   => "\r",    // 0x0D -      - Apple ][, TRS80
    );

    $key = "";
    $curCount = 0;
    $curEol = '';
    foreach($eols as $k => $eol) {
       if( ($count = substr_count($str, $eol)) > $curCount) {
          $curCount = $count;
          $curEol = $eol;
          $key = $k;
       }
    }
    return $curEol;
  }

  /**
   * Detects the EOL of an file by checking the first line.
   * @param string $fileName File to be tested (full pathname).
   * @return boolean false | Used key = enum('cr', 'lf', crlf').
   * @uses detectEOL
   *
   * @return bool|string
   */
  public static function detectFileEOL($fileName) {
     if (!file_exists($fileName)) {
       return false;
     }

     // Gets the line length
     $handle = @fopen($fileName, "r");
     if ($handle === false) {
        return false;
     }
     $line = fgets($handle);
     $key = "";
     return self::detectEOL($line, $key);
  }  // detectFileEOL
}
