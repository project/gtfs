<?php

namespace Drupal\gtfs\Entity;

/**
 * Indicates that the entity using this interface can be represented geographically
 *
 * @package Drupal\gtfs\Entity
 */
interface GeodataInterface {
  public function geojson(): array;
  // TODO WKT
}