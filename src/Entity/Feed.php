<?php

namespace Drupal\gtfs\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\gtfs\Entity\Feed\Importer;

/**
 * Defines the GTFS Feed entity.
 *
 * @ingroup gtfs
 *
 * @ContentEntityType(
 *   id = "gtfs_feed",
 *   label = @Translation("GTFS Feed"),
 *   handlers = {
 *     "storage" = "Drupal\gtfs\FeedStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gtfs\FeedListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\gtfs\Form\FeedForm",
 *       "add" = "Drupal\gtfs\Form\FeedForm",
 *       "edit" = "Drupal\gtfs\Form\FeedForm",
 *       "import" = "Drupal\gtfs\Form\FeedImportForm",
 *       "delete" = "Drupal\gtfs\Form\FeedDeleteForm",
 *     },
 *     "access" = "Drupal\gtfs\FeedAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gtfs\FeedHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gtfs_feed",
 *   data_table = "gtfs_feed_field_data",
 *   revision_table = "gtfs_feed_revision",
 *   revision_data_table = "gtfs_feed_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer gtfs feed entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *    revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message"
 *   },
 *   links = {
 *     "canonical" = "/gtfs/feed/{gtfs_feed}",
 *     "add-form" = "/admin/gtfs/feed/add",
 *     "edit-form" = "/admin/gtfs/feed/{gtfs_feed}/edit",
 *     "import-form" = "/admin/gtfs/feed/{gtfs_feed}/import",
 *     "delete-form" = "/admin/gtfs/feed/{gtfs_feed}/delete",
 *     "version-history" = "/admin/gtfs/feed/{gtfs_feed}/revisions",
 *     "revision" = "/admin/gtfs/feed/{gtfs_feed}/revisions/{gtfs_feed_revision}/view",
 *     "revision_revert" = "/admin/gtfs/feed/{gtfs_feed}/revisions/{gtfs_feed_revision}/revert",
 *     "revision_delete" = "/admin/gtfs/feed/{gtfs_feed}/revisions/{gtfs_feed_revision}/delete",
 *     "translation_revert" = "/admin/gtfs/feed/{gtfs_feed}/revisions/{gtfs_feed_revision}/revert/{langcode}",
 *     "collection" = "/admin/gtfs/feed",
 *   },
 *   field_ui_base_route = "gtfs_feed.settings"
 * )
 */
class Feed extends RevisionableContentEntityBase implements FeedInterface {

  use EntityChangedTrait;


  // Deletes this feed and eveything attached to it
  public function batchDeleteAll($deleteSelf = FALSE) {
    $batch =  [
      'title' => "Deleting all entities attached to {$this->getName()}",
      'operations' => [],
      'finished' => [$this, 'batchDeleteFinished']
    ];
    $db = \Drupal::database();
    foreach (array_values(Importer::getFileEntityMap()) as $class) {
      $entity_type = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class);
      $query = $db->select(sprintf("%s_field_data", $entity_type), 'e');
      $query->condition('feed_reference__target_id', $this->id());
      $query->fields('e', ['id']);
      $ids = $query->execute()->fetchAll(\PDO::FETCH_COLUMN);
      foreach (array_chunk($ids, 200) as $idChunk) {
        $batch['operations'][] = [
          [$class, 'deleteByIds'],
          [$idChunk],
        ];
      }
    }
    if ($deleteSelf) {
      $batch['operations'][] = [
        [static::class, 'deleteById'],
        [$this->id()],
      ];
    }

    return $batch;
  }

  public function batchDeleteFinished() {
    \Drupal::moduleHandler()->invokeAll('gtfs_batch_deleted');
  }

  public static function deleteById($id) {
    $entity = static::load($id);
    if ($entity) {
      $entity->delete();
    }
  }

  /**
   *
   * @param array $filesToImport
   *  The files within the GTFS zip to import
   *
   * @return \Drupal\gtfs\Entity\Feed\Importer
   * @throws \Exception
   */
  public function newImport(array $filesToImport = [], $full_import = TRUE) {
    $importId = \Drupal::database()->insert('gtfs_feed_imports')->fields([
      'feed_id' => $this->id(),
      'began' => time(),
      'files' => serialize($filesToImport),
      'completed' => serialize([]),
      'full' => (int) $full_import,
    ])->execute();
    return new Importer($importId, $full_import);
  }

  /**
   *
   */
  public function getCurrentImport() {
    $importId = \Drupal::database()->query(
      'SELECT `id`
       FROM {gtfs_feed_imports}
       WHERE `feed_id` = :feed_id
       ORDER BY `began` DESC
       LIMIT 1',
      [':feed_id' => $this->id()]
    )->fetch(\PDO::FETCH_COLUMN);
    return new Importer($importId);
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the feed owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', (bool) $published);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the GTFS Feed entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the GTFS Feed.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['feed_url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('GTFS Feed URL'))
      ->setDescription(t('The URL of the GTFS feed to track. You may import from this feed.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the GTFS Feed is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 4,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
