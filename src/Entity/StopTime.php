<?php

namespace Drupal\gtfs\Entity;

use Drupal\time_field\Time;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the GTFS Stop Time entity.
 *
 * @ingroup gtfs
 *
 * @ContentEntityType(
 *   id = "gtfs_stop_time",
 *   label = @Translation("GTFS Stop Time"),
 *   handlers = {
 *     "storage" = "Drupal\gtfs\StopTimeStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gtfs\StopTimeListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "controller" = "Drupal\gtfs\Controller\StopTimeController",
 *     "form" = {
 *       "default" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "add" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "edit" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "revision_revert" = "Drupal\gtfs\Form\StopTimeRevisionRevertForm",
 *       "revision_delete" = "\Drupal\gtfs\Form\StopTimeRevisionDeleteForm",
 *       "translation_revert" = "\Drupal\gtfs\Form\StopTimeRevisionRevertTranslationForm",
 *       "settings" = "Drupal\gtfs\Form\StopTimeSettingsForm",
 *     },
 *      "access" = "Drupal\gtfs\GTFSObjectAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gtfs\GTFSEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gtfs_stop_time",
 *   data_table = "gtfs_stop_time_field_data",
 *   revision_table = "gtfs_stop_time_revision",
 *   revision_data_table = "gtfs_stop_time_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer gtfs stop_time entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *    revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message"
 *   },
 *   links = {
 *     "canonical" = "/gtfs/stop_time/{gtfs_stop_time}",
 *     "add-form" = "/admin/gtfs/stop_time/add",
 *     "edit-form" = "/admin/gtfs/stop_time/{gtfs_stop_time}/edit",
 *     "delete-form" = "/admin/gtfs/stop_time/{gtfs_stop_time}/delete",
 *     "version-history" = "/admin/gtfs/stop_time/{gtfs_stop_time}/revisions",
 *     "revision" = "/admin/gtfs/stop_time/{gtfs_stop_time}/revisions/{gtfs_stop_time_revision}/view",
 *     "revision_revert" = "/admin/gtfs/stop_time/{gtfs_stop_time}/revisions/{gtfs_stop_time_revision}/revert",
 *     "revision_delete" = "/admin/gtfs/stop_time/{gtfs_stop_time}/revisions/{gtfs_stop_time_revision}/delete",
 *     "translation_revert" = "/admin/gtfs/stop_time/{gtfs_stop_time}/revisions/{gtfs_stop_time_revision}/revert/{langcode}",
 *     "collection" = "/admin/gtfs/stop_time",
 *   },
 *   field_ui_base_route = "gtfs_stop_time.settings"
 * )
 */
class StopTime extends GTFSEntityBase {

  const TIME_FORMAT = 'H:i:s';

  /**
   * {@inheritdoc}
   */
  public static function schema(): array {
    return [
      'fields' => [
        'trip_id' => ['type' => 'varchar', 'length' => 128, 'not null' => TRUE],
        'arrival_time' => ['type' => 'varchar', 'length' => 8, 'default' => ''],
        'departure_time' => [
          'type' => 'varchar',
          'length' => 8,
          'default' => '',
        ],
        'stop_id' => ['type' => 'varchar', 'length' => 128, 'not null' => TRUE],
        'stop_sequence' => ['type' => 'int', 'length' => 11],
        'stop_headsign' => [
          'type' => 'varchar',
          'length' => 255,
          'default' => '',
        ],
        'pickup_type' => ['type' => 'int', 'length' => 2],
        'drop_off_type' => ['type' => 'int', 'length' => 2],
        'shape_dist_traveled' => ['type' => 'int', 'length' => 11],
        'timepoint' => ['type' => 'int', 'length' => 1],
      ],
      'primary key' => ['trip_id', 'stop_id'],
      'foreign keys' => [
        'trip' => [
          'table' => 'gtfs_trip',
          'columns' => ['trip_id' => 'id'],
        ],
        'stop' => [
          'table' => 'gtfs_stop',
          'columns' => ['stop_id' => 'id'],
        ],
      ],
    ];
  }

  /**
   * Returns the stop associated with this stoptime.
   *
   * @return \Drupal\gtfs\Entity\Stop
   *   The associated stop
   */
  public function stop() {
    $stop_id = $this->get('stop_id')->target_id;
    return Stop::load($stop_id);
  }

  /**
   * Returns the trip associated with this stoptime.
   *
   * @return \Drupal\gtfs\Entity\Trip
   *   The associated trip
   */
  public function trip() {
    $trip_id = $this->get('trip_id')->target_id;
    return Trip::load($trip_id);
  }

  /**
   * Takes seconds in standard format and converts to seconds past midnight.
   *
   * @param string $time
   *
   * @return float|int
   */
  public static function parseTime(string $time) {
    $parts = explode(':', $time);
    $value = 0;
    $value += (int) $parts[0] * 3600;
    $value += (int) $parts[1] * 60;
    $value += (int) $parts[2];
    return $value;
  }

  /**
   * @param string $time
   *
   * @return \DateTime|false
   */
  public static function timeStringToDateTime(string $time) {
    return \DateTime::createFromFormat(self::TIME_FORMAT, $time);
  }

  /**
   * {@inheritdoc}
   */
  public function toGTFSObject(): array {
    $return = parent::toGTFSObject();
    $trip_id = $this->get('trip_id')->target_id;
    $stop_id = $this->get('stop_id')->target_id;
    $return['trip_id'] = \Drupal::database()->query(
      "SELECT `trip_id`
       FROM {gtfs_trip_field_data}
       WHERE `id` = '{$trip_id}'"
    )->fetch(\PDO::FETCH_COLUMN);
    $return['stop_id'] = \Drupal::database()->query(
      "SELECT `stop_id`
       FROM {gtfs_stop_field_data}
       WHERE `id` = '{$stop_id}'"
    )->fetch(\PDO::FETCH_COLUMN);
    return $return;
    // TODO Below is if the information were to be entered in timestamp format.
    //   As it is, it's not when it's imported, but it may be
    //   when it's entered via the UI. Currently the UI doesn't accomodate the date format.
    if ($return['arrival_time'] > 86400) {
      $return['arrival_time'] -= 86400;
    }
    $return['arrival_time'] = Time::createFromTimestamp($return['arrival_time'])->format(self::TIME_FORMAT);
    if ($return['departure_time'] > 86400) {
      $return['departure_time'] -= 86400;
    }
    $return['departure_time'] = Time::createFromTimestamp($return['departure_time'])->format(self::TIME_FORMAT);
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    $this->setName($this->trip()->getName() . ' stop ' . $this->get('stop_sequence')->value);
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('This will be automatically populated from the other fields'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE)
      ->setReadOnly(TRUE);

    $fields['trip_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Trip ID'))
      ->setDescription(t('The trip_id field contains an ID that identifies a trip. This value is referenced from the trips.txt file.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'target_type' => 'gtfs_trip',
        'default_value' => 0,
        'handler' => 'default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['arrival_time'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Arrival time'))
      ->setDescription(t('The arrival_time specifies the arrival time at a specific stop for a specific trip on a route. The time is measured from "noon minus 12h" (effectively midnight, except for days on which daylight savings time changes occur) at the beginning of the service day. For times occurring after midnight on the service day, enter the time as a value greater than 24:00:00 in HH:MM:SS local time for the day on which the trip schedule begins. If you don\'t have separate times for arrival and departure at a stop, enter the same value for arrival_time and departure_time.'))
    // ->setSetting('datetime_type', 'date')
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'weight' => -3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['departure_time'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Departure time'))
      ->setDescription(t('The departure_time specifies the departure time from a specific stop for a specific trip on a route. The time is measured from "noon minus 12h" (effectively midnight, except for days on which daylight savings time changes occur) at the beginning of the service day. For times occurring after midnight on the service day, enter the time as a value greater than 24:00:00 in HH:MM:SS local time for the day on which the trip schedule begins. If you don\'t have separate times for arrival and departure at a stop, enter the same value for arrival_time and departure_time.'))
    // ->setSetting('datetime_type', 'date')
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'weight' => -3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['stop_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Stop ID'))
      ->setDescription(t('The stop_id field contains an ID that uniquely identifies a stop. Multiple routes may use the same stop. The stop_id is referenced from the stops.txt file. If location_type is used in stops.txt, all stops referenced in stop_times.txt must have location_type of 0. Where possible, stop_id values should remain consistent between feed updates. In other words, stop A with stop_id 1 should have stop_id 1 in all subsequent data updates. If a stop is not a time point, enter blank values for arrival_time and departure_time.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'target_type' => 'gtfs_stop',
        'default_value' => 0,
        'handler' => 'default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['stop_sequence'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Stop Sequence'))
      ->setDescription(t('The stop_sequence field identifies the order of the stops for a particular trip. The values for stop_sequence must be non-negative integers, and they must increase along the trip. For example, the first stop on the trip could have a stop_sequence of 1, the second stop on the trip could have a stop_sequence of 23, the third stop could have a stop_sequence of 40, and so on.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['stop_headsign'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Stop Headsign'))
      ->setDescription(t('The stop_headsign field contains the text that appears on a sign that identifies the trip\'s destination to passengers. Use this field to override the default trip_headsign when the headsign changes between stops. If this headsign is associated with an entire trip, use trip_headsign instead.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['pickup_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Pickup Type'))
      ->setDescription(t('The pickup_type field indicates whether passengers are picked up at a stop as part of the normal schedule or whether a pickup at the stop is not available. This field also allows the transit agency to indicate that passengers must call the agency or notify the driver to arrange a pickup at a particular stop.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue('0')
      ->setSettings([
        'allowed_values' => [
          '0' => 'Regularly scheduled pickup',
          '1' => 'No pickup available',
          '2' => 'Must phone agency to arrange pickup',
          '3' => 'Must coordinate with driver to arrange pickup',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['drop_off_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Dropoff Type'))
      ->setDescription(t('The drop_off_type field indicates whether passengers are dropped off at a stop as part of the normal schedule or whether a drop off at the stop is not available. This field also allows the transit agency to indicate that passengers must call the agency or notify the driver to arrange a drop off at a particular stop.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue('0')
      ->setSettings([
        'allowed_values' => [
          '0' => 'Regularly scheduled drop off',
          '1' => 'No drop off available',
          '2' => 'Must phone agency to arrange drop off',
          '3' => 'Must coordinate with driver to arrange drop off',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['shape_dist_traveled'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Shape Distance Traveled'))
      ->setDescription(t('When used in the stop_times.txt file, the shape_dist_traveled field positions a stop as a distance from the first shape point. The shape_dist_traveled field represents a real distance traveled along the route in units such as feet or kilometers. For example, if a bus travels a distance of 5.25 kilometers from the start of the shape to the stop, the shape_dist_traveled for the stop ID would be entered as "5.25". This information allows the trip planner to determine how much of the shape to draw when showing part of a trip on the map. The values used for shape_dist_traveled must increase along with stop_sequence: they cannot be used to show reverse travel along a route. The units used for shape_dist_traveled in the stop_times.txt file must match the units that are used for this field in the shapes.txt file.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'number_decimal',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['timepoint'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Timepoint'))
      ->setDescription(t('The timepoint field can be used to indicate if the specified arrival and departure times for a stop are strictly adhered to by the transit vehicle or if they are instead approximate and/or interpolated times. The field allows a GTFS producer to provide interpolated stop times that potentially incorporate local knowledge, but still indicate if the times are approximate.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    return $fields;
  }

}
