<?php

namespace Drupal\gtfs\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the GTFS Direction entity.
 *
 * @ingroup gtfs
 *
 * @ContentEntityType(
 *   id = "gtfs_direction",
 *   label = @Translation("GTFS Direction"),
 *   handlers = {
 *     "storage" = "Drupal\gtfs\DirectionStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gtfs\DirectionListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "controller" = "Drupal\gtfs\Controller\DirectionController",
 *     "form" = {
 *       "default" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "add" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "edit" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "revision_revert" = "Drupal\gtfs\Form\DirectionRevisionRevertForm",
 *       "revision_delete" = "\Drupal\gtfs\Form\DirectionRevisionDeleteForm",
 *       "translation_revert" = "\Drupal\gtfs\Form\DirectionRevisionRevertTranslationForm",
 *       "settings" = "Drupal\gtfs\Form\DirectionSettingsForm",
 *     },
 *     "access" = "Drupal\gtfs\GTFSObjectAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gtfs\GTFSEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gtfs_direction",
 *   data_table = "gtfs_direction_field_data",
 *   revision_table = "gtfs_direction_revision",
 *   revision_data_table = "gtfs_direction_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer gtfs direction entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "direction",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *    revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message"
 *   },
 *   links = {
 *     "canonical" = "/gtfs/direction/{gtfs_direction}",
 *     "add-form" = "/admin/gtfs/direction/add",
 *     "edit-form" = "/admin/gtfs/direction/{gtfs_direction}/edit",
 *     "delete-form" = "/admin/gtfs/direction/{gtfs_direction}/delete",
 *     "version-history" = "/admin/gtfs/direction/{gtfs_direction}/revisions",
 *     "revision" = "/admin/gtfs/direction/{gtfs_direction}/revisions/{gtfs_direction_revision}/view",
 *     "revision_revert" = "/admin/gtfs/direction/{gtfs_direction}/revisions/{gtfs_direction_revision}/revert",
 *     "revision_delete" = "/admin/gtfs/direction/{gtfs_direction}/revisions/{gtfs_direction_revision}/delete",
 *     "translation_revert" = "/admin/gtfs/direction/{gtfs_direction}/revisions/{gtfs_direction_revision}/revert/{langcode}",
 *     "collection" = "/admin/gtfs/direction",
 *   },
 *   field_ui_base_route = "gtfs_direction.settings"
 * )
 */
class Direction extends GTFSEntityBase {
  use HasIdTrait;

  public static $resourceURL = '/gtfs/api/v1/agencies/{agency_id}/routes/{route_id}/directions';

  /**
   * {@inheritdoc}
   */
  public static function schema(): array {
    return [
      'fields' => [
        'route_id' => ['type' => 'varchar', 'length' => 128],
        'direction_id' => ['type' => 'varchar', 'length' => 128, 'not null' => TRUE],
        'direction' => ['type' => 'varchar', 'length' => 128, 'not null' => TRUE],
      ],
      'primary key' => ['direction_id', 'route_id'],
      'foreign keys' => [
        'route' => [
          'table' => 'gtfs_route',
          'columns' => ['route_id' => 'id'],
        ],
      ],
    ];
  }


  /**
   * {@inheritDoc}
   */
  public static function getById(Route $route, $direction_id) {
    $id = \Drupal::database()->query(
      "SELECT `id`
       FROM {gtfs_direction_field_data}
       WHERE `direction_id` = :direction_id
       AND `route_id` = :route_id
       AND `feed_reference__target_id` = :feed_id",
      [
        ':direction_id' => $direction_id,
        ':route_id' => $route->id(),
        ':feed_id' => $route->get('feed_reference')->target_id,
      ]
    )->fetch(\PDO::FETCH_COLUMN);

    return Direction::load($id);
  }

  public static function getFromSource($route_id, $direction_id) {
    return \Drupal::database()->query('
        SELECT *
        FROM {gtfs_direction_source}
        WHERE `route_id` = :route_id
        AND `direction_id` = :direction_id
        AND `feed_reference__target_revision_id` IN (
          SELECT MAX(feed_reference__target_revision_id)
          FROM {gtfs_direction_source}
          GROUP BY `route_id`,`direction_id`
      )
    ', [
      ':route_id' => $route_id,
      ':direction_id' => $direction_id,
    ])->fetch(\PDO::FETCH_ASSOC);
  }

  /**
   *
   */
  public function route() {
    $route_id = $this->get('route_id')->target_id;
    return Route::load($route_id);
  }

  /**
   * {@inheritdoc}
   */
  public function resourceUrl(string $append = ''): string {
    $agency_id = $this->route()->agency()->get('agency_id')->value;
    $route_id = $this->route()->get('route_id')->value;
    $tokens = ['{agency_id}', '{route_id}'];
    $values = [$agency_id, $route_id];
    $link = str_replace($tokens, $values, self::$resourceURL);
    $url = \Drupal::request()->getSchemeAndHttpHost() . $link . '/' . $this->get('direction_id')->value;
    $url .= $append;
    \Drupal::moduleHandler()->alter('gtfs_entity_resource_url', $url, $this);
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function links(): array {
    $reverse = $this->resourceUrl();
    if ($this->get('direction_id')->value == 0) {
      $reverse = str_replace('0', '1', $reverse);
    } else {
      $reverse = str_replace('1', '0', $reverse);
    }
    $links = [
      'route' => $this->route()->resourceUrl(),
      'reverse' => $reverse,

    ];
    return array_merge(parent::links(), $links);
  }

  /**
   * {@inheritdoc}
   */
  public function toGTFSObject(): array {
    $return = parent::toGTFSObject();
    $route_id = $this->get('route_id')->target_id;
    $return['route_id'] = \Drupal::database()->query(
      "SELECT `route_id`
       FROM {gtfs_route_field_data}
       WHERE `id` = '{$route_id}'"
    )->fetch(\PDO::FETCH_COLUMN);
    $return['direction_id'] = (string) $this->get('direction_id')->value;
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return  $this->get('direction')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('direction', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['direction_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Direction ID'))
      ->setDescription(t('The direction_id field contains an ID that identifies a direction for a route.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['direction'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Direction'))
      ->setDescription(t('The name of the direction.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['route_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Route ID'))
      ->setDescription(t('The route_id field contains an ID that uniquely identifies a route. This value is referenced from the routes.txt file.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'target_type' => 'gtfs_route',
        'default_value' => 0,
        'handler' => 'default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    return $fields;
  }

}
