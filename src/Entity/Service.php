<?php

namespace Drupal\gtfs\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the GTFS Service entity.
 *
 * @ingroup gtfs
 *
 * @ContentEntityType(
 *   id = "gtfs_service",
 *   label = @Translation("GTFS Service"),
 *   handlers = {
 *     "storage" = "Drupal\gtfs\ServiceStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gtfs\ServiceListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "controller" = "Drupal\gtfs\Controller\ServiceController",
 *     "form" = {
 *       "default" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "add" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "edit" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "revision_revert" = "Drupal\gtfs\Form\ServiceRevisionRevertForm",
 *       "revision_delete" = "\Drupal\gtfs\Form\ServiceRevisionDeleteForm",
 *       "translation_revert" = "\Drupal\gtfs\Form\ServiceRevisionRevertTranslationForm",
 *       "settings" = "Drupal\gtfs\Form\ServiceSettingsForm",
 *     },
 *     "access" = "Drupal\gtfs\GTFSObjectAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gtfs\GTFSEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gtfs_service",
 *   data_table = "gtfs_service_field_data",
 *   revision_table = "gtfs_service_revision",
 *   revision_data_table = "gtfs_service_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer gtfs service entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *    revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message"
 *   },
 *   links = {
 *     "canonical" = "/gtfs/service/{gtfs_service}",
 *     "add-form" = "/admin/gtfs/service/add",
 *     "edit-form" = "/admin/gtfs/service/{gtfs_service}/edit",
 *     "delete-form" = "/admin/gtfs/service/{gtfs_service}/delete",
 *     "version-history" = "/admin/gtfs/service/{gtfs_service}/revisions",
 *     "revision" = "/admin/gtfs/service/{gtfs_service}/revisions/{gtfs_service_revision}/view",
 *     "revision_revert" = "/admin/gtfs/service/{gtfs_service}/revisions/{gtfs_service_revision}/revert",
 *     "revision_delete" = "/admin/gtfs/service/{gtfs_service}/revisions/{gtfs_service_revision}/delete",
 *     "translation_revert" = "/admin/gtfs/service/{gtfs_service}/revisions/{gtfs_service_revision}/revert/{langcode}",
 *     "collection" = "/admin/gtfs/service",
 *   },
 *   field_ui_base_route = "gtfs_service.settings"
 * )
 */
class Service extends GTFSEntityBase {
  use HasIdTrait;

  public static $resourceURL = '/gtfs/api/v1/services';

  /**
   * {@inheritdoc}
   */
  public static function schema(): array {
    return [
      'fields' => [
        'service_id' => ['type' => 'varchar', 'length' => 128, 'not null' => TRUE],
        'monday' => ['type' => 'int', 'length' => 1],
        'tuesday' => ['type' => 'int', 'length' => 1],
        'wednesday' => ['type' => 'int', 'length' => 1],
        'thursday' => ['type' => 'int', 'length' => 1],
        'friday' => ['type' => 'int', 'length' => 1],
        'saturday' => ['type' => 'int', 'length' => 1],
        'sunday' => ['type' => 'int', 'length' => 1],
        'start_date' => ['type' => 'varchar', 'length' => 8],
        'end_date' => ['type' => 'varchar', 'length' => 8],
      ],
      'primary key' => ['service_id'],
    ];
  }

  /**
   *
   */
  public static function getCurrentId() {
    $day_of_week = strtolower(date('l'));
    return \Drupal::database()->query(
      "SELECT `service_id`
       FROM {gtfs_service_field_data}
       WHERE `{$day_of_week}` = '1'
       ORDER BY `service_id`
       LIMIT 1"
    )->fetch(\PDO::FETCH_COLUMN);
  }

  /**
   * Saves all services after insert to trigger hook calculating name.
   *
   * @param $feed_id
   *  The ID of the feed which has just been imported.
   *
   * @return array
   *  The batch operations to save services
   */
  public static function postImportBatchOperations($feed_id): array {
    $operations = parent::postImportBatchOperations($feed_id);
    $services = \Drupal::database()->query(
      'SELECT `id`
       FROM {gtfs_service_field_data}
       WHERE `feed_reference__target_id` = :frti',
      [':frti' => $feed_id]
    )->fetchAll(\PDO::FETCH_COLUMN, 0);
    $operations = $operations + array_map(function ($chunk) {
      return [
        'gtfs_save_chunk_of_ids',
        [$chunk, static::class],
      ];
    }, array_chunk($services, 200));
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    $days_effective = [];
    foreach ($days as $day) {
      if ($this->get($day)->value) {
        $days_effective[] = $day;
      }
    }
    if ($days_effective === ['saturday', 'sunday']) {
      $name = 'Weekend';
    }
    elseif ($days_effective === ['monday', 'tuesday', 'wednesday', 'thursday', 'friday']) {
      $name = 'Weekday';
    }
    else {
      $name = implode(', ', array_map('ucfirst', $days_effective));
    }
    $name .= ' Service';
    $this->setName($name);
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  public static function loadMultipleFromDate(\DateTime $date) {
    $day_of_week = strtolower($date->format('l'));
    $ids = \Drupal::database()->select('gtfs_service_field_data', 'f')
      ->fields('f', ['id'])
      ->condition("f.{$day_of_week}", 1)
      ->condition("f.start_date", (int) $date->format('Ymd'), '<')
      ->condition("f.end_date", $date->format('Ymd'), '>')
      ->addTag('gtfs_service_access')
      ->execute()
    ->fetchCol();
    return static::loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the GTFS Service entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 256,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['service_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Service ID'))
      ->setDescription(t('The service_id contains an ID that uniquely identifies a set of dates when service is available for one or more routes. Each service_id value can appear at most once in a calendar.txt file. This value is dataset unique. It is referenced by the trips.txt file.'))
    // This is how we map to the service.
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['monday'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Monday'))
      ->setDescription(t('The Monday field contains a binary value that indicates whether the service is valid for all Mondays.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['tuesday'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Tuesday'))
      ->setDescription(t('The Tuesday field contains a binary value that indicates whether the service is valid for all Tuesdays.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['wednesday'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Wednesday'))
      ->setDescription(t('The Wednesday field contains a binary value that indicates whether the service is valid for all Wednesdays.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['thursday'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Thursday'))
      ->setDescription(t('The Thursday field contains a binary value that indicates whether the service is valid for all Thursdays.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['friday'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Friday'))
      ->setDescription(t('The Friday field contains a binary value that indicates whether the service is valid for all Fridays.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['saturday'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Saturday'))
      ->setDescription(t('The Saturday field contains a binary value that indicates whether the service is valid for all Saturdays.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['sunday'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Sunday'))
      ->setDescription(t('The Sunday field contains a binary value that indicates whether the service is valid for all Sundays.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['start_date'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Start date'))
      ->setDescription(t('The start date for the service.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'weight' => -3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['end_date'] = BaseFieldDefinition::create('string')
      ->setLabel(t('End date'))
      ->setDescription(t('The end date for the service.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'weight' => -3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
