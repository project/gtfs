<?php

namespace Drupal\gtfs\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the GTFS Fare Attribute entity.
 *
 * @ingroup gtfs
 *
 * @ContentEntityType(
 *   id = "gtfs_fare_attribute",
 *   label = @Translation("GTFS Fare Attribute"),
 *   handlers = {
 *     "storage" = "Drupal\gtfs\FareAttributeStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gtfs\FareAttributeListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "controller" = "Drupal\gtfs\Controller\FareAttributeController",
 *     "form" = {
 *       "default" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "add" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "edit" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "revision_revert" = "Drupal\gtfs\Form\FareAttributeRevisionRevertForm",
 *       "revision_delete" = "\Drupal\gtfs\Form\FareAttributeRevisionDeleteForm",
 *       "translation_revert" = "\Drupal\gtfs\Form\FareAttributeRevisionRevertTranslationForm",
 *       "settings" = "Drupal\gtfs\Form\FareAttributeSettingsForm",
 *     },
 *     "access" = "Drupal\gtfs\GTFSObjectAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gtfs\GTFSEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gtfs_fare_attribute",
 *   data_table = "gtfs_fare_attribute_field_data",
 *   revision_table = "gtfs_fare_attribute_revision",
 *   revision_data_table = "gtfs_fare_attribute_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer gtfs fare_attribute entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "fare_id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *    revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message"
 *   },
 *   links = {
 *     "canonical" = "/gtfs/fare_attribute/{gtfs_fare_attribute}",
 *     "add-form" = "/admin/gtfs/fare_attribute/add",
 *     "edit-form" = "/admin/gtfs/fare_attribute/{gtfs_fare_attribute}/edit",
 *     "delete-form" = "/admin/gtfs/fare_attribute/{gtfs_fare_attribute}/delete",
 *     "version-history" = "/admin/gtfs/fare_attribute/{gtfs_fare_attribute}/revisions",
 *     "revision" = "/admin/gtfs/fare_attribute/{gtfs_fare_attribute}/revisions/{gtfs_fare_attribute_revision}/view",
 *     "revision_revert" = "/admin/gtfs/fare_attribute/{gtfs_fare_attribute}/revisions/{gtfs_fare_attribute_revision}/revert",
 *     "revision_delete" = "/admin/gtfs/fare_attribute/{gtfs_fare_attribute}/revisions/{gtfs_fare_attribute_revision}/delete",
 *     "translation_revert" = "/admin/gtfs/fare_attribute/{gtfs_fare_attribute}/revisions/{gtfs_fare_attribute_revision}/revert/{langcode}",
 *     "collection" = "/admin/gtfs/fare_attribute",
 *   },
 *   field_ui_base_route = "gtfs_fare_attribute.settings"
 * )
 */
class FareAttribute extends GTFSEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(): array {
    return [
      'fields' => [
        'fare_id' => ['type' => 'varchar', 'length' => 128, 'not null' => TRUE],
        'price' => ['type' => 'numeric', 'precision' => 9, 'scale' => 6],
        'currency_type' => ['type' => 'varchar', 'length' => 8],
        'payment_method' => ['type' => 'int', 'length' => 2],
        'transfers' => ['type' => 'int', 'length' => 2],
        'agency_id' => ['type' => 'varchar', 'length' => 128],
        'transfer_duration' => ['type' => 'int', 'length' => 11],
      ],
      'primary key' => ['fare_id'],
      'foreign keys' => [
        'agency' => [
          'table' => 'gtfs_agency',
          'columns' => ['agency_id' => 'id'],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('fare_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['fare_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Fare ID'))
      ->setDescription(t('The fare_id field contains an ID that uniquely identifies a fare class. The fare_id is dataset unique'))
      ->setRevisionable(TRUE) // This is how we map to the feed
      ->setSettings([
          'max_length' => 255,
          'text_processing' => 0
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string'
      ])
      ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => -4
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['price'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Price'))
      ->setDescription(t('The price field contains the fare price, in the unit specified by currency_type.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string'
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -3
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['currency_type'] = BaseFieldDefinition::create('currency_field')
      ->setLabel(t('Currency'))
      ->setDescription(t('The currency_type field defines the currency used to pay the fare. '))
      ->setSettings([
        'display' => 'AlphabeticCode'
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -3
      ])
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['payment_method'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Payment Method'))
      ->setDescription(t('The payment_method field indicates when the fare must be paid.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'allowed_values' => [
         '1' => 'On Board',
         '2' => 'Before Boarding'
        ]
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -3
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['transfers'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Transfers'))
      ->setDescription(t('The transfers field specifies the number of transfers permitted on this fare. '))
      ->setRevisionable(TRUE)
      ->setSettings([
        'allowed_values' => [
          '0' => 'No transfers permitted on this fare',
          '1' => 'Passenger may transfer once',
          '2' => 'Passenger may transfer twice'
        ]
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -3
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['agency_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Agency'))
      ->setDescription(t('Which agency the fare applies to.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'target_type' => 'gtfs_agency',
        'default_value' => 0,
        'handler' => 'default'
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['transfer_duration'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Transfer Duration'))
      ->setDescription(t('The transfer_duration field specifies the length of time in seconds before a transfer expires. When used with a transfers value of 0, the transfer_duration field indicates how long a ticket is valid for a fare where no transfers are allowed. Unless you intend to use this field to indicate ticket validity, transfer_duration should be omitted or empty when transfers is set to 0.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string'
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -3
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    return $fields;
  }

}
