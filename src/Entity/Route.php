<?php /** @noinspection Annotator */

namespace Drupal\gtfs\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the GTFS Route entity.
 *
 * @ingroup gtfs
 *
 * @ContentEntityType(
 *   id = "gtfs_route",
 *   label = @Translation("GTFS Route"),
 *   handlers = {
 *     "storage" = "Drupal\gtfs\RouteStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gtfs\RouteListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "controller" = "Drupal\gtfs\Controller\RouteController",
 *     "form" = {
 *       "default" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "add" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "edit" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "revision_revert" = "Drupal\gtfs\Form\RouteRevisionRevertForm",
 *       "revision_delete" = "\Drupal\gtfs\Form\RouteRevisionDeleteForm",
 *       "translation_revert" = "\Drupal\gtfs\Form\RouteRevisionRevertTranslationForm",
 *       "settings" = "Drupal\gtfs\Form\RouteSettingsForm",
 *     },
 *     "access" = "Drupal\gtfs\GTFSObjectAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gtfs\GTFSEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gtfs_route",
 *   data_table = "gtfs_route_field_data",
 *   revision_table = "gtfs_route_revision",
 *   revision_data_table = "gtfs_route_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer gtfs route entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "route_short_name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *    revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message"
 *   },
 *   links = {
 *     "canonical" = "/gtfs/route/{gtfs_route}",
 *     "add-form" = "/admin/gtfs/route/add",
 *     "edit-form" = "/admin/gtfs/route/{gtfs_route}/edit",
 *     "delete-form" = "/admin/gtfs/route/{gtfs_route}/delete",
 *     "version-history" = "/admin/gtfs/route/{gtfs_route}/revisions",
 *     "revision" = "/admin/gtfs/route/{gtfs_route}/revisions/{gtfs_route_revision}/view",
 *     "revision_revert" = "/admin/gtfs/route/{gtfs_route}/revisions/{gtfs_route_revision}/revert",
 *     "revision_delete" = "/admin/gtfs/route/{gtfs_route}/revisions/{gtfs_route_revision}/delete",
 *     "translation_revert" = "/admin/gtfs/route/{gtfs_route}/revisions/{gtfs_route_revision}/revert/{langcode}",
 *     "collection" = "/admin/gtfs/route",
 *   },
 *   field_ui_base_route = "gtfs_route.settings"
 * )
 */
class Route extends GTFSEntityBase implements GeodataInterface {

  use HasIdTrait;

  public static $resourceURL = '/gtfs/api/v1/agencies/{agency_id}/routes';

  /**
   * {@inheritdoc}
   */
  public static function schema(): array {
    return [
      'fields' => [
        'route_id' => ['type' => 'varchar', 'length' => 128],
        'agency_id' => ['type' => 'varchar', 'length' => 128],
        'route_short_name' => ['type' => 'varchar', 'length' => 255],
        'route_long_name' => ['type' => 'varchar', 'length' => 255],
        'route_desc' => ['type' => 'text', 'length' => 4096],
        'route_type' => ['type' => 'int', 'length' => 2],
        'route_url' => ['type' => 'varchar', 'length' => 255],
        'route_color' => ['type' => 'varchar', 'length' => 10],
        'route_text_color' => ['type' => 'varchar', 'length' => 10],
        'route_sort_order' => ['type' => 'int', 'length' => 10],
      ],
      'primary key' => ['route_id'],
      'foreign keys' => [
        'agency' => [
          'table' => 'gtfs_agency',
          'columns' => ['agency_id' => 'id'],
        ],
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public static function getById(Agency $agency, $route_id) {
    $id = \Drupal::database()->query(
      "SELECT `id`
       FROM {gtfs_route_field_data}
       WHERE `route_id` = :route_id
       AND `agency_id` = :agency_id
       AND `feed_reference__target_id` = :feed_id",
      [
        ':route_id' => $route_id,
        ':agency_id' => $agency->id(),
        ':feed_id' => $agency->get('feed_reference')->target_id,
      ]
    )->fetch(\PDO::FETCH_COLUMN);

    return self::load($id);
  }

  public static function getFromSource($agency_id, $route_id) {
    return \Drupal::database()->query('
        SELECT *
        FROM {gtfs_route_source}
        WHERE `route_id` = :route_id
        AND `agency_id` = :agency_id
        AND `feed_reference__target_revision_id` IN (
            SELECT MAX(feed_reference__target_revision_id)
            FROM {gtfs_route_source}
            GROUP BY route_id
        )
    ', [
      ':agency_id' => $agency_id,
      ':route_id' => $route_id,
    ])->fetch(\PDO::FETCH_ASSOC);
  }


  /**
   * Gets the agency associated with this route.
   *
   * @return \Drupal\gtfs\Entity\GTFSObjectInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function agency(): GTFSObjectInterface {
    return Agency::storage()->load($this->get('agency_id')->target_id);
  }

  /**
   *
   */
  public function tripIds($directionIds = NULL) {
    if (is_null($directionIds)) {
      $directionIds = \Drupal::database()->query(
        "SELECT `direction_id`
      FROM {gtfs_direction_field_data}
      WHERE `route_id` = :id",
        [':id' => $this->id()]
      )->fetchAll(\PDO::FETCH_COLUMN, 0);
    } else if (is_string($directionIds)) {
      $directionIds = explode(',', $directionIds);
    }
    $query = "SELECT `id`
       FROM {gtfs_trip_field_data}
       WHERE `route_id` = :route_id";
    $placeholders = [':route_id' => $this->id()];
    if (count($directionIds)) {
      $query .= " AND `direction_id` IN (:direction_ids[])";
      $placeholders[':direction_ids[]'] = $directionIds;
    }
    return \Drupal::database()
      ->query($query, $placeholders)
      ->fetchAll(\PDO::FETCH_COLUMN, 0);
  }

  /**
   *
   */
  public function trips() {
    return Trip::loadMultiple($this->tripIds());
  }

  /**
   *
   */
  public function stopTimeIds() {
    $trip_ids = $this->tripIds();
    if (count($trip_ids) === 0) return [];
    return \Drupal::database()->query(
      "SELECT `id`
       FROM {gtfs_stop_time_field_data}
       WHERE `trip_id` IN (:tripIds[])",
      [
        ':route_id' => $this->id(),
        ':tripIds[]' => $trip_ids,
      ]
    )->fetchAll(\PDO::FETCH_COLUMN, 0);
  }

  /**
   *
   */
  public function stopTimes() {
    return StopTime::loadMultiple($this->stopTimeIds());
  }

  /**
   *
   */
  public function stopIds($directionIds = NULL) {

    $trip_ids = $this->tripIds($directionIds);
    if (count($trip_ids) === 0) return [];
    return \Drupal::database()->query(
      "SELECT `id`
       FROM {gtfs_stop_field_data}
       WHERE `id` IN (
         SELECT `stop_id`
         FROM {gtfs_stop_time_field_data}
         WHERE `trip_id` IN  (:tripIds[])
       )",
      [
        ':tripIds[]' => $trip_ids,
      ]
    )->fetchAll(\PDO::FETCH_COLUMN);
  }

  /**
   *
   */
  public function stops() {
    $stop_ids = $this->stopIds();
    if (count($stop_ids) === 0) return [];
    return Stop::loadMultiple($stop_ids);
  }

  /**
   *
   */
  public function services() {
    $route_id = $this->id();
    return Service::loadMultiple(\Drupal::database()->query(
      "SELECT `id`
       FROM {gtfs_service_field_data}
       WHERE `id` IN (
         SELECT `service_id`
         FROM {gtfs_trip_field_data}
         WHERE `route_id` = '{$route_id}'
       )"
    )->fetchAll(\PDO::FETCH_COLUMN, 0));
  }

  public static function servicesSource($route_id) {
    return \Drupal::database()->query('
        SELECT *
        FROM {gtfs_service_source}
        WHERE `service_id` IN (
            SELECT `service_id`
            FROM {gtfs_trip_source}
            WHERE `route_id` = :route_id
            AND `feed_reference__target_revision_id` IN (
                SELECT MAX(feed_reference__target_revision_id)
                FROM {gtfs_trip_source}
                GROUP BY trip_id
            )
        )
        AND `feed_reference__target_revision_id` IN (
            SELECT MAX(feed_reference__target_revision_id)
            FROM {gtfs_service_source}
            GROUP BY service_id
        )
    ', [
      ':route_id' => $route_id,
    ])->fetchAll(\PDO::FETCH_ASSOC);
  }

  public function directions() {
    $route_id = $this->id();
    return Direction::loadMultiple(\Drupal::database()->query(
      "SELECT `id`
      FROM {gtfs_direction_field_data}
      WHERE `route_id` = :id",
      [':id' => $route_id]
    )->fetchAll(\PDO::FETCH_COLUMN, 0));
  }

  public static function directionsSource($route_id) {
    return \Drupal::database()->query('
        SELECT *
        FROM {gtfs_direction_source}
        WHERE `route_id` = :route_id
        AND `feed_reference__target_revision_id` IN (
            SELECT MAX(feed_reference__target_revision_id)
            FROM {gtfs_direction_source}
            GROUP BY route_id,direction_id
        )
    ', [
      ':route_id' => $route_id,
    ])->fetchAll(\PDO::FETCH_ASSOC);
  }

  public function geojson(): array {
    $shape_ids = \Drupal::database()->query(
      'SELECT `shape_id`
          FROM {gtfs_shape_field_data}
          WHERE `id` IN (
            SELECT `shape_id`
            FROM {gtfs_trip_field_data}
            WHERE `route_id` = :route_id
        )',
      [':route_id' => $this->id()]
    )->fetchAll(\PDO::FETCH_COLUMN);
    return [
      'type' => 'MultiLineString',
      'properties' => [
        'name' => $this->getName()
      ],
      'coordinates' => array_map(function ($shape_id) {
        $coordinates = \Drupal::database()->query(
          'SELECT `shape_pt_lon`,`shape_pt_lat`
            FROM {gtfs_shape_field_data}
            WHERE `shape_id` = :shape_id
            ORDER BY `shape_pt_sequence` ASC',
          [':shape_id' => $shape_id]
        )->fetchAll(\PDO::FETCH_NUM);
        return array_map(function ($pair) {
          return array_map('floatval', $pair);
        }, $coordinates);
      }, $shape_ids)
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function resourceUrl(string $append = ''): string {
    $link = str_replace('{agency_id}', $this->agency()->get('agency_id')->value, self::$resourceURL) . '/' . $this->get('route_id')->value;
    $url = \Drupal::request()->getSchemeAndHttpHost() . $link;
    $url .= $append;
    \Drupal::moduleHandler()->alter('gtfs_entity_resource_url', $url, $this);
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function links(): array {
    $links = [
      'agency' => $this->agency()->resourceUrl(),
      'trips' => $this->resourceUrl() . '/trips',
      'stops' => $this->resourceUrl() . '/stops',
      'directions' => $this->resourceUrl() . '/directions',
      'geojson' => $this->resourceUrl() . '/geojson',
      'shapes' => \Drupal::request()->getSchemeAndHttpHost() . Shape::$resourceURL . '/' . implode(',', array_unique(array_map(function ($trip) {
          return $trip->shapeId();
        }, $this->trips()))),
    ];
    return array_merge(parent::links(), $links);
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('route_short_name')->value . ' ' . $this->get('route_long_name')->value;
  }

  /**
   * {@inheritDoc}
   */
  public function label() {
    return $this->getName();
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('route_long_name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['route_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Route ID'))
      ->setDescription(t('The route_id field contains an ID that uniquely identifies a route. The route_id is dataset unique.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['agency_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Route agency'))
      ->setDescription(t('The agency_id field defines an agency for the specified route. This value is referenced from the agency.txt file. Use this field when you are providing data for routes from more than one agency.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'target_type' => 'gtfs_agency',
        'default_value' => 0,
        'handler' => 'default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['route_short_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Route Short Name'))
      ->setDescription(t('The route_short_name contains the short name of a route. This will often be a short, abstract identifier like "32", "100X", or "Green" that riders use to identify a route, but which doesn\'t give any indication of what places the route serves. At least one of route_short_name or route_long_name must be specified, or potentially both if appropriate. If the route does not have a short name, please specify a route_long_name and use an empty string as the value for this field.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['route_long_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Route Long Name'))
      ->setDescription(t('The route_long_name contains the full name of a route. This name is generally more descriptive than the route_short_name and will often include the route\'s destination or stop. At least one of route_short_name or route_long_name must be specified, or potentially both if appropriate. If the route does not have a long name, please specify a route_short_name and use an empty string as the value for this field.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['route_desc'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Route Description'))
      ->setDescription(t('The route_desc field contains a description of a route. Please provide useful, quality information. Do not simply duplicate the name of the route. For example, "A trains operate between Inwood-207 St, Manhattan and Far Rockaway-Mott Avenue, Queens at all times. Also from about 6AM until about midnight, additional A trains operate between Inwood-207 St and Lefferts Boulevard (trains typically alternate between Lefferts Blvd and Far Rockaway)."'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['route_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Route Type'))
      ->setDescription(t('The route_type field describes the type of transportation used on a route.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'allowed_values' => [
          '0' => 'Tram, Streetcar, Light rail',
          '1' => 'Subway, Metro',
          '2' => 'Rail',
          '3' => 'Bus',
          '4' => 'Ferry',
          '5' => 'Cable tram',
          '6' => 'Aerial lift',
          '7' => 'Funicular',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['route_url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Route URL'))
      ->setDescription(t('The route_url field contains the URL of a web page about that particular route. This should be different from the agency_url. The value must be a fully qualified URL that includes https:// or https://, and any special characters in the URL must be correctly escaped. See http://www.w3.org/Addressing/URL/4_URI_Recommentations.html for a description of how to create fully qualified URL values.'))
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['route_color'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Route Color'))
      ->setDescription(t('In systems that have colors assigned to routes, the route_color field defines a color that corresponds to a route. The color must be provided as a six-character hexadecimal number, for example, 00FFFF. If no color is specified, the default route color is white (FFFFFF). The color difference between route_color and route_text_color should provide sufficient contrast when viewed on a black and white screen. The W3C Techniques for Accessibility Evaluation And Repair Tools document offers a useful algorithm for evaluating color contrast. There are also helpful online tools for choosing contrasting colors, including the snook.ca Color Contrast Check application.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 7,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['route_text_color'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Route Text Color'))
      ->setDescription(t('The route_text_color field can be used to specify a legible color to use for text drawn against a background of route_color. The color must be provided as a six-character hexadecimal number, for example, FFD700. If no color is specified, the default text color is black (000000). The color difference between route_color and route_text_color should provide sufficient contrast when viewed on a black and white screen.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 7,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['route_sort_order'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Route Sort Order'))
      ->setDescription(t('The route_sort_order field can be used to order the routes in a way which is ideal for presentation to customers. It must be a non-negative integer. Routes with smaller route_sort_order values should be displayed before routes with larger route_sort_order values.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    return $fields;
  }

}
