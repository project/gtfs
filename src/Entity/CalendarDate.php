<?php

namespace Drupal\gtfs\Entity;

use Yasumi\Yasumi;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the GTFS Calendar Date entity.
 *
 * @ingroup gtfs
 *
 * @ContentEntityType(
 *   id = "gtfs_calendar_date",
 *   label = @Translation("GTFS Calendar Date"),
 *   handlers = {
 *     "storage" = "Drupal\gtfs\CalendarDateStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gtfs\CalendarDateListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "controller" = "Drupal\gtfs\Controller\CalendarDateController",
 *     "form" = {
 *       "default" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "add" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "edit" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "revision_revert" = "Drupal\gtfs\Form\CalendarDateRevisionRevertForm",
 *       "revision_delete" = "\Drupal\gtfs\Form\CalendarDateRevisionDeleteForm",
 *       "translation_revert" = "\Drupal\gtfs\Form\CalendarDateRevisionRevertTranslationForm",
 *       "settings" = "Drupal\gtfs\Form\CalendarDateSettingsForm",
 *     },
 *     "access" = "Drupal\gtfs\GTFSObjectAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gtfs\GTFSEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gtfs_calendar_date",
 *   data_table = "gtfs_calendar_date_field_data",
 *   revision_table = "gtfs_calendar_date_revision",
 *   revision_data_table = "gtfs_calendar_date_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer gtfs calendar_date entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "date",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *    revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message"
 *   },
 *   links = {
 *     "canonical" = "/gtfs/calendar_date/{gtfs_calendar_date}",
 *     "add-form" = "/admin/gtfs/calendar_date/add",
 *     "edit-form" = "/admin/gtfs/calendar_date/{gtfs_calendar_date}/edit",
 *     "delete-form" = "/admin/gtfs/calendar_date/{gtfs_calendar_date}/delete",
 *     "version-history" = "/admin/gtfs/calendar_date/{gtfs_calendar_date}/revisions",
 *     "revision" = "/admin/gtfs/calendar_date/{gtfs_calendar_date}/revisions/{gtfs_calendar_date_revision}/view",
 *     "revision_revert" = "/admin/gtfs/calendar_date/{gtfs_calendar_date}/revisions/{gtfs_calendar_date_revision}/revert",
 *     "revision_delete" = "/admin/gtfs/calendar_date/{gtfs_calendar_date}/revisions/{gtfs_calendar_date_revision}/delete",
 *     "translation_revert" = "/admin/gtfs/calendar_date/{gtfs_calendar_date}/revisions/{gtfs_calendar_date_revision}/revert/{langcode}",
 *     "collection" = "/admin/gtfs/calendar_date",
 *   },
 *   field_ui_base_route = "gtfs_calendar_date.settings"
 * )
 */
class CalendarDate extends GTFSEntityBase {

  const GTFS_FORMAT = 'Ymd';

  const DB_FORMAT = 'Ymd';

  const EXCEPTION_TYPE_ADDED = '1';

  const EXCEPTION_TYPE_REMOVED = '2';

  /**
   * {@inheritdoc}
   */
  public static function schema(): array {
    return [
      'fields' => [
        'service_id' => [
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
        ],
        'date' => ['type' => 'varchar', 'length' => 8, 'not null' => TRUE,],
        'exception_type' => ['type' => 'int', 'length' => 2],
      ],
      'primary key' => ['service_id', 'date'],
      'foreign keys' => [
        'service' => [
          'table' => 'gtfs_service',
          'columns' => ['service_id' => 'id',],
        ],
      ],
    ];
  }

  /**
   * Returns the associated {@see \Drupal\gtfs\Entity\Service}.
   *
   * @return \Drupal\gtfs\Entity\Service
   */
  public function service(): Service {
    $service_id = $this->get('service_id')->target_id;
    return Service::load($service_id);
  }

  /**
   * Utility function get date object.
   *
   * @return \DateTime|false
   */
  public function date() {
    return \DateTime::createFromFormat(self::DB_FORMAT, str_replace('-', '', $this->get('date')->value));
  }

  /**
   * Returns the name of this date, using the holiday date if possible.
   *
   * @return string
   * @throws \ReflectionException
   */
  public function dateName(): string {
    $date = $this->date();
    $date_name = null;
    $provider_classname = '';
    \Drupal::moduleHandler()->alter('gtfs_yasumi_provider', $provider_classname);

    if (class_exists('\Yasumi\Yasumi') && $date) {
      $provider = Yasumi::create($provider_classname, (int) date('Y', $date->getTimestamp()));

      foreach($provider->getHolidays() as $holiday) {
        if($holiday->format(self::GTFS_FORMAT) === $date->format(self::GTFS_FORMAT)) {
          $date_name = $holiday->getName();
        }
      }
    } else {
      \Drupal::logger('GTFS Calendar Date')->warning('Yasumi is not installed but GTFS Calendar Dates are being requested. Yasumi will output standard holiday names');
    }

    if(!$date_name && $date) {
      $date_name = $date->format('F jS, Y');
    }
    return $date_name ?: '';
  }

  public function correspondant() {
    $id = \Drupal::database()->query('
      SELECT id
      FROM {gtfs_calendar_date_field_data}
      WHERE date = :this_date
      AND exception_type <> :this_exception_type
      AND feed_reference__target_id  = :this_frti
    ', [
      ':this_date' => $this->get('date')->value,
      ':this_exception_type' => $this->get('exception_type')->value,
      ':this_frti' => $this->get('feed_reference')->target_id,
    ])->fetchField();
    if ($id) {
      return static::load($id);
    }
    return false;
  }

  /**
   * {@inheritdoc}
   */
  public function toGTFSObject(): array {
    $return = parent::toGTFSObject();
    $date = $this->date();
    if ($date) {
        $return['date'] = $date->format(self::GTFS_FORMAT);
    }
    $return['name'] = $this->dateName();
    unset($return['self']);
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    $action = $this->get('exception_type')->value == self::EXCEPTION_TYPE_ADDED ? 'Added' : 'Removed';
    $service = $this->service()->getName();
    $datename = $this->dateName();
    return "{$action} {$service} on {$datename}";
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['service_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Service ID'))
      ->setDescription(t('The service_id contains an ID that uniquely identifies a set of dates when a service exception is available for one or more routes. Each (service_id, date) pair can only appear once in calendar_dates.txt. If the a service_id value appears in both the calendar.txt and calendar_dates.txt files, the information in calendar_dates.txt modifies the service information specified in calendar.txt. This field is referenced by the trips.txt file.'))
      ->setRevisionable(TRUE)
      ->setSettings([
         'target_type' => 'gtfs_service',
         'default_value' => 0,
         'handler' => 'default'
      ])
      ->setDisplayOptions('form', [
         'type' => 'entity_reference_autocomplete',
         'weight' => -3,
         'settings' => [
       'match_operator' => 'CONTAINS',
       'size' => '60',
       'autocomplete_type' => 'tags',
       'placeholder' => '',
         ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Date'))
      ->setDescription(t('The date field specifies a particular date when service availability is different than the norm. You can use the exception_type field to indicate whether service is available on the specified date.'))
      ->setSetting('datetime_type', 'date')
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => -3,
      ])
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['exception_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Exception Type'))
      ->setDescription(t('The exception_type indicates whether service is available on the date specified in the date field.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue('1')
      ->setSettings([
        'allowed_values' => [
           self::EXCEPTION_TYPE_ADDED => 'Added',
           self::EXCEPTION_TYPE_REMOVED => 'Removed'
        ]
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -3
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    return $fields;
  }

}
