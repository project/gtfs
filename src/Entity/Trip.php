<?php

namespace Drupal\gtfs\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the GTFS Trip entity.
 *
 * @ingroup gtfs
 *
 * @ContentEntityType(
 *   id = "gtfs_trip",
 *   label = @Translation("GTFS Trip"),
 *   handlers = {
 *     "storage" = "Drupal\gtfs\TripStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gtfs\TripListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "controller" = "Drupal\gtfs\Controller\TripController",
 *     "form" = {
 *       "default" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "add" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "edit" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "revision_revert" = "Drupal\gtfs\Form\TripRevisionRevertForm",
 *       "revision_delete" = "\Drupal\gtfs\Form\TripRevisionDeleteForm",
 *       "translation_revert" = "\Drupal\gtfs\Form\TripRevisionRevertTranslationForm",
 *       "settings" = "Drupal\gtfs\Form\TripSettingsForm",
 *     },
 *     "access" = "Drupal\gtfs\GTFSObjectAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gtfs\GTFSEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gtfs_trip",
 *   data_table = "gtfs_trip_field_data",
 *   revision_table = "gtfs_trip_revision",
 *   revision_data_table = "gtfs_trip_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer gtfs trip entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "trip_id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *    revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message"
 *   },
 *   links = {
 *     "canonical" = "/gtfs/trip/{gtfs_trip}",
 *     "add-form" = "/admin/gtfs/trip/add",
 *     "edit-form" = "/admin/gtfs/trip/{gtfs_trip}/edit",
 *     "delete-form" = "/admin/gtfs/trip/{gtfs_trip}/delete",
 *     "version-history" = "/admin/gtfs/trip/{gtfs_trip}/revisions",
 *     "revision" = "/admin/gtfs/trip/{gtfs_trip}/revisions/{gtfs_trip_revision}/view",
 *     "revision_revert" = "/admin/gtfs/trip/{gtfs_trip}/revisions/{gtfs_trip_revision}/revert",
 *     "revision_delete" = "/admin/gtfs/trip/{gtfs_trip}/revisions/{gtfs_trip_revision}/delete",
 *     "translation_revert" = "/admin/gtfs/trip/{gtfs_trip}/revisions/{gtfs_trip_revision}/revert/{langcode}",
 *     "collection" = "/admin/gtfs/trip",
 *   },
 *   field_ui_base_route = "gtfs_trip.settings"
 * )
 */
class Trip extends GTFSEntityBase implements GeodataInterface {
  use HasIdTrait;

  public static $resourceURL = '/gtfs/api/v1/agencies/{agency_id}/routes/{route_id}/trips';

  /**
   * {@inheritdoc}
   */
  public static function schema(): array {
    return [
      'fields' => [
        'route_id' => ['type' => 'varchar', 'length' => 128],
        'service_id' => ['type' => 'varchar', 'length' => 128],
        'trip_id' => ['type' => 'varchar', 'length' => 128, 'not null' => TRUE],
        'trip_headsign' => ['type' => 'varchar', 'length' => 255],
        'trip_short_name' => ['type' => 'varchar', 'length' => 255],
        'direction_id' => ['type' => 'int', 'length' => 1],
        'block_id' => ['type' => 'varchar', 'length' => 8],
        'shape_id' => ['type' => 'varchar', 'length' => 128],
        'wheelchair_accessible' => ['type' => 'int', 'length' => 2],
        'bikes_allowed' => ['type' => 'int', 'length' => 2],
      ],
      'primary key' => ['trip_id'],
      'foreign keys' => [
        'route' => [
          'table' => 'gtfs_route',
          'columns' => ['route_id' => 'id'],
        ],
        'service' => [
          'table' => 'gtfs_service',
          'columns' => ['service_id' => 'id'],
        ],
        'shape' => [
          'table' => 'gtfs_shape',
          'columns' => ['shape_id' => 'id'],
        ],
      ],
    ];
  }


  /**
   * {@inheritDoc}
   */
  public static function getById(Route $route, $trip_id) {
    $id = \Drupal::database()->query(
      "SELECT `id`
       FROM {gtfs_trip_field_data}
       WHERE `trip_id` = :trip_id
       AND `route_id` = :route_id
       AND `feed_reference__target_id` = :feed_id",
      [
        ':trip_id' => $trip_id,
        ':route_id' => $route->id(),
        ':feed_id' => $route->get('feed_reference')->target_id,
      ]
    )->fetch(\PDO::FETCH_COLUMN);

    return Trip::load($id);
  }

  /**
   *
   */
  public function stop_times() {
    return StopTime::loadMultiple(\Drupal::database()->query(
      'SELECT `id`
       FROM {gtfs_stop_time_field_data}
       WHERE `trip_id` = :trip_id',
      [':trip_id' => $this->id()]
    )->fetchAll(\PDO::FETCH_COLUMN, 0));
  }

  public function stops() {
    return Stop::loadMultiple(\Drupal::database()->query(
      'SELECT `stop_id`
      FROM {gtfs_stop_time_field_data}
      WHERE `trip_id` = :trip_id',
      [':trip_id' => $this->id()]
    )->fetchAll(\PDO::FETCH_COLUMN));
  }

  /**
   *
   */
  public function route() {
    $route_id = $this->get('route_id')->target_id;
    return Route::load($route_id);
  }

  /**
   *
   */
  public function shapes() {
    $shape_ids = \Drupal::database()->query(
      "SELECT `id`
       FROM {gtfs_shape_field_data}
       WHERE `shape_id` = :shape_id",
      [':shape_id' => $this->shapeId()]
    )->fetchAll(\PDO::FETCH_COLUMN, 0);
    return Shape::loadMultiple($shape_ids);
  }

  /**
   *
   */
  public function shapeId() {
    return \Drupal::database()->query(
      "SELECT `shape_id`
       FROM {gtfs_shape_field_data}
       WHERE `id` = :shape_id",
      [':shape_id' => $this->get('shape_id')->target_id]
    )->fetch(\PDO::FETCH_COLUMN);
  }

  public function geojson(): array {
    $any_shape = current($this->shapes());
    return $any_shape->geojson();
  }

  /**
   *
   */
  public function shapeResourceUrl() {
    $link = Shape::$resourceURL;
    $link = \Drupal::request()->getSchemeAndHttpHost() . $link . '/' . $this->shapeId();
    return $link;
  }

  /**
   * {@inheritdoc}
   */
  public function resourceUrl(string $append = ''): string {
    $agency_id = $this->route()->agency()->get('agency_id')->value;
    $route_id = $this->route()->get('route_id')->value;
    $tokens = ['{agency_id}', '{route_id}'];
    $values = [$agency_id, $route_id];
    $link = str_replace($tokens, $values, self::$resourceURL);
    $schema = static::schema();
    $unique_path = implode('/', array_map(function ($field) {
      return $this->get($field)->value;
    }, $schema['primary key']));
    $url = \Drupal::request()->getSchemeAndHttpHost() . $link . '/' . $unique_path;
    $url .= $append;
    \Drupal::moduleHandler()->alter('gtfs_entity_resource_url', $url, $this);
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function links(): array {
    $links = [
      'route' => $this->route()->resourceUrl(),
      'stopTimes' => $this->resourceUrl('/stopTimes'),
      'stops' => $this->resourceUrl('/stops'),
      'shape' => $this->shapeResourceUrl(),
      'geojson' => $this->resourceUrl() . '/geojson',
    ];
    return array_merge(parent::links(), $links);
  }

  /**
   * {@inheritdoc}
   */
  public function toGTFSObject(): array {
    $return = parent::toGTFSObject();
    $route_id = $this->get('route_id')->target_id;
    $service_id = $this->get('service_id')->target_id;
    $return['route_id'] = \Drupal::database()->query(
      "SELECT `route_id`
       FROM {gtfs_route_field_data}
       WHERE `id` = '{$route_id}'"
    )->fetch(\PDO::FETCH_COLUMN);
    $return['service_id'] = \Drupal::database()->query(
      "SELECT `service_id`
       FROM {gtfs_service_field_data}
       WHERE `id` = '{$service_id}'"
    )->fetch(\PDO::FETCH_COLUMN);
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    $headsign = $this->get('trip_headsign')->value;
    $trip_id = $this->get('trip_id')->value;
    return $headsign ?: $trip_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('trip_headsign', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['route_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Route ID'))
      ->setDescription(t('The route_id field contains an ID that uniquely identifies a route. This value is referenced from the routes.txt file.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'target_type' => 'gtfs_route',
        'default_value' => 0,
        'handler' => 'default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['service_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Service ID'))
      ->setDescription(t('The service_id contains an ID that uniquely identifies a set of dates when service is available for one or more routes. This value is referenced from the calendar.txt or calendar_dates.txt file.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'target_type' => 'gtfs_service',
        'default_value' => 0,
        'handler' => 'default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['trip_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Trip ID'))
      ->setDescription(t('The trip_id field contains an ID that identifies a trip. The trip_id is dataset unique.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['trip_headsign'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Trip Headsign'))
      ->setDescription(t('The trip_headsign field contains the text that appears on a sign that identifies the trip\'s destination to passengers. Use this field to distinguish between different patterns of service in the same route. If the headsign changes during a trip, you can override the trip_headsign by specifying values for the stop_headsign field in stop_times.txt.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['trip_short_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Trip Short Name'))
      ->setDescription(t('The trip_short_name field contains the text that appears in schedules and sign boards to identify the trip to passengers, for example, to identify train numbers for commuter rail trips. If riders do not commonly rely on trip names, please leave this field blank. A trip_short_name value, if provided, should uniquely identify a trip within a service day; it should not be used for destination names or limited/express designations.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['direction_id'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Direction ID'))
      ->setDescription(t('The direction_id field contains a binary value that indicates the direction of travel for a trip. Use this field to distinguish between bi-directional trips with the same route_id. This field is not used in routing; it provides a way to separate trips by direction when publishing time tables. You can specify names for each direction with the trip_headsign field.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'allowed_values' => [
          '0' => 'Outbound',
          '1' => 'Inbound',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['block_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Block ID'))
      ->setDescription(t('The block_id field identifies the block to which the trip belongs. A block consists of a single trip or many sequential trips made using the same vehicle, defined by shared service day and block_id. A block_id can have trips with different service days, making distinct blocks. '))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['shape_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Shape ID'))
      ->setDescription(t('The shape_id field contains an ID that defines a shape for the trip. This value is referenced from the shapes.txt file. The shapes.txt file allows you to define how a line should be drawn on the map to represent a trip.'))
      ->setSettings([
        'target_type' => 'gtfs_shape',
        'default_value' => 0,
        'handler' => 'default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['wheelchair_accessible'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Wheelchair Accessible'))
      ->setDescription(t(''))
      ->setRevisionable(TRUE)
      ->setDefaultValue('0')
      ->setSettings([
        'allowed_values' => [
          '0' => 'No information available',
          '1' => 'Yes',
          '2' => 'No',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['bikes_allowed'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t(''))
      ->setDescription(t(''))
      ->setRevisionable(TRUE)
      ->setDefaultValue('0')
      ->setSettings([
        'allowed_values' => [
          '0' => 'No information available',
          '1' => 'Yes',
          '2' => 'No',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
