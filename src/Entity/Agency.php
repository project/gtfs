<?php

namespace Drupal\gtfs\Entity;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Defines the GTFS Agency entity.
 *
 * @ingroup gtfs
 *
 * @ContentEntityType(
 *   id = "gtfs_agency",
 *   label = @Translation("GTFS Agency"),
 *   handlers = {
 *     "storage" = "Drupal\gtfs\AgencyStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gtfs\AgencyListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *     "controller" = "Drupal\gtfs\Controller\AgencyController",
 *     "form" = {
 *       "default" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "add" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "edit" = "Drupal\gtfs\Form\GTFSObjectForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "revision_revert" = "Drupal\gtfs\Form\AgencyRevisionRevertForm",
 *       "revision_delete" = "\Drupal\gtfs\Form\AgencyRevisionDeleteForm",
 *       "translation_revert" = "\Drupal\gtfs\Form\AgencyRevisionRevertTranslationForm",
 *       "settings" = "Drupal\gtfs\Form\AgencySettingsForm",
 *     },
 *     "access" = "Drupal\gtfs\GTFSObjectAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\gtfs\GTFSEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "gtfs_agency",
 *   data_table = "gtfs_agency_field_data",
 *   revision_table = "gtfs_agency_revision",
 *   revision_data_table = "gtfs_agency_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer gtfs agency entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "agency_name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *    revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message"
 *   },
 *   links = {
 *     "canonical" = "/gtfs/agency/{gtfs_agency}",
 *     "add-form" = "/admin/gtfs/agency/add",
 *     "edit-form" = "/admin/gtfs/agency/{gtfs_agency}/edit",
 *     "delete-form" = "/admin/gtfs/agency/{gtfs_agency}/delete",
 *     "version-history" = "/admin/gtfs/agency/{gtfs_agency}/revisions",
 *     "revision" = "/admin/gtfs/agency/{gtfs_agency}/revisions/{gtfs_agency_revision}/view",
 *     "revision_revert" = "/admin/gtfs/agency/{gtfs_agency}/revisions/{gtfs_agency_revision}/revert",
 *     "revision_delete" = "/admin/gtfs/agency/{gtfs_agency}/revisions/{gtfs_agency_revision}/delete",
 *     "translation_revert" = "/admin/gtfs/agency/{gtfs_agency}/revisions/{gtfs_agency_revision}/revert/{langcode}",
 *     "collection" = "/admin/gtfs/agency",
 *   },
 *   field_ui_base_route = "gtfs_agency.settings"
 * )
 */
class Agency extends GTFSEntityBase {
  use HasIdTrait;

  /**
   * {@inheritdoc}
   */
  public static $resourceURL = '/gtfs/api/v1/agencies';

  /**
   * {@inheritdoc}
   */
  public static function schema(): array {
    return [
      'fields' => [
        'agency_id' => [
          'type' => 'varchar',
          'length' => 128,
          'default' => '',
        ],
        'agency_name' => [
          'type' => 'varchar',
          'length' => 255,
          'default' => '',
        ],
        'agency_url' => [
          'type' => 'varchar',
          'length' => 255,
          'default' => '',
        ],
        'agency_timezone' => [
          'type' => 'varchar',
          'length' => 255,
          'default' => '',
        ],
        'agency_lang' => [
          'type' => 'varchar',
          'length' => 255,
          'default' => '',
        ],
        'agency_phone' => [
          'type' => 'varchar',
          'length' => 255,
          'default' => '',
        ],
        'agency_fare_url' => [
          'type' => 'varchar',
          'length' => 255,
          'default' => '',
        ],
        'agency_email' => [
          'type' => 'varchar',
          'length' => 255,
          'default' => '',
        ],
      ],
      'primary key' => ['agency_id'],
    ];
  }

  /**
   * Gets all route ids associated with the agency.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *  A query for routes matching this agency.
   */
  public function routeIds(): QueryInterface {
    return \Drupal::entityQuery('gtfs_route')
      ->condition('agency_id', $this->id())
      ->condition(
        'feed_reference__target_id',
        $this->get('feed_reference')->target_id
      );
  }

  public function routes() {
    return Route::loadMultiple($this->routeIds()->execute());
  }

  /**
   * {@inheritdoc}
   */
  public function links(): array {
    $links = [
      'routes' => $this->resourceUrl('/routes'),
      'fare_attributes' => $this->resourceUrl('/fareAttributes'),
    ];
    return array_merge(parent::links(), $links);
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('agency_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('agency_name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['agency_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Agency ID'))
      ->setDescription(t('The agency_id field is an ID that uniquely identifies a transit agency. A transit feed may represent data from more than one agency. The agency_id is dataset unique. This field is optional for transit feeds that only contain data for a single agency.'))
    // This is how we map to the feed.
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['agency_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The agency_name field contains the full name of the transit agency.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['agency_url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Agency URL'))
      ->setDescription(t('The agency_url field contains the URL of the transit agency. The value must be a fully qualified URL that includes https:// or https://, and any special characters in the URL must be correctly escaped. See http://www.w3.org/Addressing/URL/4_URI_Recommentations.html for a description of how to create fully qualified URL values.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['agency_timezone'] = BaseFieldDefinition::create('tzfield')
      ->setLabel(t('Agency Timezone'))
      ->setDescription(t('The agency_timezone field contains the timezone where the transit agency is located. Timezone names never contain the space character but may contain an underscore. Please refer to https://en.wikipedia.org/wiki/List_of_tz_zones for a list of valid values. If multiple agencies are specified in the feed, each must have the same agency_timezone.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['agency_lang'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Agency Language'))
      ->setDescription(t('The agency_lang field contains a IETF BCP 47 language code specifying the primary language used by this transit agency. This setting helps GTFS consumers choose capitalization rules and other language-specific settings for the feed. For an introduction to IETF BCP 47, please refer to https://www.rfc-editor.org/rfc/bcp/bcp47.txt and http://www.w3.org/International/articles/language-tags/.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'language_select',
        'weight' => -3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ])
      ->setDefaultValue(LanguageInterface::LANGCODE_NOT_SPECIFIED)
      ->setInitialValue(LanguageInterface::LANGCODE_NOT_SPECIFIED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['agency_phone'] = BaseFieldDefinition::create('telephone')
      ->setLabel(t('Agency Phone'))
      ->setDescription(t('The agency_phone field contains a single voice telephone number for the specified agency. This field is a string value that presents the telephone number as typical for the agency\'s service area. It can and should contain punctuation marks to group the digits of the number. Dialable text (for example, TriMet\'s "503-238-RIDE") is permitted, but the field must not contain any other descriptive text.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['agency_fare_url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Agency Fare URL'))
      ->setDescription(t('The agency_fare_url specifies the URL of a web page that allows a rider to purchase tickets or other fare instruments for that agency online. The value must be a fully qualified URL that includes https:// or https://, and any special characters in the URL must be correctly escaped. See http://www.w3.org/Addressing/URL/4_URI_Recommentations.html for a description of how to create fully qualified URL values.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['agency_email'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Agency Email'))
      ->setDescription(t('Contains a single valid email address actively monitored by the agency’s customer service department. This email address will be considered a direct contact point where transit riders can reach a customer service representative at the agency.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    return $fields;
  }

}
