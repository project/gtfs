<?php

namespace Drupal\gtfs\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\gtfs\GTFSEntityStorageInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\file\Entity\File;

/**
 * This class defines generic functions as required by
 * the interface, except for those containing crucial
 * data like getGTFSFields. These functions should be
 * adequate for basic GTFS objects like Agency but will
 * be overwritten for more complex ones like StopTimes.
 * Inheriting objects may use these parent functions as
 * a starting point.
 */
abstract class GTFSEntityBase extends RevisionableContentEntityBase implements GTFSObjectInterface {

  use EntityChangedTrait;

  /**
   * The REST URL at which this entity may be found.
   *
   * @var string
   */
  public static $resourceURL = '/gtfs/api/v1';

  /**
   * The Drupal fields we don't want output.
   *
   * @var array
   */
  public static $fields_to_hide = [
    'id',
    'uuid',
    'vid',
    'hash',
    'index_hash',
    'created',
    'changed',
    'langcode',
    'revision_created',
    'revision_user',
    'revision_log_message',
    'default_langcode',
    'revision_default',
    'user_id',
    'status',
    'revision_translation_affected',
    'feed_reference',
  ];

  public static function deleteByIds($ids, &$context = []) {
    foreach ($ids as $id) {
      static::deleteById($id);
    }
    $context['message'] = t('Deleted ' . count($ids) . ' ' . static::class . ' objects');
  }

  public static function deleteById($id) {
    $entity = static::load($id);
    if ($entity) {
      $entity->delete();
    }
  }

  /**
   * Returns the entity as an associative array.
   *
   * This function gets all the properties of the entity except the ones
   * specified in $fields_to_hide and returns the current entity as an
   * associative array.
   *
   * @return array
   */
  public function toGTFSObject(): array {
    $fields = array_diff(array_keys(static::getFields()), static::$fields_to_hide, self::$fields_to_hide);
    $fields = array_map(function ($field) {
      return static::getFieldDefinition($field);
    }, $fields);
    $fields = array_filter($fields, function ($field) {
      return !$field->isComputed();
    });
    $object = array_combine(array_map(function ($field) {
      return $field->getName();
    }, $fields), array_map(function ($field) {
      $name = $field->getName();
      $type = $field->getType();
      switch ($type) {
        case 'entity_reference':
          $target_type = $field->getSetting('target_type');
          $id = $this->get($name)->target_id;
          $value = \Drupal::database()->query(
            "SELECT {$name}
             FROM {{$target_type}_field_data}
             WHERE id = :id",
             [':id' => $id]
          )->fetch(\PDO::FETCH_COLUMN);
          break;
        case 'file':
        case 'image':
          $obj = $this->get($name)->get(0);
          $value = $obj
            ? \Drupal::service('file_url_generator')->generateAbsoluteString(File::load($obj->target_id)->getFileUri())
            : '';
          break;
        default:
          $value = $this->get($name)->value;
          break;
      }
      return $value ? trim($value) : '';
    }, $fields));
    $object['self'] = $this->resourceUrl();
    return $object;
  }

  /**
   * {@inheritdoc}
   */
  public function toGTFSRow(): string {
    return implode(',', array_values($this->toGTFSObject()));
  }

  /**
   * {@inheritdoc}
   */
  public function resourceUrl(string $append = ''): string {
    $url = \Drupal::request()->getSchemeAndHttpHost() . static::$resourceURL;
    $schema = static::schema();
    if (isset($schema['primary key'])) {
      foreach ($schema['primary key'] as $key) {
        $url .= "/{$this->get($key)->value}";
      }
    }
    $url .= $append;
    \Drupal::moduleHandler()->alter('gtfs_entity_resource_url', $url, $this);
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function links(): array {
    $entity_type = \Drupal::service('entity_type.repository')->getEntityTypeFromClass(get_called_class());

    $links = [
      'self' => $this->resourceUrl(),
    ];

    \Drupal::moduleHandler()->alter(["{$entity_type}_links", 'gtfs_entity_links'], $links, $this);

    return $links;
  }

  /**
   * Exposes post import batch operations for alteration.
   *
   * Sometimes after importing a file we may want to do something based
   * on the newly-imported feed, like save all so that fields can be calculated.
   * This function exposes an alter hook and passes the newly-imported feed id.
   *
   * @param $feed_id
   *  The feed of the batch that was just inserted.
   *
   * @return array
   *  The operations to perform.
   */
  public static function postImportBatchOperations($feed_id): array {
    $operations = [];
    $type = \Drupal::service('entity_type.repository')->getEntityTypeFromClass(static::class);
    $type = str_replace('gtfs_', '', $type);
    \Drupal::moduleHandler()->alter("gtfs_post_import_batch_{$type}", $operations, $feed_id);
    return $operations;
  }

  /**
   * BELOW THIS POINT all methods are inherited from RevisionableContentEntityBase
   * or its parents. Some may have GTFS-specific details.
   */

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the agency owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', (bool) $published);
    return $this;
  }

  /**
   * Set up some fields common to all our GTFS entities.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *
   * @return array|\Drupal\Core\Field\FieldDefinitionInterface[]
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the GTFS Feed entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    /**
     * We want to make sure that all entities are linked back to a feed.
     * If, for example multiple feeds contain an agency with the same name,
     * we want to have a way to disambiguate the entity.
     */
    $fields['feed_reference'] = BaseFieldDefinition::create('entity_reference_revisions')
      ->setLabel(t('Parent Feed'))
      ->setDescription(t('The dataset to which the entity is attached.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'target_type' => 'gtfs_feed',
        'target_bundle' => 'gtfs_feed',
        'default_value' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_revisions_autocomplete"',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    /**
     * This field is the hash of the imported data, so we know how to
     * determine revisions if a feed is imported multiple times.
     * Feeds should be imported multiple times because the same URL will
     * contain new data when the GTFS information changes.
     */
    $fields['hash'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Hash of import data'))
      ->setRevisionable(TRUE);

    /**
     * The "index hash" is a way to determine an entity across revisions.
     * It is a hash of the feed reference id as well as any unique ids
     * on the entity itself, e.g. for Agencies the agency_id field. This should
     * not change across revisions of a feed and therefore we can track entities
     * across revisions this way, QED.
     */
    $fields['index_hash'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Hash of unique indexes'))
      ->setRevisionable(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the entity is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 4,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * Returns storage for given entity. Utility function.
   *
   * @return \Drupal\gtfs\GTFSEntityStorageInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function storage(): GTFSEntityStorageInterface {
    $entity_type = \Drupal::service('entity_type.repository')->getEntityTypeFromClass(get_called_class());
    return \Drupal::entityTypeManager()->getStorage($entity_type);
  }

  /**
   * Shorthand for loading entity by properties.
   *
   * @param array $properties
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function loadByProperties(array $properties) {
    return static::storage()->loadByProperties($properties);
  }

  public function feed(): Feed {
    return Feed::load($this->get('feed_reference')->target_id);
  }

}
