<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the GTFS Feed entity.
 *
 * @see \Drupal\gtfs\Entity\Feed.
 */
class FeedAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\gtfs\Entity\FeedInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished gtfs feed entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published gtfs feed entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit gtfs feed entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete gtfs feed entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add gtfs feed entities');
  }

}
