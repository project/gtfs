<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of GTFS Route entities.
 *
 * @ingroup gtfs
 */
class RouteListBuilder extends GTFSObjectListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['route_id'] = $this->t('ID');
    $header['route_short_name'] = $this->t('Short Name');
    $header['route_long_name'] = $this->t('Long Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\gtfs\Entity\Route $entity */
    $row['id'] = Link::createFromRoute(
      $entity->get('route_id')->value,
      'entity.gtfs_route.canonical',
      ['gtfs_route' => $entity->id()]
    );
    $row['route_short_name'] = $entity->get('route_short_name')->value;
    $row['route_long_name'] = $entity->get('route_long_name')->value;
    return $row + parent::buildRow($entity);
  }

}
