<?php

namespace Drupal\gtfs\Form;

use Drupal\gtfs\Entity\Feed\Importer;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the form which allows for deleting of all instances of an entity.
 */
class EntityDeleteForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gtfs_entity_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $file_entity_map = Importer::getFileEntityMap();
    $header = [
      'class' => ['data' => $this->t('Entity Type')],
      'count' => ['data' => $this->t('Number of Entities')],
    ];
    $options = [];
    foreach ($file_entity_map as $class) {
      $entity_type = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class);
      $number = \Drupal::database()->query(
        "SELECT count(*) FROM {{$entity_type}}"
      )->fetch(\PDO::FETCH_COLUMN);
      if ($number >= 0) {
        $parts = explode("\\", $class);
        $options[$class] = [
          'class' => end($parts),
          'count' => $number,
        ];
      }
    }
    $form['description'] = [
      '#markup' => $this->t('Select the entities to delete'),
    ];
    $form['entities_to_delete'] = [
      '#title' => $this->t('Entities to delete.'),
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#default_value' => array_keys($options),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entities = array_values(array_filter($form_state->getValue('entities_to_delete'), function ($key, $value) {
          return $key === $value;
    }, ARRAY_FILTER_USE_BOTH));
    $database = \Drupal::database();
    $schema = $database->schema();
    foreach ($entities as $entity) {
      $entity_type = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($entity);
      $database->query("TRUNCATE {$entity_type}");
      $database->query("TRUNCATE {$entity_type}_revision");
      $database->query("TRUNCATE {$entity_type}_field_data");
      $database->query("TRUNCATE {$entity_type}_field_revision");
      $schema->dropTable("{$entity_type}_temp");
    }
    \Drupal::messenger()->addMessage($this->t('Selected entity tables have been truncated.'));
  }

}
