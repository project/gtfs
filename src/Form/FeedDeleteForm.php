<?php

namespace Drupal\gtfs\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for deleting GTFS Feed entities.
 *
 * @ingroup gtfs
 */
class FeedDeleteForm extends ContentEntityDeleteForm {
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['delete_feed'] = [
      '#type' => 'checkbox',
      '#title' => 'Delete this feed',
      '#default_value' => FALSE,
    ];
    $form['delete_attached'] = [
      '#type' => 'checkbox',
      '#title' => 'Delete all attached entities',
      '#default_value' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $delete_attached = $form_state->getValue('delete_attached');
    $delete_feed = $form_state->getValue('delete_feed');
    /** @var \Drupal\gtfs\Entity\Feed $entity */
    $feed = $this->getEntity();
    if ($delete_feed) {
      $message = sprintf('The feed %s has been deleted', $feed->getName());
      if ($delete_attached) {
        $message .= ', along with all attached entities';
      }
    } else if ($delete_attached) {
      $message = sprintf('All entities attached to %s have been deleted', $feed->getName());
    } else {
      $message = 'Nothing deleted';
    }

    if ($delete_attached) {
      $batch = $feed->batchDeleteAll($delete_feed);
      batch_set($batch);
    } else if ($delete_feed) {
      $feed->delete();
    }
    $form_state->setRedirectUrl($this->getRedirectUrl());

    $this->messenger()->addStatus($message);
    $this->logDeletionMessage();
  }

}
