<?php

namespace Drupal\gtfs\Form;

use Drupal\Core\Url;
use Drupal\gtfs\Entity\Feed;
use Drupal\gtfs\Entity\Feed\Downloader;
use Drupal\gtfs\Entity\Feed\Importer;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 */
class FeedImportForm extends ContentEntityForm {

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\gtfs\Entity\Feed|null $gtfs_feed
   * @param string $step
   * @param string $class
   *
   * @return array|mixed
   */
  public function buildForm(array $form, FormStateInterface $form_state, Feed $gtfs_feed = NULL, string $step = 'begin', string $class = 'undefined') {
    if (!$form_state->has('entity_form_initialized')) {
      $this->init($form_state);
    }

    $form_state->set('step', $step);

    $form += call_user_func_array(
      [$this, "{$step}Step"],
      [&$form, $form_state, $gtfs_feed, $class]
    );

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\gtfs\Entity\Feed|null $feed
   *
   * @return array
   */
  private function beginStep(array &$form, FormStateInterface $form_state, Feed $feed = NULL) {
    $form['description'] = [
      '#markup' => $this->t('This step will download @filename to a temporary folder. The next step will prompt you to choose which entities you wish to import.', ['@filename' => $feed->get('feed_url')->value]),
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['next'] = [
      '#type' => 'submit',
      '#value' => $this->t('Download'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\gtfs\Entity\Feed|null $feed
   *
   * @return array
   */
  private function chooseEntitiesStep(array &$form, FormStateInterface $form_state, Feed $feed = NULL) {
    $downloader = new Downloader($feed);
    $options = [];
    foreach (Importer::getImportOrder() as $filename) {
      $filepath = "{$downloader->getExtractedFileDirectory()}/{$filename}.txt";
      if (file_exists($filepath)) {
        $options[$filename] = [
          'filename' => "{$filename}.txt",
          'count' => Importer::countRecordsInFile($filepath)
        ];
      }
    }
    $header = [
      'filename' => ['data' => $this->t('Filename')],
      'count' => ['data' => $this->t('Approximate Record Count')],
    ];
    $form['entities_to_import'] = [
      '#title' => $this->t('Files to import'),
      '#type' => 'tableselect',
      '#header' => $header,
      '#description' => $this->t('Entry counts are approximate'),
      '#options' => $options,
      '#default_value' => array_keys($options),
    ];
    $form['full_import'] = [
      '#type' => 'checkbox',
      '#title' => 'Full Import',
      '#description' => 'If checked, this import will import all GTFS fles as entities managed by Drupal. Otherwise only the basic version of the files will be available',
      '#default_value' => TRUE
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['next'] = [
      '#type' => 'submit',
      '#value' => $this->t('Prepare'),
      '#button_type' => 'primary',
    ];
    return $form;

  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\gtfs\Entity\Feed|null $feed
   * @param string $classname
   *
   * @return array
   */
  public function transformStep(array &$form, FormStateInterface $form_state, Feed $feed = NULL, string $classname = '') {
    if (!$classname) return $form;
    $class = base64_decode($classname);
    $temp_table_name = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class) . '_temp';
    $form['description'] = [
      '#markup' => $this->t('Press next to insert entities from @tablename into database', ['@tablename' => $temp_table_name]),
    ];
    $form['entity'] = [
      '#type' => 'hidden',
      '#value' => $class,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['next'] = [
      '#type' => 'submit',
      '#value' => $this->t('Insert'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\gtfs\Entity\Feed|null $feed
   *
   * @return array
   */
  public function cleanupStep(array &$form, FormStateInterface $form_state, Feed $feed = NULL) {
    $form['description'] = [
      '#markup' => $this->t('Press finish to cleanup files and temporary tables created during the import process'),
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['next'] = [
      '#type' => 'submit',
      '#value' => $this->t('Finish'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $step = $form_state->get('step');
    $feed = $this->getEntity();
    $actions = [
      'begin' => function ($form, $form_state) use ($feed) {
        $downloader = new Downloader($feed);
        $batch = $downloader->batch();
        batch_set($batch);
        $url = Url::fromRoute('entity.gtfs_feed.import_form', [
          'gtfs_feed' => $feed->id(),
          'step' => 'chooseEntities',
        ]);
        return $url;
      },
      'chooseEntities' => function ($form, $form_state) use ($feed) {
        $full_import = $form_state->getValue('full_import');
        $entities = array_values(array_filter($form_state->getValue('entities_to_import'), function ($key, $value) {
          return $key === $value;
        }, ARRAY_FILTER_USE_BOTH));
        $importer = $feed->newImport($entities, (bool) $full_import);
        $batch = $importer->batchPrepare();
        $next_url = $batch['next_url'];
        unset($batch['next_url']);
        batch_set($batch);
        return $next_url;
      },
      'transform' => function ($form, $form_state) use ($feed) {
        $class = $form_state->getValue('entity');
        $importer = $feed->getCurrentImport();
        $batch = $importer->batchTransform($class);
        $next_url = $batch['next_url'];
        unset($batch['next_url']);
        batch_set($batch);
        return $next_url;
      },
      'cleanup' => function ($form, $form_state) use ($feed) {
        $batch = $feed->getCurrentImport()->batchCleanup();
        batch_set($batch);
        return Url::fromRoute('entity.gtfs_feed.canonical', [
          'gtfs_feed' => $feed->id(),
        ]);
      },
      'default' => function () use ($feed) {
        return Url::fromRoute('entity.gtfs_feed.import_form', [
          'gtfs_feed' => $feed->id(),
          'step' => 'error',
        ]);
      },
    ];
    if (!isset($actions[$step])) {
      $step = 'default';
    }
    $url = $actions[$step]($form, $form_state);
    $form_state->setRedirectUrl($url);
  }

}
