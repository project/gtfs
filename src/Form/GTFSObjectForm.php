<?php

namespace Drupal\gtfs\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for GTFS object forms.
 *
 * @ingroup gtfs
 */
class GTFSObjectForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\gtfs\Entity\GTFSObjectInterface $entity */
    $form = parent::buildForm($form, $form_state);

    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $type = $entity->getEntityType();

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime(\Drupal::time()->getRequestTime());
      $entity->setRevisionUserId(\Drupal::currentUser()->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        \Drupal::messenger()->addMessage($this->t("Created the %label %type.", [
          '%label' => $entity->label(),
          '%type' => $type->getLabel()
        ]));
        break;

      default:
        \Drupal::messenger()->addMessage($this->t('Saved the %label %type.', [
          '%label' => $entity->label(),
          '%type' => $type->getLabel()
        ]));
    }
    $form_state->setRedirect("entity.{$type->id()}.canonical", ["{$type->id()}" => $entity->id()]);
  }

}
