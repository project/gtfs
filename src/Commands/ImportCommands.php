<?php

namespace Drupal\gtfs\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\gtfs\Entity\Feed;
use Drupal\gtfs\Entity\Feed\Importer;
use Drush\Commands\DrushCommands;

class ImportCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeRepositoryInterface
   */
  private $entityTypeRepository;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * @var \Drupal\gtfs\Entity\Feed\Importer
   */
  private $importer;

  /**
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\Core\Entity\EntityTypeRepositoryInterface $entityTypeRepository
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityTypeRepositoryInterface $entityTypeRepository, FileSystemInterface $fileSystem, LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeRepository = $entityTypeRepository;
    $this->fileSystem = $fileSystem;
    $this->loggerChannelFactory = $loggerChannelFactory;
  }

  /**
   * Import entities from GTFS feed
   *
   * @param int $feedId
   *   ID of feed to import
   *   Argument provided to the drush command.
   *
   * @command gtfs:feed:import
   *
   * @option entities
   *   Specify a comma-separated list of entities to import. Defaults to all.
   *
   * @usage gtfs:feed:import 1 --entities=agency
   *   1 is the feed to import the entities of
   */
  public function importFeed($feedId, $options = ['entities' => 'all']) {
    $this->logger()->notice("Beginning to import feed {$feedId}");
    $feed = Feed::load($feedId);
    $downloader = new Feed\Downloader($feed);
    $this->logger()->notice("Downloading feed");
    $batch = $downloader->batch();
    batch_set($batch);
    drush_backend_batch_process();
    $choices = [];
    \Drupal::database()->query('SET wait_timeout = 60000;')->execute();
    foreach (Importer::getImportOrder() as $filename) {
      $filepath = "{$downloader->getExtractedFileDirectory()}/{$filename}.txt";
      if (file_exists($filepath)) {
        $choices[] = $filename;
      }
    }
    $entities = $this->input()->getOption('entities');
    $entities = $entities === 'all' ? $choices : explode(',', $entities);
    $this->importer = $feed->newImport($entities);
    $batch = $this->importer->batchPrepare(FALSE);
    // Split up the batch to avoid timeouts
    foreach ($batch['operations'] as $operation) {
      $minibatch = [
        'operations' => [$operation],
      ];
      batch_set($minibatch);
      drush_backend_batch_process();
    }
    if (isset($batch['finished'])) {
      call_user_func($batch['finished'], true, [], []);
    }
    $file_entity_map = Importer::getFileEntityMap();
    foreach ($entities as $entity) {
      // https://symfony.com/doc/current/console/style.html#progress-bar-methods
      $class = $file_entity_map[$entity];
      $batch = $this->importer->batchTransform($class, FALSE);
      foreach (array_chunk($batch['operations'], 20) as $chunk) {
        $minibatch = [
          'operations' => $chunk,
        ];
        batch_set($minibatch);
        drush_backend_batch_process();
      }
      if (isset($batch['finished'])) {
        call_user_func($batch['finished'], true, [], []);
      }
    }
    $batch = $this->importer->batchCleanup();
    batch_set($batch);
    drush_backend_batch_process();
    $this->logger()->notice('All Batches Finished');
  }

  public static function importFinished() {

  }

}
