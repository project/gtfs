<?php

namespace Drupal\gtfs\Commands;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\gtfs\Entity\Feed;
use Drush\Commands\DrushCommands;

class DeleteCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   */
  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->loggerChannelFactory = $loggerChannelFactory;
  }

  /**
   * Delete GTFS feeds and attached entities
   *
   * @param int $feedId
   *   ID of feed to delete
   *   Argument provided to the drush command.
   *
   * @command gtfs:feed:delete
   *
   * @usage gtfs:feed:delete 1
   *   1 is the feed to delete the entities of
   */
  public function deleteFeed($feedId) {
    $feed = Feed::load($feedId);
    if ($this->input()->getOption('yes')) {
      $deleteSelf = true;
    } else if ($this->input()->getOption('no')) {
      $deleteSelf = false;
    } else {
      $deleteSelf = $this->io()->confirm('Do you want to delete this feed along with all attached entities?', false);
    }
    $batch = $feed->batchDeleteAll($deleteSelf);
    batch_set($batch);
    drush_backend_batch_process();
    _drush_batch_finished();
  }


}