<?php

namespace Drupal\gtfs;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class GTFSPermissions
 *
 * Dynamically generates permissions for GTFS entities.
 *
 * @package Drupal\gtfs
 */
class GTFSPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a GTFSPermissions instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Get permissions for Taxonomy Views Integrator.
   *
   * @return array
   *   Permissions array.
   */
  public function permissions() {
    $permissions = [];

    $types = $this->entityTypeManager->getDefinitions();

    foreach ($types as $type) {
      $accessHandler = $type->getHandlerClass('access');
      $type_id = str_replace('gtfs_', '', $type->id());
      $label = $type->getLabel();

      if ($accessHandler === 'Drupal\gtfs\GTFSObjectAccessControlHandler') {
        $permissions += [
          "add gtfs {$type_id} entities" => [
            'title' => $this->t('Create new %label entities', ['%label' => $label])
          ],
          "administer gtfs {$type_id} entities" => [
            'title' => $this->t('Administer %label entities', ['%label' => $label]),
            'description' => $this->t('Allow access to administration for %label entities', ['%label' => $label]),
            'restrict access' => true
          ],
          "delete gtfs {$type_id} entities" => [
            'title' => $this->t('Delete %label entities', ['%label' => $label])
          ],
          "edit gtfs {$type_id} entities" => [
            'title' => $this->t('Edit %label entities', ['%label' => $label])
          ],
          "view published gtfs {$type_id} entities" => [
            'title' => $this->t('View published %label entities', ['%label' => $label])
          ],
          "view unpublished gtfs {$type_id} entities" => [
            'title' => $this->t('View unpublished %label entities', ['%label' => $label])
          ],
          "view all gtfs {$type_id} revisions" => [
            'title' => $this->t('View all %label revisions', ['%label' => $label])
          ],
          "revert all gtfs {$type_id} revisions" => [
            'title' => $this->t('Revert all %label revisions', ['%label' => $label])
          ],
          "delete all gtfs {$type_id} revisions" => [
            'title' => $this->t('Delete all %label revisions', ['%label' => $label])
          ],
        ];
      }
    }

    return $permissions;
  }
}
