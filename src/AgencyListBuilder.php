<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of GTFS Agency entities.
 *
 * @ingroup gtfs
 */
class AgencyListBuilder extends GTFSObjectListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['agency_id'] = $this->t('ID');
    $header['agency_name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['agency_id'] = $entity->get('agency_id')->value;
    $row['agency_name'] = Link::createFromRoute(
      $entity->label(),
      'entity.gtfs_agency.canonical',
      ['gtfs_agency' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
