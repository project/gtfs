<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\gtfs\Entity\GTFSObjectInterface;

/**
 * Defines the storage handler class for GTFS Frequency entities.
 *
 * This extends the base storage class, adding required special handling for
 * GTFS Frequency entities.
 *
 * @ingroup gtfs
 */
class FrequencyStorage extends SqlContentEntityStorage implements GTFSEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(GTFSObjectInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {gtfs_frequency_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {gtfs_frequency_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(GTFSObjectInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {gtfs_frequency_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('gtfs_frequency_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

  public function createWithSampleValues($bundle = FALSE, array $values = []) {
    return $this->create($values); // Lazy way out of fulfilling the interface
  }

}
