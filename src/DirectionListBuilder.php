<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of GTFS Direction entities.
 *
 * @ingroup gtfs
 */
class DirectionListBuilder extends GTFSObjectListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['direction_id'] = $this->t('ID');
    $header['direction'] = $this->t('Direction');
    $header['route'] = $this->t('Route');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\gtfs\Entity\Direction $entity */
    $row['direction_id'] = Link::createFromRoute(
      $entity->label(),
      'entity.gtfs_direction.canonical',
      ['gtfs_direction' => $entity->id()]
    );
    $row['direction'] = $entity->getName();
    $row['route'] = Link::createFromRoute(
      $entity->route()->getName(),
      'entity.gtfs_route.canonical',
      ['gtfs_route' => $entity->route()->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
