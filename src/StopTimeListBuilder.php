<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of GTFS StopTime entities.
 *
 * @ingroup gtfs
 */
class StopTimeListBuilder extends GTFSObjectListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    $header['stop_id'] = $this->t('Stop');
    $header['trip_id'] = $this->t('Trip');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\gtfs\Entity\StopTime $entity */
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.gtfs_stop_time.canonical',
      ['gtfs_stop_time' => $entity->id()]
    );
    $row['stop_id'] = $entity->stop() ? Link::createFromRoute(
      $entity->stop()->label(),
      'entity.gtfs_stop.canonical',
      ['gtfs_stop' => $entity->stop()->id()]
    ) : 'Invalid stop';
    $row['trip_id'] = $entity->trip() ? Link::createFromRoute(
      $entity->trip()->label(),
      'entity.gtfs_trip.canonical',
      ['gtfs_trip' => $entity->stop()->id()]
    ) : 'Invalid Trip';
    return $row + parent::buildRow($entity);
  }

}
