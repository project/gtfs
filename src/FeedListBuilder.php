<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of GTFS Feed entities.
 *
 * @ingroup gtfs
 */
class FeedListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('GTFS Feed ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getOperations(EntityInterface $entity) {
    $operations = parent::getOperations($entity);
    if($entity->get('feed_url')->value) {
      $operations['import'] = [
        'title' => $this->t('Import'),
        'weight' => 20,
        'url' => $entity->toUrl('import-form'),
      ];
    }
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\gtfs\Entity\Feed $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.gtfs_feed.canonical',
      ['gtfs_feed' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
