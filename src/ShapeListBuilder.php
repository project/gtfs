<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of GTFS Shape entities.
 *
 * @ingroup gtfs
 */
class ShapeListBuilder extends GTFSObjectListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['shape_id'] = $this->t('ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\gtfs\Entity\Shape $entity */
    $row['shape_id'] = $entity->get('shape_id')->value;
    $row['name'] = Link::createFromRoute(
      $entity->getName(),
      'entity.gtfs_shape.canonical',
      ['gtfs_shape' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
