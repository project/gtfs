<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for GTFS entities.
 *
 */
class GTFSObjectAccessControlHandler extends EntityAccessControlHandler {


    /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $type = coerceEntityTypeId($entity->getEntityTypeId());

    /** @var \Drupal\gtfs\Entity\GTFSObjectInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, "view unpublished {$type} entities");
        }
        return AccessResult::allowedIfHasPermission($account, "view published {$type} entities");

      case 'update':
        return AccessResult::allowedIfHasPermission($account, "edit {$type} entities");

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, "delete {$type} entities");
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $type = coerceEntityTypeId($context['entity_type_id']);
    if ($type) {
      return AccessResult::allowedIfHasPermission($account, "add {$type} entities");
    }
    return AccessResult::neutral();
  }
}