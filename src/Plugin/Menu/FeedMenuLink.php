<?php


namespace Drupal\gtfs\Plugin\Menu;


use Drupal\Core\Menu\MenuLinkDefault;

/**
 * Represents a menu link for a single Feed.
 */
class FeedMenuLink extends MenuLinkDefault {}