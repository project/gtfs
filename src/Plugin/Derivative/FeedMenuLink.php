<?php


namespace Drupal\gtfs\Plugin\Derivative;


use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derivative class that provides the menu links for the Feeds.
 */
class FeedMenuLink extends DeriverBase implements ContainerDeriverInterface {

  /**
   * @var EntityTypeManagerInterface $entityTypeManager.
   */
  protected $entityTypeManager;

  /**
   * Creates a FeedMenuLink instance.
   *
   * @param $base_plugin_id
   * @param EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    // We assume we don't have too many...
    try {
      $feeds = $this->entityTypeManager->getStorage('gtfs_feed')
        ->loadMultiple();
    } catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
      \Drupal::messenger()->addMessage(t($e->getMessage()), MessengerInterface::TYPE_ERROR);
      $feeds = [];
    }
    foreach ($feeds as $id => $feed) {
      try {
        $links[$id] = [
            'title' => $feed->label(),
            'route_name' => $feed->toUrl()->getRouteName(),
            'route_parameters' => ['gtfs_feed' => $feed->id()],
          ] + $base_plugin_definition;
      } catch (EntityMalformedException $e) {
        \Drupal::messenger()->addMessage(t($e->getMessage()), MessengerInterface::TYPE_ERROR);
      }
    }

    return $links;
  }
}
