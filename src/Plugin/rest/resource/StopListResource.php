<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Stop;

/**
 * Provides GTFS stops as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_stop_list_resource",
 *   label = @Translation("GTFS stops REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/stops"
 *   }
 * )
 */
class StopListResource extends GTFSResourceBase {

  public static $url = '/gtfs/api/{version}/stops';

  public function get($version = 'v1') {

    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}();

    return new ResourceResponse($data, $meta);
  }

  public function source() {
    $meta = [];

    $data = \Drupal::database()
      ->query('
        SELECT *
        FROM {gtfs_stop_source}
        WHERE `feed_reference__target_revision_id` IN (
            SELECT MAX(feed_reference__target_revision_id)
            FROM {gtfs_stop_source}
            GROUP BY `stop_id`
        )')
      ->fetchAll(\PDO::FETCH_ASSOC);

    foreach ($data as &$datum) {
      $datum = static::removeDrupalIds($datum);
    }

    return [$meta, $data];
  }

  /**
   * Responds to route GET requests.
   *
   * @return array
   *   The response.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function v1() {

    $meta = $this->initializeMeta();

    $storage = Stop::storage();

    $query = \Drupal::database()->select('gtfs_stop_field_data', 's')
      ->fields('s', ['id'])
      ->addTag('gtfs_stop_access');

    $meta['totalCount'] = (clone $query)->countQuery()->execute()->fetch(\PDO::FETCH_COLUMN);

    // https://tools.ietf.org/html/rfc7231#section-6.3.5
    if (!$meta['totalCount']) {
      return [$meta, []];
    }

    $results = $query->range($meta['offset'], $meta['limit'])->execute()->fetchAll(\PDO::FETCH_COLUMN);

    // https://tools.ietf.org/html/rfc7231#section-6.3.6
    if (empty($results)) {
      return [$meta, []];
    }

    if (is_string($results)) {
      $results = [$results];
    }

    ResourceMeta::setPagerFromResults($meta, [
      'results' => $results,
      'url' => self::$url,
    ]);

    $data = array_map(function ($stop) {
      return $stop->toGTFSObject();
    }, array_values($storage->loadMultiple($results)));

    return [$meta, $data];
  }

}
