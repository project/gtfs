<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Agency;
use Drupal\gtfs\Entity\Route;

/**
 * Provides GTFS routes as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_route_list_resource",
 *   label = @Translation("GTFS routes REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/agencies/{agency_id}/routes"
 *   }
 * )
 */
class RouteListResource extends GTFSResourceBase {

  public static $url = '/gtfs/api/{version}/agencies/{agency_id}/routes';

  public function get($version = 'v1', $agency_id = NULL) {

    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}($agency_id);

    return new ResourceResponse($data, $meta);
  }

  public function source($agency_id = NULL) {
    $meta = [];

    $data = \Drupal::database()
      ->query('
        SELECT *
        FROM {gtfs_route_source}
        WHERE `agency_id` = :agency_id
        AND `feed_reference__target_revision_id` IN (
            SELECT MAX(feed_reference__target_revision_id)
            FROM {gtfs_route_source}
            GROUP BY route_id
        )
       ', [
         ':agency_id' => $agency_id,
      ])
      ->fetchAll(\PDO::FETCH_ASSOC);

    foreach ($data as &$datum) {
      $datum = static::removeDrupalIds($datum);
    }

    return [$meta, $data];
  }

  /**
   * Responds to route GET requests.
   *
   * @param null $agency_id
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   *   The response.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function v1($agency_id = NULL) {

    $agency = Agency::getById($agency_id);

    $meta = $this->initializeMeta();

    $storage = Route::storage();

    $meta['totalCount'] = (int) $agency->routeIds()->count()->execute();

    // https://tools.ietf.org/html/rfc7231#section-6.3.5
    if (!$meta['totalCount']) {
      return [$meta, []];
    }

    $results = $agency->routeIds()
      ->range($meta['offset'], $meta['limit'])
      ->execute();

    // https://tools.ietf.org/html/rfc7231#section-6.3.6
    if (empty($results)) {
      return [$meta, []];
    }

    if (is_string($results)) {
      $results = [$results];
    }

    ResourceMeta::setPagerFromResults($meta, [
      'results' => $results,
      'url' => str_replace(['{version}', '{agency_id}'], ['v1', $agency_id], self::$url),
    ]);

    $data = array_map(function ($route) {
      return $route->toGTFSObject();
    }, array_values($storage->loadMultiple($results)));

    return [$meta, $data];
  }

}
