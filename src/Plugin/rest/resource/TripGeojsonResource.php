<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Agency;
use Drupal\gtfs\Entity\Route;
use Drupal\gtfs\Entity\Trip;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides GTFS trips GeoJSON as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_trip_geojson_resource",
 *   label = @Translation("GTFS Trip GeoJSON REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/v1/agencies/{agency_id}/routes/{route_id}/trips/{trip_id}/geojson"
 *   }
 * )
 */
class TripGeojsonResource extends GTFSResourceBase {

  public static $invalidRequestMessage = 'No trip was provided';

  public static $notFoundMessage = 'Route with ID @route_id was not found for agency with ID @agency_id';

  /**
   * Responds to trip GET requests.
   *
   * @param string|null $agency_id
   * @param string|null $route_id
   * @param string|null $trip_id
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   * @throws \Exception
   */
  public function get($agency_id = NULL, $route_id = NULL, $trip_id = NULL) {

    if (!$trip_id) {
      throw new BadRequestHttpException(t(static::$invalidRequestMessage));
    }

    $agency = Agency::getById($agency_id);

    $route = Route::getById($agency, $route_id);

    $trip = Trip::getById($route, $trip_id);

    if (!$trip) {
      throw new NotFoundHttpException(t('Trip with ID @trip_id was not found for route with ID @route_id for agency with ID @agency_id', ['@trip_id' => $trip_id, '@route_id' => $route->id(), '@agency_id' => $agency->id()]));
    }

    $meta = $this->initializeMeta();
    $meta['totalCount'] = $meta['dataCount'] = 1;
    $meta['links'] = array_merge($meta['links'], $trip->links());

    $data = $trip->geojson();

    return new ResourceResponse($data, $meta);
  }


}
