<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\rest\ResourceResponse as RESTResponse;

/**
 * Provides discovery for GTFS resources
 *
 * @RestResource(
 *   id = "gtfs_discovery_resource",
 *   label = @Translation("GTFS REST discovery"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/v1"
 *   }
 * )
 */
class DiscoveryResource extends GTFSResourceBase {

  /**
   * Responds to discovery
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   */
  public function get() {

    $resources = \Drupal::getContainer()->get('plugin.manager.rest')->getDefinitions();

    $resources = array_filter($resources, function ($resource) {
      return strpos($resource['provider'], 'gtfs') !== false && isset($resource['uri_paths']['canonical']);
    });

    $resources = array_map(function ($resource) {
      return [
        'link' => \Drupal::request()->getSchemeAndHttpHost() . $resource['uri_paths']['canonical'],
        'label' => $resource['label']->render()
      ];
    }, $resources);

    usort($resources, function ($a, $b) {
      return strcmp($a['link'], $b['link']);
    });

    return new RESTResponse($resources);
  }

}
