<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Service;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides GTFS services as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_service_resource",
 *   label = @Translation("GTFS service REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/services/{service_id}"
 *   }
 * )
 */
class ServiceResource extends GTFSResourceBase {

  public static $invalidRequestMessage = 'No service was provided';

  public static $notFoundMessage = 'Service with ID @service_id was not found';

  public function get($version = 'v1', $service_id = NULL) {

    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}($service_id);

    return new ResourceResponse($data, $meta);
  }

  public function source($service_id = NULL) {
    $meta = [];

    $data = Service::getFromSource($service_id);

    return [$meta, $data];
  }

  /**
   * Responds to service GET requests.
   *
   * @param string|null $service_id
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   * @throws \Exception
   */
  public function v1($service_id = NULL) {

    if (!$service_id) {
      throw new BadRequestHttpException(t(static::$invalidRequestMessage));
    }

    $service = Service::getById($service_id);

    if (!$service) {
      throw new NotFoundHttpException(t(static::$notFoundMessage, ['@service_id' => $service_id]));
    }

    $meta = $this->initializeMeta();
    $meta['totalCount'] = $meta['dataCount'] = 1;

    $data = $service->toGTFSObject();

    return [$meta, $data];

  }

}
