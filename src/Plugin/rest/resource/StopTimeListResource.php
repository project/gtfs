<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Agency;
use Drupal\gtfs\Entity\Route;
use Drupal\gtfs\Entity\StopTime;
use Drupal\gtfs\Entity\Trip;

/**
 * Provides GTFS stop times as a rest resource
 *
 * @RestResource(
 *   id = "gtfs_stop_time_list_resource",
 *   label = @Translation("GTFS trip stop times REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/agencies/{agency_id}/routes/{route_id}/trips/{trip_id}/stopTimes"
 *   }
 * )
 */
class StopTimeListResource extends GTFSResourceBase {

  public static $url = '/gtfs/api/{version}/agencies/{agency_id}/routes/{route_id}/trips/{trip_id}/stopTimes';

  public function get($version = 'v1', $agency_id = NULL, $route_id = NULL, $trip_id = NULL) {

    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}($agency_id, $route_id, $trip_id);

    return new ResourceResponse($data, $meta);
  }

  public function source($agency_id = NULL, $route_id = NULL, $trip_id = NULL) {
    $meta = [];

    $data = \Drupal::database()
      ->query('
        SELECT *
        FROM {gtfs_stop_time_source}
        WHERE `trip_id` = :trip_id
        AND `route_id` IN (
            SELECT `route_id`
            FROM {gtfs_route_source}
            WHERE `agency_id` = :agency_id
            AND `route_id` = :route_id
            AND `feed_reference__target_revision_id` IN (
                SELECT MAX(feed_reference__target_revision_id)
                FROM {gtfs_route_source}
                GROUP BY `route_id`
            )
        )
       ', [
        ':agency_id' => $agency_id,
        ':route_id' => $route_id,
        ':trip_id' => $trip_id,
      ])
      ->fetchAll(\PDO::FETCH_ASSOC);

    foreach ($data as &$datum) {
      $datum = static::removeDrupalIds($datum);
    }

    return [$meta, $data];
  }

  /**
   * Responds to stop time GET requests.
   *
   * @param null $agency_id
   * @param null $route_id
   * @param null $trip_id
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function v1($agency_id = NULL, $route_id = NULL, $trip_id = NULL) {

    $agency = Agency::getById($agency_id);

    $route = Route::getById($agency, $route_id);

    $trip = Trip::getById($route, $trip_id);

    $meta = $this->initializeMeta();

    $storage = StopTime::storage();

    $query = \Drupal::entityQuery('gtfs_stop_time')
      ->condition('trip_id', $trip->id())
      ->condition('feed_reference__target_id', $agency->get('feed_reference')->target_id);

    $meta['totalCount'] = (int) (clone $query)->count()->execute();

    // https://tools.ietf.org/html/rfc7231#section-6.3.5
    if(!$meta['totalCount']) {
      return [$meta, []];
    }

    $results = $query->range($meta['offset'], $meta['limit'])->execute();

    // https://tools.ietf.org/html/rfc7231#section-6.3.6
    if(empty($results)) {
      return [$meta, []];
    }

    if(is_string($results)) $results = [$results];

    ResourceMeta::setPagerFromResults($meta, [
      'results' => $results,
      'url' => str_replace(
        ['{version}', '{agency_id}', '{route_id}', '{trip_id}'],
        ['v1', $agency_id, $route_id, $trip_id],
        self::$url
      ),
    ]);

    $data = array_map(function ($stopTime) {
      return $stopTime->toGTFSObject();
    }, array_values($storage->loadMultiple($results)));

    return [$meta, $data];
  }
}
