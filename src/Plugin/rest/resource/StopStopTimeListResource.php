<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Stop;
use Drupal\gtfs\Entity\StopTime;

/**
 * Provides GTFS stoptime per stop as as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_stop_stop_time_list_resource",
 *   label = @Translation("GTFS stop stop times REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/v1/stops/{stop_id}/stopTimes"
 *   }
 * )
 */
class StopStopTimeListResource extends GTFSResourceBase {

  public static $url = '/gtfs/api/v1/stops/{stop_id}/stopTimes';

  /**
   * Responds to stop GET requests.
   *
   * @param null $stop_id
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function get($stop_id = NULL) {

    $stop = Stop::getById($stop_id);

    $meta = $this->initializeMeta();

    $storage = StopTime::storage();

    $query = \Drupal::entityQuery('gtfs_stop_time')
      ->condition('stop_id', $stop->id(), '=');

    $meta['totalCount'] = (int) (clone $query)->count()->execute();
    // Return new ResourceResponse($meta['totalCount'], $meta);
    // https://tools.ietf.org/html/rfc7231#section-6.3.5
    if (!$meta['totalCount']) {
      return new ResourceResponse([], $meta, 204);
    }

    $results = $query->range($meta['offset'], $meta['limit'])->execute();

    // https://tools.ietf.org/html/rfc7231#section-6.3.6
    if (empty($results)) {
      return new ResourceResponse([], $meta, 205);
    }

    if (is_string($results)) {
      $results = [$results];
    }

    ResourceMeta::setPagerFromResults($meta, [
      'results' => $results,
      'url' => str_replace(
        ['{version}', '{stop_id}'],
        ['v1', $stop_id],
        self::$url
      ),
    ]);

    $data = array_map(function ($stop_time) {
      return $stop_time->toGTFSObject();
    }, array_values($storage->loadMultiple($results)));

    return new ResourceResponse($data, $meta);
  }

}
