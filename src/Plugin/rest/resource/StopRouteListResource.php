<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Route;
use Drupal\gtfs\Entity\Stop;

/**
 * Provides GTFS routes per stop as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_stop_route_list_resource",
 *   label = @Translation("GTFS stop routes REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/stops/{stop_id}/routes"
 *   }
 * )
 */
class StopRouteListResource extends GTFSResourceBase {

  public static $url = '/gtfs/api/{version}/stops/{stop_id}/routes';

  public function get($version = 'v1', $stop_id = NULL) {

    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}($stop_id);

    return new ResourceResponse($data, $meta);
  }

  public function source($stop_id = NULL) {
    $meta = [];

    $data = \Drupal::database()
      ->query('
        SELECT *
        FROM {gtfs_route_source}
        WHERE `route_id` IN (
            SELECT `route_id`
            FROM {gtfs_trip_source}
            WHERE `trip_id` IN (
                SELECT `trip_id`
                FROM {gtfs_stop_time_source}
                WHERE `stop_id` = :stop_id
            )
            AND `feed_reference__target_revision_id` IN (
              SELECT MAX(feed_reference__target_revision_id)
              FROM {gtfs_trip_source}
              GROUP BY trip_id
            )
        )
        AND `feed_reference__target_revision_id` IN (
            SELECT MAX(feed_reference__target_revision_id)
            FROM {gtfs_route_source}
            GROUP BY route_id
        )
       ', [
        ':stop_id' => $stop_id,
      ])
      ->fetchAll(\PDO::FETCH_ASSOC);

    foreach ($data as &$datum) {
      $datum = static::removeDrupalIds($datum);
    }

    return [$meta, $data];
  }

  /**
   * Responds to stop GET requests.
   *
   * @param null $stop_id
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function v1($stop_id = NULL) {

    $stop = Stop::getById($stop_id);

    $meta = $this->initializeMeta();

    $storage = Route::storage();

    $routeIds = \Drupal::database()->query(
      "SELECT `route_id`
       FROM {gtfs_trip_field_data}
       WHERE `id` IN (
         SELECT `trip_id`
         FROM {gtfs_stop_time_field_data}
         WHERE `stop_id` = :stop_id
       )",
       [':stop_id' => $stop->id()]
     )->fetchAll(\PDO::FETCH_COLUMN);

    if (!count($routeIds)) {
      return [$meta, []];
    }

    $query = \Drupal::entityQuery('gtfs_route')
      ->condition('id', $routeIds, 'IN');

    $meta['totalCount'] = (int) (clone $query)->count()->execute();
    // Return new ResourceResponse($meta['totalCount'], $meta);
    // https://tools.ietf.org/html/rfc7231#section-6.3.5
    if (!$meta['totalCount']) {
      return [$meta, []];
    }

    $results = $query->range($meta['offset'], $meta['limit'])->execute();

    // https://tools.ietf.org/html/rfc7231#section-6.3.6
    if (empty($results)) {
      return [$meta, []];
    }

    if (is_string($results)) {
      $results = [$results];
    }

    ResourceMeta::setPagerFromResults($meta, [
      'results' => $results,
      'url' => str_replace(
        ['{version}', '{stop_id}'],
        ['v1', $stop_id],
        self::$url
      ),
    ]);

    $data = array_map(function ($route) {
      return $route->toGTFSObject();
    }, array_values($storage->loadMultiple($results)));

    return [$meta, $data];
  }

}
