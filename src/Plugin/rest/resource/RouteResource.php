<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Agency;
use Drupal\gtfs\Entity\Route;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a single GTFS route as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_route_resource",
 *   label = @Translation("GTFS route REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/agencies/{agency_id}/routes/{route_id}"
 *   }
 * )
 */
class RouteResource extends GTFSResourceBase {

  public static $invalidRequestMessage = 'No route was provided';

  public static $notFoundMessage = 'Route with ID @route_id was not found for agency with ID @agency_id';

  public function get($version = 'v1', $agency_id = NULL, $route_id = NULL) {

    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}($agency_id, $route_id);

    return new ResourceResponse($data, $meta);
  }

  public function source($agency_id = NULL, $route_id = NULL) {
    $meta = [];

    $data = Route::getFromSource($agency_id, $route_id);

    return [$meta, $data];
  }

  /**
   * Responds to route GET requests.
   *
   * @param string|null $agency_id
   * @param string|null $route_id
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   * @throws \Exception
   */
  public function v1($agency_id = NULL, $route_id = NULL) {

    $agency = Agency::getById($agency_id);

    if (!$route_id) {
      throw new BadRequestHttpException(t(static::$invalidRequestMessage));
    }

    $route = Route::getById($agency, $route_id);

    if (!$route) {
      throw new NotFoundHttpException(t(static::$notFoundMessage, ['@route_id' => $route_id, '@agency_id' => $agency->id()]));
    }

    $meta = $this->initializeMeta();
    $meta['totalCount'] = $meta['dataCount'] = 1;
    $meta['links'] = array_merge($meta['links'], $route->links());

    $data = $route->toGTFSObject();

    return [$meta, $data];
  }

}
