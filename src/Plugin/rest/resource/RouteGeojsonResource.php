<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Agency;
use Drupal\gtfs\Entity\Route;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a single GTFS route geojson as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_route_geojson_resource",
 *   label = @Translation("GTFS route geojson REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/agencies/{agency_id}/routes/{route_id}/geojson"
 *   }
 * )
 */
class RouteGeojsonResource extends GTFSResourceBase {

  public static $invalidRequestMessage = 'No route was provided';

  public static $notFoundMessage = 'Route with ID @route_id was not found for agency with ID @agency_id';

  public function get($version = 'v1', $agency_id = NULL, $route_id = NULL) {

    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}($agency_id, $route_id);

    return new ResourceResponse($data, $meta);
  }

  public function source($agency_id = NULL, $route_id = NULL) {
    $meta = [];



    $shape_ids = \Drupal::database()
      ->query('
        SELECT DISTINCT `shape_id`
        FROM {gtfs_shape_source}
        WHERE `shape_id` IN (
            SELECT `shape_id`
            FROM {gtfs_trip_source}
            WHERE `route_id` IN (
                SELECT `route_id`
                FROM {gtfs_route_source}
                WHERE `route_id` = :route_id
                AND `agency_id` = :agency_id
                AND `feed_reference__target_revision_id` IN (
                  SELECT MAX(feed_reference__target_revision_id)
                  FROM {gtfs_route_source}
                  GROUP BY route_id
                )
            )
            AND `feed_reference__target_revision_id` IN (
                SELECT MAX(feed_reference__target_revision_id)
                FROM {gtfs_trip_source}
                GROUP BY trip_id
            )
        )
        ', [
        ':agency_id' => $agency_id,
        ':route_id' => $route_id,
      ])
      ->fetchAll(\PDO::FETCH_COLUMN);

    $data = [
      'type' => 'MultiLineString',
      'coordinates' => array_map(function ($shape_id) {
        $coordinates = \Drupal::database()->query('
          SELECT `shape_pt_lon`,`shape_pt_lat`
          FROM {gtfs_shape_source}
          WHERE `shape_id` = :shape_id
          AND `feed_reference__target_revision_id` IN (
              SELECT MAX(feed_reference__target_revision_id)
              FROM {gtfs_shape_source}
              GROUP BY shape_id
          )
          ORDER BY `shape_pt_sequence` ASC
        ', [
          ':shape_id' => $shape_id,
        ])->fetchAll(\PDO::FETCH_NUM);
        return array_map(function ($pair) {
          return array_map('floatval', $pair);
        }, $coordinates);
      }, $shape_ids),
    ];

    return [$meta, $data];
  }

  /**
   * Responds to route GET requests.
   *
   * @param string|null $agency_id
   * @param string|null $route_id
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   * @throws \Exception
   */
  public function v1($agency_id = NULL, $route_id = NULL) {

    $agency = Agency::getById($agency_id);

    if (!$route_id) {
      throw new BadRequestHttpException(t(static::$invalidRequestMessage));
    }

    $route = Route::getById($agency, $route_id);

    if (!$route) {
      throw new NotFoundHttpException(t(static::$notFoundMessage, ['@route_id' => $route_id, '@agency_id' => $agency->id()]));
    }

    $meta = $this->initializeMeta();
    $meta['totalCount'] = $meta['dataCount'] = 1;
    $meta['links'] = array_merge($meta['links'], $route->links());

    $data = $route->geojson();

    return [$meta, $data];
  }

}
