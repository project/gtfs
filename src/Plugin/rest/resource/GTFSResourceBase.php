<?php
/**
 * This file contains an abstract class extending
 * the ResourceBase class from Drupal.
 * Its purpose is to make the current request
 * parameters available within the instance,
 * as well as any other utilities that may become
 * useful or necessary.
 */

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;


/**
 * Class GTFSResourceBase
 *
 * Abstract utility class for extending GTFS REST responses. Includes some
 * HATEOAS functionality for paging and self-links.
 *
 * @package Drupal\gtfs\Plugin\rest\resource
 */
abstract class GTFSResourceBase extends ResourceBase {

  const DEFAULT_LIMIT = 500;

  /**
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   The current request
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, Request $current_request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentRequest = $current_request;
  }

  /**
  * {@inheritdoc}
  */
   public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
     return new static(
       $configuration,
       $plugin_id,
       $plugin_definition,
       $container->getParameter('serializer.formats'),
       $container->get('logger.factory')->get('gtfs'),
       $container->get('request_stack')->getCurrentRequest()
     );
   }


  /**
   * Generates the default metadata based on route parameters.
   *
   * @return array
   */
  protected function initializeMeta(): array {
     $offset = (int) $this->currentRequest->get('offset');
     if(!$offset) $offset = 0;

     $limit = (int) $this->currentRequest->get('limit');
     if(!$limit) $limit = self::DEFAULT_LIMIT;

     return ResourceMeta::create([
       'offset' => $offset,
       'limit' => $limit,
       'links' => [
         'next' => false,
         'previous' => false
       ]
     ]);
   }

   public static function removeDrupalIds($datum) {
     unset($datum['feed_reference__target_id']);
     unset($datum['feed_reference__target_revision_id']);
     return $datum;
   }

}
