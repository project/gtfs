<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Direction;
use Drupal\gtfs\Entity\Service;
use Drupal\gtfs\Entity\Stop;

/**
 * Provides GTFS routes per stop as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_stop_direction_list_resource",
 *   label = @Translation("GTFS stop directions REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/stops/{stop_id}/directions"
 *   }
 * )
 */
class StopDirectionResource extends GTFSResourceBase {

  public static $url = '/gtfs/api/{version}/stops/{stop_id}/directions';

  public function get($version = 'v1', $stop_id = NULL) {
    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}($stop_id);

    return new ResourceResponse($data, $meta);
  }

  public function source($stop_id = NULL) {
    $meta = [];

    $data = \Drupal::database()
      ->query('
        SELECT *
        FROM {gtfs_direction_source}
        WHERE `route_id` IN (
            SELECT `route_id`
            FROM {gtfs_trip_source}
            WHERE `trip_id` IN (
                SELECT `trip_id`
                FROM {gtfs_stop_time_source}
                WHERE `stop_id` = :stop_id
                AND `feed_reference__target_revision_id` IN (
                    SELECT MAX(feed_reference__target_revision_id)
                    FROM {gtfs_stop_time_source}
                    GROUP BY stop_id
                )
            )
            AND `feed_reference__target_revision_id` IN (
                SELECT MAX(feed_reference__target_revision_id)
                FROM {gtfs_trip_source}
                GROUP BY trip_id
            )
        )
        AND `feed_reference__target_revision_id` IN (
            SELECT MAX(feed_reference__target_revision_id)
            FROM {gtfs_route_source}
            GROUP BY route_id
        )
      ', [
        ':stop_id' => $stop_id,
      ])
      ->fetchAll(\PDO::FETCH_ASSOC);

    foreach ($data as &$datum) {
      $datum = static::removeDrupalIds($datum);
    }

    return [$meta, $data];
  }

  /**
   * Responds to stop GET requests.
   *
   * @param null $stop_id
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function v1($stop_id = NULL) {

    $stop = Stop::getById($stop_id);

    $meta = $this->initializeMeta();

    $storage = Direction::storage();

    $trips = \Drupal::database()->query(
      'SELECT `service_id`,`route_id`,`direction_id`
       FROM {gtfs_trip_field_data}
       WHERE `id` IN (
         SELECT `trip_id`
         FROM {gtfs_stop_time_field_data}
         WHERE `stop_id` = :stop_id
       )',
       [':stop_id' => $stop->id()]
     )->fetchAll(\PDO::FETCH_ASSOC);

    if (!count($trips)) {
      return [$meta, []];
    }

    $directionIds = array_map(function ($trip) {
      return \Drupal::database()->query(
        'SELECT `id`
        FROM {gtfs_direction_field_data}
        WHERE `route_id` = :route_id
        AND `direction_id` = :direction_id',
        [
          ':route_id' => $trip['route_id'],
          ':direction_id' => $trip['direction_id'],
        ]
      )->fetch(\PDO::FETCH_COLUMN);
    }, $trips);

    $query = \Drupal::entityQuery('gtfs_direction')
      ->condition('id', $directionIds, 'IN');

    $meta['totalCount'] = (int) (clone $query)->count()->execute();
    // Return new ResourceResponse($meta['totalCount'], $meta);
    // https://tools.ietf.org/html/rfc7231#section-6.3.5
    if (!$meta['totalCount']) {
      return [$meta, []];
    }

    $results = $query->range($meta['offset'], $meta['limit'])->execute();

    // https://tools.ietf.org/html/rfc7231#section-6.3.6
    if (empty($results)) {
      return [$meta, []];
    }

    if (is_string($results)) {
      $results = [$results];
    }

    ResourceMeta::setPagerFromResults($meta, [
      'results' => $results,
      'url' => str_replace(
        ['{version}', '{stop_id}'],
        ['v1', $stop_id],
        self::$url
      ),
    ]);

    $results = array_values($storage->loadMultiple($results));

    $data = [];

    foreach ($trips as $trip) {
      $key = implode('|', $trip);
      $direction = false;
      foreach ($results as $result) {
        if ($result->get('route_id')->target_id == $trip['route_id'] && $result->get('direction_id')->value == $trip['direction_id']) {
          $direction = $result;
        }
      }
      if (!$direction) continue;
      $data[$key] = $direction->toGTFSObject();
      $data[$key]['service'] = Service::load($trip['service_id'])->toGTFSObject();
      $data[$key]['route'] = $direction->route()->toGTFSObject();
    }

    $data = array_values($data);

    return [$meta, $data];
  }

}
