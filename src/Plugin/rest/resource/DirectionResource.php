<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Agency;
use Drupal\gtfs\Entity\Route;
use Drupal\gtfs\Entity\Direction;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides GTFS directions as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_direction_resource",
 *   label = @Translation("GTFS direction REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/agencies/{agency_id}/routes/{route_id}/directions/{direction_id}"
 *   }
 * )
 */
class DirectionResource extends GTFSResourceBase {

  public static $invalidRequestMessage = 'No direction was provided';

  public static $notFoundMessage = 'Route with ID @route_id was not found for agency with ID @agency_id';

  public function get($version = 'v1', $agency_id = NULL, $route_id = NULL, $direction_id = NULL) {

    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}($agency_id, $route_id, $direction_id);

    return new ResourceResponse($data, $meta);
  }

  public function source($agency_id = NULL, $route_id = NULL, $direction_id = NULL) {
    $meta = [];

    $data = \Drupal::database()
      ->query('
        SELECT *
        FROM {gtfs_direction_source}
        WHERE `route_id` IN (
            SELECT `route_id`
            FROM {gtfs_route_source}
            WHERE `agency_id` = :agency_id
            AND `route_id` = :route_id
        )
        AND `direction_id` = :direction_id
        AND `feed_reference__target_revision_id` IN (
            SELECT MAX(feed_reference__target_revision_id)
            FROM {gtfs_direction_source}
            GROUP BY `direction_id`
        )
       ', [
         ':agency_id' => $agency_id,
         ':route_id' => $route_id,
         ':direction_id' => $direction_id,
      ])
      ->fetchAll(\PDO::FETCH_ASSOC);

    foreach ($data as &$datum) {
      $datum = static::removeDrupalIds($datum);
    }

    return [$meta, $data];
  }

  /**
   * Responds to direction GET requests.
   *
   * @param string|null $agency_id
   * @param string|null $route_id
   * @param string|null $direction_id
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   * @throws \Exception
   */
  public function v1($agency_id = NULL, $route_id = NULL, $direction_id = NULL) {

    if ($direction_id === NULL) {
      throw new BadRequestHttpException(t(static::$invalidRequestMessage));
    }

    $agency = Agency::getById($agency_id);

    $route = Route::getById($agency, $route_id);

    $direction = Direction::getById($route, $direction_id);

    if (!$direction) {
      throw new NotFoundHttpException(t('Direction with ID @direction_id was not found for route with ID @route_id for agency with ID @agency_id', ['@direction_id' => $direction_id, '@route_id' => $route->id(), '@agency_id' => $agency->id()]));
    }

    $meta = $this->initializeMeta();
    $meta['totalCount'] = $meta['dataCount'] = 1;
    $meta['links'] = array_merge($meta['links'], $direction->links());

    $data = $direction->toGTFSObject();

    return [$data, $meta];
  }


}
