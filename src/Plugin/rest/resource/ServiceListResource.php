<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Service;

/**
 * Provides GTFS services as a rest resource
 *
 * @RestResource(
 *   id = "gtfs_service_list_resource",
 *   label = @Translation("GTFS services REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/services"
 *   }
 * )
 */
class ServiceListResource extends GTFSResourceBase {

  public static $url = '/gtfs/api/{version}/services';

  public function get($version = 'v1') {

    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}();

    return new ResourceResponse($data, $meta);
  }

  public function source() {
    $meta = [];

    $data = \Drupal::database()
      ->query('
        SELECT *
        FROM {gtfs_service_source}
        WHERE `feed_reference__target_revision_id` IN (
            SELECT MAX(feed_reference__target_revision_id)
            FROM {gtfs_service_source}
            GROUP BY `service_id`
        )
       ')
      ->fetchAll(\PDO::FETCH_ASSOC);

    foreach ($data as &$datum) {
      $datum = static::removeDrupalIds($datum);
    }

    return [$meta, $data];
  }

  /**
   * Responds to service GET requests.
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function v1() {

    $meta = $this->initializeMeta();

    $storage = Service::storage();

    $query = \Drupal::entityQuery('gtfs_service');

    $meta['totalCount'] = (int) (clone $query)->count()->execute();

    // https://tools.ietf.org/html/rfc7231#section-6.3.5
    if(!$meta['totalCount']) {
      return [$meta, []];
    }

    $results = $query->range($meta['offset'], $meta['limit'])->execute();

    // https://tools.ietf.org/html/rfc7231#section-6.3.6
    if(empty($results)) {
      return [$meta, []];
    }

    if(is_string($results)) $results = [$results];

    ResourceMeta::setPagerFromResults($meta, [
      'results' => $results,
      'url' => self::$url
    ]);

    $data = array_map(function ($service) {
      return $service->toGTFSObject();
    }, array_values($storage->loadMultiple($results)));

    return [$meta, $data];
  }
}
