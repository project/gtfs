<?php /** @noinspection Annotator */

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Shape;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides GTFS shapes as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_shape_resource",
 *   label = @Translation("GTFS shape REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/v1/shapes/{shape_id}"
 *   }
 * )
 */
class ShapeResource extends GTFSResourceBase {

  public static $invalid_request_message = 'No shape was provided';

  public static $not_found_message = 'Shape with ID @shape_ids was not found';

  /**
   * Responds to shape GET requests.
   *
   * @param string|null $shape_id
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   */
  public function get($shape_id = NULL) {

    $shapes = self::getShapes($shape_id);

    $meta = $this->initializeMeta();
    $meta['totalCount'] = $meta['dataCount'] = count($shapes);
    foreach ($shapes as $shape) {
     $meta['links'] = array_merge($meta['links'], $shape->links());
    }
    $data = array_values(array_map(function ($shape) {
      return $shape->toGTFSObject();
    }, $shapes));

    return new ResourceResponse($data, $meta);
  }

  /**
   * @param string $shape_ids
   *  A comma-separated list of shapes to retrieve.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  public static function getShapes($shape_ids = '') {
    if ($shape_ids == '') {
      throw new BadRequestHttpException(t(static::$invalid_request_message));
    }

    $shape_ids = explode(',', $shape_ids);

    $ids = \Drupal::database()->query(
      "SELECT `id`
       FROM {gtfs_shape_field_data}
       WHERE `shape_id` IN (:shape_ids[])",
      [':shape_ids[]' => $shape_ids]
    )->fetchAll(\PDO::FETCH_COLUMN, 0);

    if (empty($ids)) {
      throw new NotFoundHttpException(t(static::$not_found_message, ['@shape_ids' => $shape_ids]));
    }

    return Shape::loadMultiple($ids);
  }

}
