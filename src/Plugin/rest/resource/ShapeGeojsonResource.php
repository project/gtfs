<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Shape;

/**
 * Provides GTFS shape GeoJSON as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_shape_geojson_resource",
 *   label = @Translation("GTFS Shape GeoJSON REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/v1/shapes/{shape_id}/geojson"
 *   }
 * )
 */
class ShapeGeojsonResource extends GTFSResourceBase {

  public static $invalid_request_message = 'No shape was provided';

  public static $not_found_message = 'Shape with ID @shape_ids was not found';

  /**
   * Responds to shape GET requests.
   *
   * @param string|null $shape_id
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   */
  public function get($shape_id = NULL) {

    $any_shape_with_id = Shape::load(\Drupal::database()->query(
      "SELECT `id`
      FROM {gtfs_shape_field_data}
      WHERE `shape_id` = :shape_id",
      [':shape_id' => $shape_id]
    )->fetch(\PDO::FETCH_COLUMN));

    $meta = $this->initializeMeta();
    $meta['links'] = array_merge($meta['links'], $any_shape_with_id->links());
    $data = $any_shape_with_id->geojson();

    return new ResourceResponse($data, $meta);
  }

}
