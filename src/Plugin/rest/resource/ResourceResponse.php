<?php

namespace Drupal\gtfs\Plugin\rest\resource;

/**
 * {@inheritDoc}
 */
class ResourceResponse extends \Drupal\rest\ResourceResponse {
  /**
   * {@inheritDoc}
   */
  public function __construct($data = NULL, $meta = [], $status = 200, $headers = []) {
    if(empty($meta)) $meta = ResourceMeta::create($meta);
    parent::__construct(['meta' => $meta, 'data' => $data], $status, $headers);
    $this->addCacheableDependency($data);
  }
}
