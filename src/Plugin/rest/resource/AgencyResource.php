<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Agency;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a single GTFS agency as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_agency_resource",
 *   label = @Translation("GTFS agency REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/agencies/{agency_id}"
 *   }
 * )
 */
class AgencyResource extends GTFSResourceBase {

  public static $invalid_request_message = 'No agency was provided';

  public static $not_found_message = 'Agency with ID @agency_id was not found';

  /**
   * Responds to agency GET requests.
   *
   * @param null $agency_id
   *  GTFS ID of the agency to retrieve
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   * @throws \Exception
   */
  public function get($version = 'v1', $agency_id = NULL) {
    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}($agency_id);

    return new ResourceResponse($data, $meta);
  }

  public function source($agency_id = NULL) {
    $meta = [];

    $data = Agency::getFromSource($agency_id);

    return [$meta, $data];
  }

  public function v1($agency_id = NULL) {
    if (!$agency_id) {
      throw new BadRequestHttpException(t(static::$invalid_request_message));
    }

    $agency = Agency::getById($agency_id);

    if (!$agency) {
      throw new NotFoundHttpException(t(static::$not_found_message, ['@agency_id' => $agency_id]));
    }

    $meta = $this->initializeMeta();
    $meta['totalCount'] = $meta['dataCount'] = 1;
    $meta['links'] = array_merge($meta['links'], $agency->links());

    $data = $agency->toGTFSObject();

    return [$meta, $data];
  }


}
