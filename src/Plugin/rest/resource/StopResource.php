<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Stop;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides GTFS stops as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_stop_resource",
 *   label = @Translation("GTFS stop REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/stops/{stop_id}"
 *   }
 * )
 */
class StopResource extends GTFSResourceBase {

  public static $invalidRequestMessage = 'No stop was provided';

  public static $notFoundMessage = 'Stop with ID @stop_id was not found';

  public function get($version = 'v1', $stop_id = NULL) {
    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}($stop_id);

    return new ResourceResponse($data, $meta);
  }

  public function source($stop_id = NULL) {
    $meta = [];

    $data = Stop::getFromSource($stop_id);

    return [$meta, $data];
  }

  /**
   * Responds to stop GET requests.
   *
   * @param string|null $stop_id
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   * @throws \Exception
   */
  public function v1($stop_id = NULL) {
    if (!$stop_id) {
      throw new BadRequestHttpException(t(static::$invalidRequestMessage));
    }

    $stop = Stop::getById($stop_id);

    if (!$stop) {
      throw new NotFoundHttpException(t(static::$notFoundMessage, ['@stop_id' => $stop_id]));
    }

    $meta = $this->initializeMeta();
    $meta['totalCount'] = $meta['dataCount'] = 1;
    $meta['links'] = array_merge($meta['links'], $stop->links());

    $data = $stop->toGTFSObject();

    return [$meta, $data];
  }

}
