<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Agency;

/**
 * Provides GTFS agencies as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_agency_list_resource",
 *   label = @Translation("GTFS agencies REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/agencies"
 *   }
 * )
 */
class AgencyListResource extends GTFSResourceBase {

  /**
   * {@inheritdoc}
   */
  public static $url = '/gtfs/api/{version}/agencies';

  /**
   * Responds to agency GET requests.
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   *   The API response
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function get($version = 'v1') {

    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}();

    return new ResourceResponse($data, $meta);
  }

  public function source() {
    $meta = [];

    $data = \Drupal::database()
      ->query('
        SELECT *
        FROM {gtfs_agency_source}
        WHERE `feed_reference__target_revision_id` IN (
            SELECT MAX(feed_reference__target_revision_id)
            FROM {gtfs_agency_source}
            GROUP BY agency_id
      )')
      ->fetchAll(\PDO::FETCH_ASSOC);

    foreach ($data as &$datum) {
      $datum = static::removeDrupalIds($datum);
    }

    return [$meta, $data];
  }

  public function v1() {
    $meta = $this->initializeMeta();

    $storage = Agency::storage();

    $query = \Drupal::database()->select('gtfs_agency_field_data', 'a')
      ->fields('a', ['id'])
      ->addTag('gtfs_agency_access');

    $meta['totalCount'] = (clone $query)->countQuery()->execute()->fetch(\PDO::FETCH_COLUMN);

    // https://tools.ietf.org/html/rfc7231#section-6.3.5
    if (empty($meta['totalCount'])) {
      return [$meta, []];
    }

    $results = $query->range($meta['offset'], $meta['limit'])->execute()->fetchAll(\PDO::FETCH_COLUMN);

    // https://tools.ietf.org/html/rfc7231#section-6.3.6
    if (empty($results)) {
      return [$meta, []];
    }

    if (is_string($results)) {
      $results = [$results];
    }

    ResourceMeta::setPagerFromResults($meta, [
      'results' => $results,
      'url' => self::$url,
    ]);

    $data = array_map(function ($agency) {
      return $agency->toGTFSObject();
    }, array_values($storage->loadMultiple($results)));

    return [$meta, $data];
  }

}
