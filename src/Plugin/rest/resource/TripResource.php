<?php

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Agency;
use Drupal\gtfs\Entity\Route;
use Drupal\gtfs\Entity\Trip;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides GTFS trips as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_trip_resource",
 *   label = @Translation("GTFS trip REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/agencies/{agency_id}/routes/{route_id}/trips/{trip_id}"
 *   }
 * )
 */
class TripResource extends GTFSResourceBase {

  public static $invalidRequestMessage = 'No trip was provided';

  public static $notFoundMessage = 'Route with ID @route_id was not found for agency with ID @agency_id';

  public function get($version = 'v1', $agency_id = NULL, $route_id = NULL, $trip_id = NULL) {

    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}($agency_id, $route_id, $trip_id);

    return new ResourceResponse($data, $meta);
  }

  public function source($agency_id = NULL, $route_id = NULL, $trip_id = NULL) {
    $meta = [];

    $data = \Drupal::database()
      ->query('
        SELECT *
        FROM {gtfs_trip_source}
        WHERE `trip_id` = :trip_id
        AND `route_id` IN (
            SELECT `route_id`
            FROM {gtfs_route_source}
            WHERE `agency_id` = :agency_id
            AND `route_id` = :route_id
        )
        AND `feed_reference__target_revision_id` IN (
            SELECT MAX(feed_reference__target_revision_id)
            FROM {gtfs_trip_source}
            GROUP BY `trip_id`
        )
       ', [
        ':agency_id' => $agency_id,
        ':route_id' => $route_id,
        ':trip_id' => $trip_id,
      ])
      ->fetch(\PDO::FETCH_ASSOC);

    $data = static::removeDrupalIds($data);

    return [$meta, $data];
  }

  /**
   * Responds to trip GET requests.
   *
   * @param string|null $agency_id
   * @param string|null $route_id
   * @param string|null $trip_id
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   * @throws \Exception
   */
  public function v1($agency_id = NULL, $route_id = NULL, $trip_id = NULL) {

    if (!$trip_id) {
      throw new BadRequestHttpException(t(static::$invalidRequestMessage));
    }

    $agency = Agency::getById($agency_id);

    $route = Route::getById($agency, $route_id);

    $trip = Trip::getById($route, $trip_id);

    if (!$trip) {
      throw new NotFoundHttpException(t('Trip with ID @trip_id was not found for route with ID @route_id for agency with ID @agency_id', ['@trip_id' => $trip_id, '@route_id' => $route->id(), '@agency_id' => $agency->id()]));
    }

    $meta = $this->initializeMeta();
    $meta['totalCount'] = $meta['dataCount'] = 1;
    $meta['links'] = array_merge($meta['links'], $trip->links());

    $data = $trip->toGTFSObject();

    return [$meta, $data];
  }


}
