<?php /** @noinspection Annotator */

namespace Drupal\gtfs\Plugin\rest\resource;

use Drupal\gtfs\Entity\Agency;
use Drupal\gtfs\Entity\Route;
use Drupal\gtfs\Entity\Stop;

/**
 * Provides GTFS stops as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_route_stop_list_resource",
 *   label = @Translation("GTFS route stops REST"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/{version}/agencies/{agency_id}/routes/{route_id}/stops/{direction_ids}"
 *   }
 * )
 */
class RouteStopListResource extends GTFSResourceBase {

  public static $url = '/gtfs/api/{version}/agencies/{agency_id}/routes/{route_id}/stops';

  public function get($version = 'v1', $agency_id = NULL, $route_id = NULL, $direction_ids = null) {

    if (!method_exists($this, $version)) {
      $version = 'v1';
    }

    [$meta, $data] = $this->{$version}($agency_id, $route_id, $direction_ids);

    return new ResourceResponse($data, $meta);
  }

  public function source($agency_id = NULL, $route_id = NULL, $direction_ids = NULL) {
    $meta = [];

    $data = \Drupal::database()
      ->query('
        SELECT *
        FROM {gtfs_stop_source}
        WHERE `stop_id` IN (
            SELECT `stop_id`
            FROM {gtfs_stop_time_source}
            WHERE `trip_id` IN (
                SELECT `trip_id`
                FROM {gtfs_trip_source}
                WHERE `route_id` = (
                    SELECT `route_id`
                    FROM {gtfs_route_source}
                    WHERE `route_id` = :route_id
                    AND `agency_id` = :agency_id
                )
                AND `direction_id` IN (:direction_ids[])
            )
        )
        AND `feed_reference__target_revision_id` IN (
            SELECT MAX(feed_reference__target_revision_id)
            FROM {gtfs_stop_source}
            GROUP BY `stop_id`
        )
       ', [
         ':agency_id' => $agency_id,
         ':route_id' => $route_id,
         ':direction_ids[]' => explode(',', $direction_ids),
      ])
      ->fetchAll(\PDO::FETCH_ASSOC);

    foreach ($data as &$datum) {
      $datum = static::removeDrupalIds($datum);
    }

    return [$meta, $data];
  }

  /**
   * Responds to stop GET requests.
   *
   * @param null $agency_id
   * @param null $route_id
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function v1($agency_id = NULL, $route_id = NULL, $direction_ids = null) {

    $agency = Agency::getById($agency_id);

    $route = Route::getById($agency, $route_id);

    $meta = $this->initializeMeta();

    $storage = Stop::storage();

    $stop_ids = $route->stopIds($direction_ids);

    if (count ($stop_ids) === 0) {
      return [$meta, []];
    }

    $query = \Drupal::entityQuery('gtfs_stop')
      ->condition('id', $stop_ids, 'IN');

    $meta['totalCount'] = (int) (clone $query)->count()->execute();
    // Return new ResourceResponse($meta['totalCount'], $meta);
    // https://tools.ietf.org/html/rfc7231#section-6.3.5
    if (!$meta['totalCount']) {
      return [$meta, []];
    }

    $results = $query->range($meta['offset'], $meta['limit'])->execute();

    // https://tools.ietf.org/html/rfc7231#section-6.3.6
    if (empty($results)) {
      return [$meta, []];
    }

    if (is_string($results)) {
      $results = [$results];
    }

    ResourceMeta::setPagerFromResults($meta, [
      'results' => $results,
      'url' => str_replace(
        ['{version}', '{agency_id}', '{route_id}'],
        ['v1', $agency_id, $route_id],
        self::$url
      ),
    ]);

    $data = array_map(function ($stop) {
      return $stop->toGTFSObject();
    }, array_values($storage->loadMultiple($results)));

    return [$meta, $data];
  }

}
