<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of GTFS Service entities.
 *
 * @ingroup gtfs
 */
class ServiceListBuilder extends GTFSObjectListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['service_id'] = $this->t('ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\gtfs\Entity\Service $entity */
    $row['service_id'] = $entity->get('service_id')->value;
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.gtfs_service.canonical',
      ['gtfs_service' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
