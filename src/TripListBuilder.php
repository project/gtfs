<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of GTFS Trip entities.
 *
 * @ingroup gtfs
 */
class TripListBuilder extends GTFSObjectListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['trip_id'] = $this->t('ID');
    $header['route'] = $this->t('Route');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\gtfs\Entity\Trip $entity */
    $row['trip_id'] = Link::createFromRoute(
      $entity->label(),
      'entity.gtfs_trip.canonical',
      ['gtfs_trip' => $entity->id()]
    );
//    $row['route'] = $entity->route() ? $entity->route()->getName() : 'Error: ' . $entity->get('route_id')->target_id;
    $row['route'] = Link::createFromRoute(
      $entity->route()->getName(),
      'entity.gtfs_route.canonical',
      ['gtfs_route' => $entity->route()->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
