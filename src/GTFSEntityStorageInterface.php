<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\gtfs\Entity\GTFSObjectInterface;

/**
 * Defines the storage handler class for GTFS entities.
 *
 * This extends the base storage class, adding required special handling for
 * GTFS Entity entities.
 *
 * @ingroup gtfs
 */
interface GTFSEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of GTFS Entity revision IDs for a specific GTFS Entity.
   *
   * @param \Drupal\gtfs\Entity\GTFSObjectInterface $entity
   *   The GTFS entity.
   *
   * @return int[]
   *   GTFS Entity revision IDs (in ascending order).
   */
  public function revisionIds(GTFSObjectInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as GTFS Entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   GTFS Entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\gtfs\Entity\GTFSObjectInterface $entity
   *   The GTFS entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(GTFSObjectInterface $entity);

  /**
   * Unsets the language for all GTFS Entity with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
