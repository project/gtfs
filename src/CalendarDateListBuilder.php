<?php

namespace Drupal\gtfs;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of GTFS Calendar Date entities.
 *
 * @ingroup gtfs
 */
class CalendarDateListBuilder extends GTFSObjectListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Date');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\gtfs\Entity\CalendarDate $entity */
    $row['label'] = Link::createFromRoute(
      $entity->getName(),
      'entity.gtfs_calendar_date.canonical',
      ['gtfs_calendar_date' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
