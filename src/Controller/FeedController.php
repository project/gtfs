<?php

namespace Drupal\gtfs\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\gtfs\Entity\Feed\Downloader;
use Drupal\gtfs\Entity\Feed\Importer;
use Drupal\gtfs\Entity\FeedInterface;
use Drupal\gtfs\Entity\Feed;

/**
 * Class FeedController.
 *
 *  Returns responses for GTFS Feed routes.
 */
class FeedController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a GTFS Feed revision.
   *
   * @param int $gtfs_feed_revision
   *   The GTFS Feed revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionShow($gtfs_feed_revision) {
    $feed = $this->entityTypeManager()->getStorage('gtfs_feed')->loadRevision($gtfs_feed_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('gtfs_feed');

    return $view_builder->view($feed);
  }

  /**
   * Page title callback for a GTFS Feed revision.
   *
   * @param int $gtfs_feed_revision
   *   The GTFS Feed revision ID.
   *
   * @return string
   *   The page title.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionPageTitle($gtfs_feed_revision) {
    $feed = $this->entityTypeManager()->getStorage('gtfs_feed')->loadRevision($gtfs_feed_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $feed->label(),
      '%date' => \Drupal::service('date.formatter')
        ->format($feed->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a GTFS Feed.
   *
   * @param \Drupal\gtfs\Entity\FeedInterface $gtfs_feed
   *   A GTFS Feed object.
   *
   * @return array
   *   An array as expected by drupal_render().
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function revisionOverview(FeedInterface $gtfs_feed) {
    $account = $this->currentUser();
    $langcode = $gtfs_feed->language()->getId();
    $langname = $gtfs_feed->language()->getName();
    $languages = $gtfs_feed->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $feed_storage = $this->entityTypeManager()->getStorage('gtfs_feed');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $gtfs_feed->label()]) : $this->t('Revisions for %title', ['%title' => $gtfs_feed->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all gtfs feed revisions") || $account->hasPermission('administer gtfs feed entities')));
    $delete_permission = (($account->hasPermission("delete all gtfs feed revisions") || $account->hasPermission('administer gtfs feed entities')));

    $rows = [];

    $vids = $feed_storage->revisionIds($gtfs_feed);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\gtfs\Entity\FeedInterface $revision */
      $revision = $feed_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $gtfs_feed->getRevisionId()) {
          $link = $this->getLinkGenerator()->generate($date,
            new Url('entity.gtfs_feed.revision', [
              'gtfs_feed' => $gtfs_feed->id(),
              'feed_revision' => $vid,
            ]));
        }
        else {
          $link = $gtfs_feed->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.gtfs_feed.translation_revert', ['gtfs_feed' => $gtfs_feed->id(), 'gtfs_feed_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.gtfs_feed.revision_revert', ['gtfs_feed' => $gtfs_feed->id(), 'gtfs_feed_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.gtfs_feed.revision_delete', ['gtfs_feed' => $gtfs_feed->id(), 'gtfs_feed_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['gtfs_feed_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

  /**
   * Returns a page for QA-ing feeds.
   *
   * @throws \Exception;
   *
   */
  public function qa() {
    $render = [];
    $entities = [];
    // Get all the registered entities
    \Drupal::moduleHandler()->alter('gtfs_import_entities', $entities);
    // Build the header. ID first, then all the entities
    $header = [
      'id' => $this->t('Feed ID'),
    ];
    foreach(array_keys($entities) as $key) {
      $title = ucwords(str_replace('_', ' ', $key));
      $header[$key] = $this->t($title);
    }
    $render['feeds'] = [
      '#type' => 'table',
      '#header' => $header,
    ];
    // Get all the feeds
    $feeds = Feed::loadMultiple();
    /** @var \Drupal\gtfs\Entity\FeedInterface $feed */
    foreach($feeds as $feed) {
      $downloader = new Downloader($feed);
      $context = []; // Fake context object. The downloader usually operates in batch
      $downloader->download($context);
      $downloader->extract($context);
      // Row contains the id...
      $row = [
        'id' => ['#markup' => $feed->id()],
      ];
      // As well as a count of all the entities associated with that feed
      foreach($entities as $slug => $class) {
        $entity_type = \Drupal::service('entity_type.repository')->getEntityTypeFromClass($class);
        $count = \Drupal::database()->query(
          "SELECT count(*)
           FROM {{$entity_type}_field_data}
           WHERE `feed_reference__target_id` = :frti",
          [
            ':frti' => $feed->id(),
          ]
        )->fetch(\PDO::FETCH_COLUMN);
        $count = number_format($count);
        $filepath = "{$downloader->getExtractedFileDirectory()}/{$slug}.txt";
        $expected = file_exists($filepath) ? Importer::countRecordsInFile($filepath) : 'err';
        $row[$slug] = ['#markup' => "{$count}/{$expected}"];
      }
      $render['feeds'][] = $row;
    }
    return $render;
  }

}
