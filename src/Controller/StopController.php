<?php

namespace Drupal\gtfs\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\gtfs\Entity\GTFSObjectInterface;

/**
 * Class StopController.
 *
 *  Returns responses for GTFS Stop routes.
 */
class StopController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a GTFS Stop revision.
   *
   * @param int $gtfs_stop_revision
   *   The GTFS Stop revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionShow($gtfs_stop_revision) {
    $stop = $this->entityTypeManager()->getStorage('gtfs_stop')->loadRevision($gtfs_stop_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('gtfs_stop');

    return $view_builder->view($stop);
  }

  /**
   * Page title callback for a GTFS Stop revision.
   *
   * @param int $gtfs_stop_revision
   *   The GTFS Stop revision ID.
   *
   * @return string
   *   The page title.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionPageTitle($gtfs_stop_revision) {
    $stop = $this->entityTypeManager()->getStorage('gtfs_stop')->loadRevision($gtfs_stop_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $stop->label(),
      '%date' => \Drupal::service('date.formatter')
        ->format($stop->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a GTFS Stop.
   *
   * @param \Drupal\gtfs\Entity\GTFSObjectInterface $gtfs_stop
   *   A GTFS Stop object.
   *
   * @return array
   *   An array as expected by drupal_render().
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function revisionOverview(GTFSObjectInterface $gtfs_stop) {
    $account = $this->currentUser();
    $langcode = $gtfs_stop->language()->getId();
    $langname = $gtfs_stop->language()->getName();
    $languages = $gtfs_stop->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $stop_storage = $this->entityTypeManager()->getStorage('gtfs_stop');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $gtfs_stop->label()]) : $this->t('Revisions for %title', ['%title' => $gtfs_stop->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all gtfs stop revisions") || $account->hasPermission('administer gtfs stop entities')));
    $delete_permission = (($account->hasPermission("delete all gtfs stop revisions") || $account->hasPermission('administer gtfs stop entities')));

    $rows = [];

    $vids = $stop_storage->revisionIds($gtfs_stop);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\gtfs\Entity\GTFSObjectInterface $revision */
      $revision = $stop_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $gtfs_stop->getRevisionId()) {
          $link = $this->getLinkGenerator()->generate($date,
            new Url('entity.gtfs_stop.revision', [
              'gtfs_stop' => $gtfs_stop->id(),
              'gtfs_stop_revision' => $vid
            ]));
        }
        else {
          $link = $gtfs_stop->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.gtfs_stop.translation_revert', ['gtfs_stop' => $gtfs_stop->id(), 'gtfs_stop_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.gtfs_stop.revision_revert', ['gtfs_stop' => $gtfs_stop->id(), 'gtfs_stop_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.gtfs_stop.revision_delete', ['gtfs_stop' => $gtfs_stop->id(), 'gtfs_stop_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['gtfs_stop_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
