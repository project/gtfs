<?php

namespace Drupal\gtfs\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\gtfs\Entity\GTFSObjectInterface;

/**
 * Class ServiceController.
 *
 *  Returns responses for GTFS Service routes.
 */
class ServiceController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a GTFS Service revision.
   *
   * @param int $gtfs_service_revision
   *   The GTFS Service revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionShow($gtfs_service_revision) {
    $service = $this->entityTypeManager()->getStorage('gtfs_service')->loadRevision($gtfs_service_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('gtfs_service');

    return $view_builder->view($service);
  }

  /**
   * Page title callback for a GTFS Service revision.
   *
   * @param int $gtfs_service_revision
   *   The GTFS Service revision ID.
   *
   * @return string
   *   The page title.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionPageTitle($gtfs_service_revision) {
    $service = $this->entityTypeManager()->getStorage('gtfs_service')->loadRevision($gtfs_service_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $service->label(),
      '%date' => \Drupal::service('date.formatter')
        ->format($service->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a GTFS Service.
   *
   * @param \Drupal\gtfs\Entity\GTFSObjectInterface $gtfs_service
   *   A GTFS Service object.
   *
   * @return array
   *   An array as expected by drupal_render().
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function revisionOverview(GTFSObjectInterface $gtfs_service) {
    $account = $this->currentUser();
    $langcode = $gtfs_service->language()->getId();
    $langname = $gtfs_service->language()->getName();
    $languages = $gtfs_service->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $service_storage = $this->entityTypeManager()->getStorage('gtfs_service');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $gtfs_service->label()]) : $this->t('Revisions for %title', ['%title' => $gtfs_service->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all gtfs service revisions") || $account->hasPermission('administer gtfs service entities')));
    $delete_permission = (($account->hasPermission("delete all gtfs service revisions") || $account->hasPermission('administer gtfs service entities')));

    $rows = [];

    $vids = $service_storage->revisionIds($gtfs_service);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\gtfs\Entity\GTFSObjectInterface $revision */
      $revision = $service_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $gtfs_service->getRevisionId()) {
          $link = $this->getLinkGenerator()->generate($date,
            new Url('entity.gtfs_service.revision', [
              'gtfs_service' => $gtfs_service->id(),
              'gtfs_service_revision' => $vid
            ]));
        }
        else {
          $link = $gtfs_service->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.gtfs_service.translation_revert', ['gtfs_service' => $gtfs_service->id(), 'gtfs_service_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.gtfs_service.revision_revert', ['gtfs_service' => $gtfs_service->id(), 'gtfs_service_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.gtfs_service.revision_delete', ['gtfs_service' => $gtfs_service->id(), 'gtfs_service_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['gtfs_service_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
