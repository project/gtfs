<?php

namespace Drupal\gtfs\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\gtfs\Entity\GTFSObjectInterface;

/**
 * Class FrequencyController.
 *
 *  Returns responses for GTFS Frequency routes.
 */
class FrequencyController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a GTFS Frequency revision.
   *
   * @param int $gtfs_frequency_revision
   *   The GTFS Frequency revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionShow($gtfs_frequency_revision) {
    $frequency = $this->entityTypeManager()->getStorage('gtfs_frequency')->loadRevision($gtfs_frequency_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('gtfs_frequency');

    return $view_builder->view($frequency);
  }

  /**
   * Page title callback for a GTFS Frequency revision.
   *
   * @param int $gtfs_frequency_revision
   *   The GTFS Frequency revision ID.
   *
   * @return string
   *   The page title.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionPageTitle($gtfs_frequency_revision) {
    $frequency = $this->entityTypeManager()->getStorage('gtfs_frequency')->loadRevision($gtfs_frequency_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $frequency->label(),
      '%date' => \Drupal::service('date.formatter')
        ->format($frequency->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a GTFS Frequency.
   *
   * @param \Drupal\gtfs\Entity\GTFSObjectInterface $gtfs_frequency
   *   A GTFS Frequency object.
   *
   * @return array
   *   An array as expected by drupal_render().
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function revisionOverview(GTFSObjectInterface $gtfs_frequency) {
    $account = $this->currentUser();
    $langcode = $gtfs_frequency->language()->getId();
    $langname = $gtfs_frequency->language()->getName();
    $languages = $gtfs_frequency->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $frequency_storage = $this->entityTypeManager()->getStorage('gtfs_frequency');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $gtfs_frequency->label()]) : $this->t('Revisions for %title', ['%title' => $gtfs_frequency->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all gtfs frequency revisions") || $account->hasPermission('administer gtfs frequency entities')));
    $delete_permission = (($account->hasPermission("delete all gtfs frequency revisions") || $account->hasPermission('administer gtfs frequency entities')));

    $rows = [];

    $vids = $frequency_storage->revisionIds($gtfs_frequency);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\gtfs\Entity\GTFSObjectInterface $revision */
      $revision = $frequency_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $gtfs_frequency->getRevisionId()) {
          $link = $this->getLinkGenerator()->generate($date,
            new Url('entity.gtfs_frequency.revision', [
              'gtfs_frequency' => $gtfs_frequency->id(),
              'gtfs_frequency_revision' => $vid,
            ]));
        }
        else {
          $link = $gtfs_frequency->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.gtfs_frequency.translation_revert', ['gtfs_frequency' => $gtfs_frequency->id(), 'gtfs_frequency_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.gtfs_frequency.revision_revert', ['gtfs_frequency' => $gtfs_frequency->id(), 'gtfs_frequency_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.gtfs_frequency.revision_delete', ['gtfs_frequency' => $gtfs_frequency->id(), 'gtfs_frequency_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['gtfs_frequency_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
