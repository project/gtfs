<?php

namespace Drupal\gtfs\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\gtfs\Entity\GTFSObjectInterface;

/**
 * Class FareAttributeController.
 *
 *  Returns responses for GTFS Fare Attribute routes.
 */
class FareAttributeController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a GTFS Fare Attribute revision.
   *
   * @param int $gtfs_fare_attribute_revision
   *   The GTFS Fare Attribute revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionShow($gtfs_fare_attribute_revision) {
    $fare_attribute = $this->entityTypeManager()->getStorage('gtfs_fare_attribute')->loadRevision($gtfs_fare_attribute_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('gtfs_fare_attribute');

    return $view_builder->view($fare_attribute);
  }

  /**
   * Page title callback for a GTFS Fare Attribute revision.
   *
   * @param int $gtfs_fare_attribute_revision
   *   The GTFS Fare Attribute revision ID.
   *
   * @return string
   *   The page title.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionPageTitle($gtfs_fare_attribute_revision) {
    $fare_attribute = $this->entityTypeManager()->getStorage('gtfs_fare_attribute')->loadRevision($gtfs_fare_attribute_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $fare_attribute->label(),
      '%date' => \Drupal::service('date.formatter')
        ->format($fare_attribute->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a GTFS Fare Attribute .
   *
   * @param \Drupal\gtfs\Entity\GTFSObjectInterface $gtfs_fare_attribute
   *   A GTFS FareAttribute  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function revisionOverview(GTFSObjectInterface $gtfs_fare_attribute) {
    $account = $this->currentUser();
    $langcode = $gtfs_fare_attribute->language()->getId();
    $langname = $gtfs_fare_attribute->language()->getName();
    $languages = $gtfs_fare_attribute->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $fare_attribute_storage = $this->entityTypeManager()->getStorage('gtfs_fare_attribute');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $gtfs_fare_attribute->label()]) : $this->t('Revisions for %title', ['%title' => $gtfs_fare_attribute->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all gtfs fare_attribute revisions") || $account->hasPermission('administer gtfs fare_attribute entities')));
    $delete_permission = (($account->hasPermission("delete all gtfs fare_attribute revisions") || $account->hasPermission('administer gtfs fare_attribute entities')));

    $rows = [];

    $vids = $fare_attribute_storage->revisionIds($gtfs_fare_attribute);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\gtfs\Entity\GTFSObjectInterface $revision */
      $revision = $fare_attribute_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $gtfs_fare_attribute->getRevisionId()) {
          $link = $this->getLinkGenerator()->generate($date,
            new Url('entity.gtfs_fare_attribute.revision', [
              'gtfs_fare_attribute' => $gtfs_fare_attribute->id(),
              'gtfs_fare_attribute_revision' => $vid,
            ]));
        }
        else {
          $link = $gtfs_fare_attribute->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.gtfs_fare_attribute.translation_revert', ['gtfs_fare_attribute' => $gtfs_fare_attribute->id(), 'gtfs_fare_attribute_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.gtfs_fare_attribute.revision_revert', ['gtfs_fare_attribute' => $gtfs_fare_attribute->id(), 'gtfs_fare_attribute_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.gtfs_fare_attribute.revision_delete', ['gtfs_fare_attribute' => $gtfs_fare_attribute->id(), 'gtfs_fare_attribute_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['gtfs_fare_attribute_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
