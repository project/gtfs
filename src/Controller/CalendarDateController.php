<?php

namespace Drupal\gtfs\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\gtfs\Entity\GTFSObjectInterface;

/**
 * Class CalendarDateController.
 *
 *  Returns responses for GTFS Calendar Date routes.
 */
class CalendarDateController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a GTFS Calendar Date revision.
   *
   * @param int $calendar_date_revision
   *   The GTFS Calendar Date revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionShow($calendar_date_revision) {
    $calendar_date = $this->entityTypeManager()->getStorage('gtfs_calendar_date')->loadRevision($calendar_date_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('gtfs_calendar_date');

    return $view_builder->view($calendar_date);
  }

  /**
   * Page title callback for a GTFS Calendar Date revision.
   *
   * @param int $calendar_date_revision
   *   The GTFS Calendar Date revision ID.
   *
   * @return string
   *   The page title.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionPageTitle($calendar_date_revision) {
    $calendar_date = $this->entityTypeManager()->getStorage('gtfs_calendar_date')->loadRevision($calendar_date_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $calendar_date->label(),
      '%date' => \Drupal::service('date.formatter')
        ->format($calendar_date->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a GTFS Calendar Date .
   *
   * @param \Drupal\gtfs\Entity\GTFSObjectInterface $gtfs_calendar_date
   *   A GTFS Calendar Date object.
   *
   * @return array
   *   An array as expected by drupal_render().
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function revisionOverview(GTFSObjectInterface $gtfs_calendar_date) {
    $account = $this->currentUser();
    $langcode = $gtfs_calendar_date->language()->getId();
    $langname = $gtfs_calendar_date->language()->getName();
    $languages = $gtfs_calendar_date->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $calendar_date_storage = $this->entityTypeManager()->getStorage('gtfs_calendar_date');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $gtfs_calendar_date->label()]) : $this->t('Revisions for %title', ['%title' => $gtfs_calendar_date->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all gtfs calendar_date revisions") || $account->hasPermission('administer gtfs calendar_date entities')));
    $delete_permission = (($account->hasPermission("delete all gtfs calendar_date revisions") || $account->hasPermission('administer gtfs calendar_date entities')));

    $rows = [];

    $vids = $calendar_date_storage->revisionIds($gtfs_calendar_date);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\gtfs\Entity\GTFSObjectInterface $revision */
      $revision = $calendar_date_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $gtfs_calendar_date->getRevisionId()) {
          $link = $this->getLinkGenerator()->generate($date,
            new Url('entity.gtfs_calendar_date.revision', [
              'gtfs_calendar_date' => $gtfs_calendar_date->id(),
              'gtfs_calendar_date_revision' => $vid,
            ]));
        }
        else {
          $link = $gtfs_calendar_date->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.gtfs_calendar_date.translation_revert', ['gtfs_calendar_date' => $gtfs_calendar_date->id(), 'gtfs_calendar_date_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.gtfs_calendar_date.revision_revert', ['gtfs_calendar_date' => $gtfs_calendar_date->id(), 'gtfs_calendar_date_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.gtfs_calendar_date.revision_delete', ['gtfs_calendar_date' => $gtfs_calendar_date->id(), 'gtfs_calendar_date_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['gtfs_calendar_date_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
