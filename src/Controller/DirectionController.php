<?php

namespace Drupal\gtfs\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\gtfs\Entity\GTFSObjectInterface;

/**
 * Class DirectionController.
 *
 *  Returns responses for GTFS Direction routes.
 */
class DirectionController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a GTFS Direction revision.
   *
   * @param int $gtfs_direction_revision
   *   The GTFS Direction revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionShow($gtfs_direction_revision) {
    $direction = $this->entityTypeManager()->getStorage('gtfs_direction')->loadRevision($gtfs_direction_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('gtfs_direction');

    return $view_builder->view($direction);
  }

  /**
   * Page title callback for a GTFS Direction revision.
   *
   * @param int $gtfs_direction_revision
   *   The GTFS Direction revision ID.
   *
   * @return string
   *   The page title.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionPageTitle($gtfs_direction_revision) {
    $direction = $this->entityTypeManager()->getStorage('gtfs_direction')->loadRevision($gtfs_direction_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $direction->label(),
      '%date' => \Drupal::service('date.formatter')
        ->format($direction->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a GTFS Direction.
   *
   * @param \Drupal\gtfs\Entity\GTFSObjectInterface $gtfs_direction
   *   A GTFS Direction object.
   *
   * @return array
   *   An array as expected by drupal_render().
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function revisionOverview(GTFSObjectInterface $gtfs_direction) {
    $account = $this->currentUser();
    $langcode = $gtfs_direction->language()->getId();
    $langname = $gtfs_direction->language()->getName();
    $languages = $gtfs_direction->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $direction_storage = $this->entityTypeManager()->getStorage('gtfs_direction');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $gtfs_direction->label()]) : $this->t('Revisions for %title', ['%title' => $gtfs_direction->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all gtfs direction revisions") || $account->hasPermission('administer gtfs direction entities')));
    $delete_permission = (($account->hasPermission("delete all gtfs direction revisions") || $account->hasPermission('administer gtfs direction entities')));

    $rows = [];

    $vids = $direction_storage->revisionIds($gtfs_direction);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\gtfs\Entity\GTFSObjectInterface $revision */
      $revision = $direction_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $gtfs_direction->getRevisionId()) {
          $link = $this->getLinkGenerator()->generate($date, new Url('entity.gtfs_direction.revision', ['gtfs_direction' => $gtfs_direction->id(), 'gtfs_direction_revision' => $vid]));
        }
        else {
          $link = $gtfs_direction->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.gtfs_direction.translation_revert', ['gtfs_direction' => $gtfs_direction->id(), 'gtfs_direction_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.gtfs_direction.revision_revert', ['gtfs_direction' => $gtfs_direction->id(), 'gtfs_direction_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.gtfs_direction.revision_delete', ['gtfs_direction' => $gtfs_direction->id(), 'gtfs_direction_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['gtfs_direction_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
