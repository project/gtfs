<?php

namespace Drupal\gtfs\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\gtfs\Entity\GTFSObjectInterface;

/**
 * Class ShapeController.
 *
 *  Returns responses for GTFS Shape routes.
 */
class ShapeController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a GTFS Shape revision.
   *
   * @param int $gtfs_shape_revision
   *   The GTFS Shape revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionShow($gtfs_shape_revision) {
    $shape = $this->entityTypeManager()->getStorage('gtfs_shape')->loadRevision($gtfs_shape_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('gtfs_shape');

    return $view_builder->view($shape);
  }

  /**
   * Page title callback for a GTFS Shape revision.
   *
   * @param int $gtfs_shape_revision
   *   The GTFS Shape revision ID.
   *
   * @return string
   *   The page title.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionPageTitle($gtfs_shape_revision) {
    $shape = $this->entityTypeManager()->getStorage('gtfs_shape')->loadRevision($gtfs_shape_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $shape->label(),
      '%date' => \Drupal::service('date.formatter')
        ->format($shape->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a GTFS Shape.
   *
   * @param \Drupal\gtfs\Entity\GTFSObjectInterface $gtfs_shape
   *   A GTFS Shape object.
   *
   * @return array
   *   An array as expected by drupal_render().
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function revisionOverview(GTFSObjectInterface $gtfs_shape) {
    $account = $this->currentUser();
    $langcode = $gtfs_shape->language()->getId();
    $langname = $gtfs_shape->language()->getName();
    $languages = $gtfs_shape->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $shape_storage = $this->entityTypeManager()->getStorage('gtfs_shape');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $gtfs_shape->label()]) : $this->t('Revisions for %title', ['%title' => $gtfs_shape->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all gtfs shape revisions") || $account->hasPermission('administer gtfs shape entities')));
    $delete_permission = (($account->hasPermission("delete all gtfs shape revisions") || $account->hasPermission('administer gtfs shape entities')));

    $rows = [];

    $vids = $shape_storage->revisionIds($gtfs_shape);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\gtfs\Entity\GTFSObjectInterface $revision */
      $revision = $shape_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $gtfs_shape->getRevisionId()) {
          $link = $this->getLinkGenerator()->generate($date,
            new Url('entity.gtfs_shape.revision', [
              'gtfs_shape' => $gtfs_shape->id(),
              'gtfs_shape_revision' => $vid
            ]));
        }
        else {
          $link = $gtfs_shape->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.gtfs_shape.translation_revert', ['gtfs_shape' => $gtfs_shape->id(), 'gtfs_shape_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.gtfs_shape.revision_revert', ['gtfs_shape' => $gtfs_shape->id(), 'gtfs_shape_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.gtfs_shape.revision_delete', ['gtfs_shape' => $gtfs_shape->id(), 'gtfs_shape_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['gtfs_shape_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
