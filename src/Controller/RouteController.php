<?php

namespace Drupal\gtfs\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\gtfs\Entity\GTFSObjectInterface;

/**
 * Class RouteController.
 *
 *  Returns responses for GTFS Route routes.
 */
class RouteController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a GTFS Route revision.
   *
   * @param int $gtfs_route_revision
   *   The GTFS Route revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionShow($gtfs_route_revision) {
    $route = $this->entityTypeManager()->getStorage('gtfs_route')->loadRevision($gtfs_route_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('gtfs_route');

    return $view_builder->view($route);
  }

  /**
   * Page title callback for a GTFS Route revision.
   *
   * @param int $gtfs_route_revision
   *   The GTFS Route revision ID.
   *
   * @return string
   *   The page title.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionPageTitle($gtfs_route_revision) {
    $route = $this->entityTypeManager()->getStorage('gtfs_route')->loadRevision($gtfs_route_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $route->label(),
      '%date' => \Drupal::service('date.formatter')
        ->format($route->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a GTFS Route.
   *
   * @param \Drupal\gtfs\Entity\GTFSObjectInterface $gtfs_route
   *   A GTFS Route object.
   *
   * @return array
   *   An array as expected by drupal_render().
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function revisionOverview(GTFSObjectInterface $gtfs_route) {
    $account = $this->currentUser();
    $langcode = $gtfs_route->language()->getId();
    $langname = $gtfs_route->language()->getName();
    $languages = $gtfs_route->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $route_storage = $this->entityTypeManager()->getStorage('gtfs_route');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $gtfs_route->label()]) : $this->t('Revisions for %title', ['%title' => $gtfs_route->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all gtfs route revisions") || $account->hasPermission('administer gtfs route entities')));
    $delete_permission = (($account->hasPermission("delete all gtfs route revisions") || $account->hasPermission('administer gtfs route entities')));

    $rows = [];

    $vids = $route_storage->revisionIds($gtfs_route);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\gtfs\Entity\GTFSObjectInterface $revision */
      $revision = $route_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $gtfs_route->getRevisionId()) {
          $link = $this->getLinkGenerator()->generate($date,
            new Url('entity.gtfs_route.revision', [
              'gtfs_route' => $gtfs_route->id(),
              'gtfs_route_revision' => $vid
            ]));
        }
        else {
          $link = $gtfs_route->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.gtfs_route.translation_revert', ['gtfs_route' => $gtfs_route->id(), 'gtfs_route_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.gtfs_route.revision_revert', ['gtfs_route' => $gtfs_route->id(), 'gtfs_route_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.gtfs_route.revision_delete', ['gtfs_route' => $gtfs_route->id(), 'gtfs_route_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['gtfs_route_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
