<?php

namespace Drupal\gtfs\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\gtfs\Entity\GTFSObjectInterface;

/**
 * Class TripController.
 *
 *  Returns responses for GTFS Trip routes.
 */
class TripController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a GTFS Trip revision.
   *
   * @param int $gtfs_trip_revision
   *   The GTFS Trip revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionShow($gtfs_trip_revision) {
    $trip = $this->entityTypeManager()->getStorage('gtfs_trip')->loadRevision($gtfs_trip_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('gtfs_trip');

    return $view_builder->view($trip);
  }

  /**
   * Page title callback for a GTFS Trip revision.
   *
   * @param int $gtfs_trip_revision
   *   The GTFS Trip revision ID.
   *
   * @return string
   *   The page title.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionPageTitle($gtfs_trip_revision) {
    $trip = $this->entityTypeManager()->getStorage('gtfs_trip')->loadRevision($gtfs_trip_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $trip->label(),
      '%date' => \Drupal::service('date.formatter')
        ->format($trip->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a GTFS Trip.
   *
   * @param \Drupal\gtfs\Entity\GTFSObjectInterface $gtfs_trip
   *   A GTFS Trip object.
   *
   * @return array
   *   An array as expected by drupal_render().
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function revisionOverview(GTFSObjectInterface $gtfs_trip) {
    $account = $this->currentUser();
    $langcode = $gtfs_trip->language()->getId();
    $langname = $gtfs_trip->language()->getName();
    $languages = $gtfs_trip->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $trip_storage = $this->entityTypeManager()->getStorage('gtfs_trip');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $gtfs_trip->label()]) : $this->t('Revisions for %title', ['%title' => $gtfs_trip->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all gtfs trip revisions") || $account->hasPermission('administer gtfs trip entities')));
    $delete_permission = (($account->hasPermission("delete all gtfs trip revisions") || $account->hasPermission('administer gtfs trip entities')));

    $rows = [];

    $vids = $trip_storage->revisionIds($gtfs_trip);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\gtfs\Entity\GTFSObjectInterface $revision */
      $revision = $trip_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $gtfs_trip->getRevisionId()) {
          $link = $this->getLinkGenerator()->generate($date, new Url('entity.gtfs_trip.revision', ['gtfs_trip' => $gtfs_trip->id(), 'gtfs_trip_revision' => $vid]));
        }
        else {
          $link = $gtfs_trip->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.gtfs_trip.translation_revert', ['gtfs_trip' => $gtfs_trip->id(), 'gtfs_trip_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.gtfs_trip.revision_revert', ['gtfs_trip' => $gtfs_trip->id(), 'gtfs_trip_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.gtfs_trip.revision_delete', ['gtfs_trip' => $gtfs_trip->id(), 'gtfs_trip_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['gtfs_trip_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
