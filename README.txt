CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Configuration
* Next Steps
* Roadmap


INTRODUCTION
------------

This module provides utilities for working with [GTFS](https://gtfs.org/)
feeds in Drupal. Specifically, it provides entities for each file specified
in the GTFS standard that map and relate each of the fields therein.

NOTE: as this module is still under development, there are a few files for
which it does not provide entities.

It is designed to be used to base more complex Drupal entities off of
information synchronized with a GTFS feed. For example, a website for each
route that a transit agency provides.


INSTALLATION
------------

Install the GTFS Utilities module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

1. Add a feed
Navigate to the new GTFS top-level item in the menu system, and add a feed.
Use the URL of your GTFS feed (ending in .zip) and give the feed a name.

2. Import the feed
From the newly-created feed, click the "Import" button and follow the prompts.

3. Configure imported entities
Under the top-level GTFS menu, you can now view and configure all imported
entities.


NEXT STEPS
----------

You should now be able to run the import wizard every time an updated feed is
released and the imported entities will be updated with the new information
without deleting information manually added in new fields.


ROADMAP
-------

While this module is under development, it can certainly be used for basic
purposes. It currently powers a number of transit sites and web applications.
Further work needs to be done on:

* Automatic updating of GTFS entities
* Completion of GTFS file entities